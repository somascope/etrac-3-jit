# eTrac3 (etrac-3-jit)

eTrac 3 JIT mobile app

## Install the dependencies

```bash
yarn
# or
npm install
```

### CLI commands list

https://quasar.dev/quasar-cli-vite/commands-list

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

## Icon Genie

### Icons for SPA

```bash
icongenie generate -m spa -i D:/Projects/Zenmation/icon.png
```
