import { useQuasar } from "quasar";
import { Platform } from "quasar";

const $q = useQuasar();

export function useStatusBarColor(id) {
  const screenId = id.toLowerCase();
  let screen = screens.find((screen) => screen.id === screenId);

  if (screen === undefined) {
    screen = screens.find((screen) => screen.id === "primary");
  }

  const color = screen.color;

  if (Platform.is.cordova) {
    StatusBar.backgroundColorByHexString(color);
  }
}

let deviceIsReady = false;
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
  deviceIsReady = true;
  document.removeEventListener("deviceready", onDeviceReady, false);
  StatusBar.overlaysWebView(false);
}

const screens = [
  {
    id: "activation",
    color: "#000000",
  },
  {
    id: "home",
    color: "#000000",
  },
  {
    id: "search",
    color: "#000000",
  },
  {
    id: "splash",
    color: "#000000",
  },
  {
    id: "primary",
    color: "#216acc",
  },
  {
    id: "c1",
    color: "#23408f",
  },
  {
    id: "c2",
    color: "#7d2d86",
  },
  {
    id: "c3",
    color: "#a40b0b",
  },
  {
    id: "c4",
    color: "#dd561f",
  },
  {
    id: "c5",
    color: "#08892f",
  },
  {
    id: "c6",
    color: "#0093a5",
  },
];
