export function useTextFromHTML(htmlContent) {
  const html = htmlContent;
  const tempDiv = document.createElement("div");
  tempDiv.innerHTML = html;
  let rawText = tempDiv.textContent || tempDiv.innerText || "";
  rawText = rawText.replace(/\r?\n|\r/g, " ").trim();
  rawText = rawText.replace(/\s+/g, " ").trim();
  return rawText;
}
