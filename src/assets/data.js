const data = {
  ////////////////////////////////////////////////
  // About Content
  ////////////////////////////////////////////////
  about: [
    {
      id: 1,
      title: "An eLearning Companion App",
      content: `<i>eTrac<sup>®</sup> for just-in-time learning</i> is a companion app that is included with the licensing fee of the online learning curriculum <i>eTrac<sup>®</sup> for Job Seekers with Disabilities</i>.`,
    },
    {
      id: 2,
      title: "Job Seeking Tips and Techniques",
      content: `<i>eTrac<sup>®</sup> for just-in-time learning</i> provides job seekers with quick tips and techniques through a library of job seeking articles and videos available in the <i>eTrac<sup>®</sup> for Job Seekers with Disabilities</i> curriculum.`,
    },
    {
      id: 3,
      title: "Browse by 6 Job-Seeking Categories",
      content: `
      <ol>
        <li>Exploring a Career</li>
        <li>Filling Out the Paperwork</li>
        <li>Starting Your Job Search</li>
        <li>Interviewing for a Job</li>
        <li>Evaluating and Accepting a Job Offer</li>
        <li>Keeping a Job</li>
      </ol>`,
    },
    {
      id: 4,
      title: "Search Feature",
      content: `Looking for something specific? You can use the search feature to find any specific topics of interest you might have for finding or keeping a job.`,
    },
    {
      id: 5,
      title: "Audio Narration Provided",
      content: `You can choose to listen to an audio version of any article.`,
    },
    {
      id: 6,
      title: "My Application Feature",
      content: `The <b>My Application</b> feature of this <i>eTrac<sup>®</sup></i> app is an innovative way of collecting application data in one place on your smart phone and exporting this information to build resumés and to fill out applications for employers.`,
    },
    {
      id: 7,
      title: "Captions and Transcripts",
      content: `All videos are captioned, and provide full transcripts.`,
    },
    {
      id: 8,
      title: "Like your favorites",
      content: `Any articles or videos can be liked and easily revisited at any time.`,
    },
  ],
  ////////////////////////////////////////////////
  // Categories
  ////////////////////////////////////////////////
  categories: [
    {
      id: "c1",
      title: "Exploring a Career",
      image: "/images/Category1.jpg",
      imageDescription:
        "A man in a wheelchair with alternative access controls",
    },
    {
      id: "c2",
      title: "Filling Out the Paperwork",
      image: "/images/Category2.jpg",
      imageDescription: "Woman wearing glasses and smiling",
    },
    {
      id: "c3",
      title: "Starting Your Job Search",
      image: "/images/Category3.jpg",
      imageDescription: "A man wearing glasses in a wheel chair",
    },
    {
      id: "c4",
      title: "Interviewing for a Job",
      image: "/images/Category4.jpg",
      imageDescription: "A man wearing glasses smiling",
    },
    {
      id: "c5",
      title: "Evaluating and Accepting a Job Offer ",
      image: "/images/Category5.jpg",
      imageDescription: "A man in a commercial kitchen wearing chef's clothing",
    },
    {
      id: "c6",
      title: "Keeping a Job",
      image: "/images/Category6.jpg",
      imageDescription: "A woman working in a warehouse",
    },
  ],
  //////////////////////////////////////////////////////////
  // Topics
  // List item types:
  // a. resources       (type: "resource", id: "c4-t3-r2")
  // b. resource lists  (type: "list", id: "c4-t3-l1")
  //////////////////////////////////////////////////////////
  topics: [
    // Category 1 topics
    {
      id: "c1-t1",
      categoryId: "c1",
      title: "Getting Started",
      list: {
        items: [
          {
            type: "resource",
            id: "c1-t1-r1",
          },
          {
            type: "resource",
            id: "c1-t1-r2",
          },
          {
            type: "resource",
            id: "c1-t1-r3",
          },
          {
            type: "resource",
            id: "c1-t1-r4",
          },
          {
            type: "resource",
            id: "c1-t1-r5",
          },
        ],
      },
    },
    {
      id: "c1-t2",
      categoryId: "c1",
      title: "Choosing a Career",
      list: {
        items: [
          {
            type: "list",
            id: "c1-t2-l1",
          },
        ],
      },
    },
    {
      id: "c1-t3",
      categoryId: "c1",
      title: "Exploring Types of Jobs",
      list: {
        items: [
          {
            type: "resource",
            id: "c1-t3-r1",
          },
          {
            type: "resource",
            id: "c1-t3-r2",
          },
          {
            type: "resource",
            id: "c1-t3-r3",
          },
        ],
      },
    },
    // Category 2 topics
    {
      id: "c2-t1",
      categoryId: "c2",
      title: "Getting Started",
      list: {
        items: [
          {
            type: "resource",
            id: "c2-t1-r1",
          },
          {
            type: "resource",
            id: "c2-t1-r2",
          },
        ],
      },
    },
    {
      id: "c2-t2",
      categoryId: "c2",
      title: "Filling Out Job Applications",
      list: {
        items: [
          {
            type: "resource",
            id: "c2-t2-r1",
          },
          {
            type: "list",
            id: "c2-t2-l1",
          },
          {
            type: "list",
            id: "c2-t2-l2",
          },
        ],
      },
    },
    {
      id: "c2-t3",
      categoryId: "c2",
      title: "Resumés",
      list: {
        items: [
          {
            type: "list",
            id: "c2-t3-l1",
          },
          {
            type: "list",
            id: "c2-t3-l2",
          },
        ],
      },
    },
    {
      id: "c2-t4",
      categoryId: "c2",
      title: "Supporting Materials",
      list: {
        items: [
          {
            type: "list",
            id: "c2-t4-l1",
          },
        ],
      },
    },
    // Category 3 topics
    {
      id: "c3-t1",
      categoryId: "c3",
      title: "Getting Started",
      list: {
        items: [
          {
            type: "resource",
            id: "c3-t1-r1",
          },
          {
            type: "resource",
            id: "c3-t1-r2",
          },
        ],
      },
    },
    {
      id: "c3-t2",
      categoryId: "c3",
      title: "How and Where to Look for Jobs",
      list: {
        items: [
          {
            type: "list",
            id: "c3-t2-l1",
          },
        ],
      },
    },
    {
      id: "c3-t3",
      categoryId: "c3",
      title: "Career Networking",
      list: {
        items: [
          {
            type: "list",
            id: "c3-t3-l1",
          },
        ],
      },
    },
    {
      id: "c3-t4",
      categoryId: "c3",
      title: "Starting Your Job Search",
      list: {
        items: [
          {
            type: "resource",
            id: "c3-t4-r1",
          },
          {
            type: "resource",
            id: "c3-t4-r2",
          },
          {
            type: "list",
            id: "c3-t4-l1",
          },
          {
            type: "resource",
            id: "c3-t4-r8",
          },
          {
            type: "resource",
            id: "c3-t4-r9",
          },
        ],
      },
    },
    // Category 4 topics
    {
      id: "c4-t1",
      categoryId: "c4",
      title: "Interviewing Basics",
      list: {
        items: [
          {
            type: "resource",
            id: "c4-t1-r1",
          },
          {
            type: "resource",
            id: "c4-t1-r2",
          },
          {
            type: "list",
            id: "c4-t1-l1",
          },
        ],
      },
    },
    {
      id: "c4-t2",
      categoryId: "c4",
      title: "Preparing for the Interview",
      list: {
        items: [
          {
            type: "resource",
            id: "c4-t2-r1",
          },
          {
            type: "resource",
            id: "c4-t2-r2",
          },
          {
            type: "resource",
            id: "c4-t2-r3",
          },
          {
            type: "resource",
            id: "c4-t2-r4",
          },
          {
            type: "list",
            id: "c4-t2-l1",
          },
          {
            type: "resource",
            id: "c4-t2-r9",
          },
          {
            type: "resource",
            id: "c4-t2-r10",
          },
          {
            type: "resource",
            id: "c4-t2-r11",
          },
          {
            type: "resource",
            id: "c4-t2-r12",
          },
          {
            type: "list",
            id: "c4-t2-l2",
          },
          {
            type: "list",
            id: "c4-t2-l3",
          },
          {
            type: "resource",
            id: "c4-t2-r21",
          },
          {
            type: "resource",
            id: "c4-t2-r22",
          },
        ],
      },
    },
    {
      id: "c4-t3",
      categoryId: "c4",
      title: "Completing the Interview",
      list: {
        items: [
          {
            type: "resource",
            id: "c4-t3-r1",
          },
          {
            type: "resource",
            id: "c4-t3-r2",
          },
          {
            type: "list",
            id: "c4-t3-l1",
          },
          {
            type: "resource",
            id: "c4-t3-r12",
          },
          {
            type: "resource",
            id: "c4-t3-r13",
          },
          {
            type: "list",
            id: "c4-t3-l2",
          },
          {
            type: "resource",
            id: "c4-t3-r22",
          },
          {
            type: "list",
            id: "c4-t3-l3",
          },
          {
            type: "list",
            id: "c4-t3-l4",
          },
        ],
      },
    },
    {
      id: "c4-t4",
      categoryId: "c4",
      title: "Following Up After the Interview",
      list: {
        items: [
          {
            type: "resource",
            id: "c4-t4-r1",
          },
          {
            type: "resource",
            id: "c4-t4-r2",
          },
          {
            type: "resource",
            id: "c4-t4-r3",
          },
        ],
      },
    },
    // Category 5 topics
    {
      id: "c5-t1",
      categoryId: "c5",
      title: "The Waiting Game",
      list: {
        items: [
          {
            type: "resource",
            id: "c5-t1-r1",
          },
          {
            type: "resource",
            id: "c5-t1-r2",
          },
          {
            type: "resource",
            id: "c5-t1-r3",
          },
          {
            type: "resource",
            id: "c5-t1-r4",
          },
          {
            type: "resource",
            id: "c5-t1-r5",
          },
          {
            type: "resource",
            id: "c5-t1-r6",
          },
          {
            type: "resource",
            id: "c5-t1-r7",
          },
        ],
      },
    },
    {
      id: "c5-t2",
      categoryId: "c5",
      title: "Deciding if the Job is Right",
      list: {
        items: [
          {
            type: "resource",
            id: "c5-t2-r1",
          },
          {
            type: "resource",
            id: "c5-t2-r2",
          },
          {
            type: "resource",
            id: "c5-t2-r3",
          },
          {
            type: "resource",
            id: "c5-t2-r4",
          },
          {
            type: "resource",
            id: "c5-t2-r5",
          },
        ],
      },
    },
    {
      id: "c5-t3",
      categoryId: "c5",
      title: "Employment Paperwork",
      list: {
        items: [
          {
            type: "resource",
            id: "c5-t3-r1",
          },
          {
            type: "resource",
            id: "c5-t3-r2",
          },
          {
            type: "resource",
            id: "c5-t3-r3",
          },
          {
            type: "resource",
            id: "c5-t3-r4",
          },
          {
            type: "resource",
            id: "c5-t3-r5",
          },
          {
            type: "resource",
            id: "c5-t3-r6",
          },
          {
            type: "resource",
            id: "c5-t3-r7",
          },
        ],
      },
    },
    {
      id: "c5-t4",
      categoryId: "c5",
      title: "Disclosure and Accommodations",
      list: {
        items: [
          {
            type: "resource",
            id: "c5-t4-r1",
          },
          {
            type: "resource",
            id: "c5-t4-r2",
          },
          {
            type: "resource",
            id: "c5-t4-r3",
          },
          {
            type: "resource",
            id: "c5-t4-r4",
          },
          {
            type: "resource",
            id: "c5-t4-r5",
          },
          {
            type: "resource",
            id: "c5-t4-r6",
          },
          {
            type: "resource",
            id: "c5-t4-r7",
          },
          {
            type: "resource",
            id: "c5-t4-r8",
          },
        ],
      },
    },
    // Category 6 topics
    {
      id: "c6-t1",
      categoryId: "c6",
      title: "Getting Started",
      list: {
        items: [
          {
            type: "resource",
            id: "c6-t1-r1",
          },
          {
            type: "resource",
            id: "c6-t1-r2",
          },
          {
            type: "resource",
            id: "c6-t1-r3",
          },
        ],
      },
    },
    {
      id: "c6-t2",
      categoryId: "c6",
      title: "Your First Day",
      list: {
        items: [
          {
            type: "resource",
            id: "c6-t2-r1",
          },
          {
            type: "resource",
            id: "c6-t2-r2",
          },
          {
            type: "resource",
            id: "c6-t2-r3",
          },
          {
            type: "resource",
            id: "c6-t2-r4",
          },
          {
            type: "resource",
            id: "c6-t2-r5",
          },
          {
            type: "resource",
            id: "c6-t2-r6",
          },
        ],
      },
    },
    {
      id: "c6-t3",
      categoryId: "c6",
      title: "Keeping Your Job and Resolving Problems",
      list: {
        items: [
          {
            type: "resource",
            id: "c6-t3-r1",
          },
          {
            type: "resource",
            id: "c6-t3-r2",
          },
          {
            type: "resource",
            id: "c6-t3-r3",
          },
          {
            type: "resource",
            id: "c6-t3-r4",
          },
          {
            type: "resource",
            id: "c6-t3-r5",
          },
          {
            type: "resource",
            id: "c6-t3-r6",
          },
          {
            type: "resource",
            id: "c6-t3-r7",
          },
          {
            type: "list",
            id: "c6-t3-l1",
          },
          {
            type: "resource",
            id: "c6-t3-r10",
          },
          {
            type: "resource",
            id: "c6-t3-r11",
          },
        ],
      },
    },
    {
      id: "c6-t4",
      categoryId: "c6",
      title: "What's Next for You?",
      list: {
        items: [
          {
            type: "resource",
            id: "c6-t4-r1",
          },
          {
            type: "resource",
            id: "c6-t4-r2",
          },
          {
            type: "resource",
            id: "c6-t4-r3",
          },
          {
            type: "resource",
            id: "c6-t4-r4",
          },
        ],
      },
    },
  ],
  ////////////////////////////////////////////////
  // Resource lists
  ////////////////////////////////////////////////
  resourceLists: [
    {
      id: "c1-t2-l1",
      categoryId: "c1",
      topicId: "c1-t2",
      introItem: "c1-t2-r1",
      list: {
        items: [
          {
            id: "c1-t2-r2",
          },
          {
            id: "c1-t2-r3",
          },
          {
            id: "c1-t2-r4",
          },
          {
            id: "c1-t2-r5",
          },
        ],
      },
    },
    {
      id: "c2-t2-l1",
      categoryId: "c2",
      topicId: "c2-t2",
      introItem: "c2-t2-r2",
      list: {
        items: [
          {
            id: "c2-t2-r3",
          },
          {
            id: "c2-t2-r4",
          },
        ],
      },
    },
    {
      id: "c2-t2-l2",
      categoryId: "c2",
      topicId: "c2-t2",
      introItem: "c2-t2-r5",
      list: {
        items: [
          {
            id: "c2-t2-r6",
          },
          {
            id: "c2-t2-r7",
          },
          {
            id: "c2-t2-r8",
          },
          {
            id: "c2-t2-r9",
          },
          {
            id: "c2-t2-r10",
          },
          {
            id: "c2-t2-r11",
          },
        ],
      },
    },
    {
      id: "c2-t3-l1",
      categoryId: "c2",
      topicId: "c2-t3",
      introItem: "c2-t3-r1",
      list: {
        items: [
          {
            id: "c2-t3-r2",
          },
          {
            id: "c2-t3-r3",
          },
          {
            id: "c2-t3-r4",
          },
        ],
      },
    },
    {
      id: "c2-t3-l2",
      categoryId: "c2",
      topicId: "c2-t3",
      introItem: "c2-t3-r5",
      list: {
        items: [
          {
            id: "c2-t3-r6",
          },
          {
            id: "c2-t3-r7",
          },
          {
            id: "c2-t3-r8",
          },
        ],
      },
    },
    {
      id: "c2-t4-l1",
      categoryId: "c2",
      topicId: "c2-t4",
      introItem: "c2-t4-r1",
      list: {
        items: [
          {
            id: "c2-t4-r2",
          },
          {
            id: "c2-t4-r3",
          },
          {
            id: "c2-t4-r4",
          },
          {
            id: "c2-t4-r5",
          },
        ],
      },
    },
    {
      id: "c3-t2-l1",
      categoryId: "c3",
      topicId: "c3-t2",
      introItem: "c3-t2-r1",
      list: {
        items: [
          {
            id: "c3-t2-r2",
          },
          {
            id: "c3-t2-r3",
          },
          {
            id: "c3-t2-r4",
          },
          {
            id: "c3-t2-r5",
          },
          {
            id: "c3-t2-r6",
          },
        ],
      },
    },
    {
      id: "c3-t3-l1",
      categoryId: "c3",
      topicId: "c3-t3",
      introItem: "c3-t3-r1",
      list: {
        items: [
          {
            id: "c3-t3-r2",
          },
          {
            id: "c3-t3-r3",
          },
          {
            id: "c3-t3-r4",
          },
        ],
      },
    },
    {
      id: "c3-t4-l1",
      categoryId: "c3",
      topicId: "c3-t4",
      introItem: "c3-t4-r3",
      list: {
        items: [
          {
            id: "c3-t4-r4",
          },
          {
            id: "c3-t4-r5",
          },
          {
            id: "c3-t4-r6",
          },
          {
            id: "c3-t4-r7",
          },
        ],
      },
    },
    {
      id: "c4-t1-l1",
      categoryId: "c4",
      topicId: "c4-t1",
      introItem: "c4-t1-r3",
      list: {
        items: [
          {
            id: "c4-t1-r4",
          },
          {
            id: "c4-t1-r8",
          },
          {
            id: "c4-t1-r5",
          },
          {
            id: "c4-t1-r6",
          },
          {
            id: "c4-t1-r7",
          },
        ],
      },
    },
    {
      id: "c4-t2-l1",
      categoryId: "c4",
      topicId: "c4-t2",
      introItem: "c4-t2-r5",
      list: {
        items: [
          {
            id: "c4-t2-r6",
          },
          {
            id: "c4-t2-r7",
          },
          {
            id: "c4-t2-r8",
          },
        ],
      },
    },
    {
      id: "c4-t2-l2",
      categoryId: "c4",
      topicId: "c4-t2",
      introItem: "c4-t2-r13",
      list: {
        items: [
          {
            id: "c4-t2-r14",
          },
          {
            id: "c4-t2-r15",
          },
        ],
      },
    },
    {
      id: "c4-t2-l3",
      categoryId: "c4",
      topicId: "c4-t2",
      introItem: "c4-t2-r16",
      list: {
        items: [
          {
            id: "c4-t2-r17",
          },
          {
            id: "c4-t2-r18",
          },
          {
            id: "c4-t2-r19",
          },
          {
            id: "c4-t2-r20",
          },
        ],
      },
    },
    {
      id: "c4-t3-l1",
      categoryId: "c4",
      topicId: "c4-t3",
      introItem: "c4-t3-r3",
      list: {
        items: [
          {
            id: "c4-t3-r4",
          },
          {
            id: "c4-t3-r5",
          },
          {
            id: "c4-t3-r6",
          },
          {
            id: "c4-t3-r7",
          },
          {
            id: "c4-t3-r8",
          },
        ],
      },
    },
    {
      id: "c4-t3-l2",
      categoryId: "c4",
      topicId: "c4-t3",
      introItem: "c4-t3-r14",
      list: {
        items: [
          {
            id: "c4-t3-r15",
          },
          {
            id: "c4-t3-r16",
          },
          {
            id: "c4-t3-r17",
          },
          {
            id: "c4-t3-r18",
          },
          {
            id: "c4-t3-r21",
          },
        ],
      },
    },
    {
      id: "c4-t3-l3",
      categoryId: "c4",
      topicId: "c4-t3",
      introItem: "c4-t3-r23",
      list: {
        items: [
          {
            id: "c4-t3-r24",
          },
          {
            id: "c4-t3-r25",
          },
          {
            id: "c4-t3-r26",
          },
        ],
      },
    },
    {
      id: "c4-t3-l4",
      categoryId: "c4",
      topicId: "c4-t3",
      introItem: "c4-t3-r27",
      list: {
        items: [
          {
            id: "c4-t3-r28",
          },
        ],
      },
    },
    {
      id: "c6-t3-l1",
      categoryId: "c6",
      topicId: "c6-t3",
      introItem: "c6-t3-r8",
      list: {
        items: [
          {
            id: "c6-t3-r9",
          },
        ],
      },
    },
  ],
  ////////////////////////////////////////////////
  // Resources
  ////////////////////////////////////////////////
  resources: [
    // Category 1 resources
    {
      id: "c1-t1-r1",
      categoryId: "c1",
      topicId: "c1-t1",
      title: "Why People Work",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t1-r1_cy6cre.jpg",
      thumbnailDescription:
        "Group of people are meeting together in a job setting.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598359/audio/c1-t1-r1_nhxyiz.mp3",
        text: `
        <div class="article">
          <p>A job is a set of specific tasks that an employer pays another person to complete. Some workers are paid for each hour that they work; others are paid a set amount (a salary) every week, two weeks or monthly.</p>
          <p>Some jobs are unpaid. These are called volunteer jobs and can be a good way to build your skills and gain experience. Volunteer jobs should be treated with the same respect as paid work.</p>
          <p>People work for different reasons. Earning money tops the list. After all, you need to pay for things like housing, food, transportation, and entertainment. People also work to:</p>
          <ul>
            <li>Be independent.</li>
            <li>Have access to employment benefits, like health care.</li>
            <li>Meet new people and be part of a social group.</li>
            <li>Learn new skills.</li>
            <li>Expand their skills.</li>
            <li>Use their skills in new ways.</li>
            <li>Contribute to the community.</li>
            <li>Feel productive.</li>
            <li>Stay active.</li>
          </ul>
          <p>Do you know why you want to work? Understanding your reasons for working will help you to decide what you want to do and where you want to work.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t1-r2",
      categoryId: "c1",
      topicId: "c1-t1",
      title: "What Employers Want in an Employee",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t1-r2_wnxqgi.jpg",
      thumbnailDescription:
        "Young woman who is blind is wearing headphones and typing at a computer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598359/audio/c1-t1-r2_aqmfgp.mp3",
        text: `
        <div class="article">
          <p>All employers typically look for employees who:</p>
          <ol>
            <li><b>Are honest</b>. This means being truthful in what you say and in what you do.</li>
            <li><b>Can do the work</b>. Employers want to hire someone who can do the tasks the job requires. This doesn't mean they only hire people who have already done a job or task before. Employers also are looking for employees who can learn to do the work and have the right education, skills, experience, and attitude.</li>
            <li><b>Work hard and do quality work</b>. Employers want to hire a person who is willing to complete all of the job's tasks – even the ones the person doesn't like to do!</li>
            <li><b>Are flexible and willing to learn new things</b>. Most employers have their own way of doing things. While they don't expect a new employee to know how to do a job perfectly the first day, they do expect the employee to be willing to learn new things or new ways of doing things.</li>
            <li><b>Are reliable</b>. Employers want to hire employees who show up when they're scheduled to work, arrive on time, and are ready to work.</li>
            <li><b>Get along with others and have a positive attitude</b>. Teamwork is part of almost every job. Employers want employees who will work well with co-workers, supervisors, and customers.</li>
          </ol>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t1-r3",
      categoryId: "c1",
      topicId: "c1-t1",
      title: "Typical Job Search Process",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t1-r3_rfomjq.jpg",
      thumbnailDescription: "Person at a computer is doing a job search.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1696523473/audio/c1-t1-r3_01_oev48o.mp3",
        text: `
        <div class="article">
          <h2><span style="font-size: 1.4em;">Phase 1:</span><br/>Exploring a Job and Career</h2>
          <p>Learning about jobs and industries that match your interests will help you identify the skills, education, and experiences you'll need to get a job in a career that interests you.</p>
          <h2><span style="font-size: 1.4em;">Phase 2: </span><br/>Filling Out the Paperwork</h2>
          <p>You'll need to fill out applications and write resumés and cover letters. A resumé outlines your experiences, skills and work history. A cover letter highlights your interest and qualifications for the specific job. You also might need to prepare letters of explanation and gather work samples.</p>

          <h2><span style="font-size: 1.4em;">Phase 3: </span><br/>Starting Your Job Search</h2>
          <p>There are many ways and places to find jobs. Today, there are many more ways to find job openings but some things – like networking – are still very important.</p>

          <h2><span style="font-size: 1.4em;">Phase 4: </span><br/>Interviewing for a Job</h2>
          <p>Most employers interview potential employees before making a hiring decision.</p>

          <h2><span style="font-size: 1.4em;">Phase 5: </span><br/>Evaluating and Accepting a Job Offer</h2>
          <p>Not every job is right for every person. When you're offered a job, it's important to make sure it's a good fit.</p>

          <h2><span style="font-size: 1.4em;">Phase 6: </span><br/>Keeping the Job</h2>
          <p>Successful employees know what it takes to meet an employer's expectations.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c1-t1-r4",
      categoryId: "c1",
      topicId: "c1-t1",
      title: "Living the Dream!",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t1-r4_zjrjfu.jpg",
      thumbnailPosition: "17% 50%",
      thumbnailDescription: "Headshot of a man being interviewed",
      type: "video",
      content: {
        media: "hsUlhrt7uCg",
        text: `
        <div class="transcript">
          <h2>Doing What I Like</h2>
          <p><span class="transcript-speaker">James:</span> Doing the job that I have now could lead me to something better in my career.</p>
          <p><span class="transcript-speaker">Laura:</span> I love the atmosphere. You meet good solid people who take care of their bodies and live an active lifestyle and a healthy lifestyle and I just love it. They're friendly and that's all about me. I'm a friendly person, a caring person.</p>
          <p><span class="transcript-speaker">Bennie:</span> This is a very nice job and I Iove it and I enjoy it, you know. It's something I always wanted to do.</p>
          <p><span class="transcript-speaker">Zach:</span> I like the attitudes that the other coworkers show to help customers find certain things in a store, throughout the retail environment. And I am really happy with what I'm doing right now.</p>
          <p><span class="transcript-speaker">Laura:</span> I go above and beyond to make the club look nice and clean. I do things that you don't have to do. But that's just who I am. I'm a hard worker.</p>

          <h2>Training and Education</h2>
          <p><span class="transcript-speaker">Bennie:</span> I wasn't the type of person that had a lot of skills. Then, I went to the American Red Cross and got my CPR license and my first aid license. Without this training, I couldn't get what I wanted. If you want to go back to school, go back to school.</p>
          <p><span class="transcript-speaker">James:</span> Working at Midwest Special Services, I learned to work with vulnerable adults and that was one of the main reasons they wanted to hire me with health care services because a lot of the residents are considered vulnerable adults. It was because I had the experience and the patience and understanding to work with a variety of different people.</p>
          <p><span class="transcript-speaker">Alan:</span> I took what's called a Food Service Aid course and I went through a work experience program during my last year there. I received some training through a summer program that I was a part of for four summers called Natural Environment training.</p>
          <p><span class="transcript-speaker">Zach:</span> We mainly learned about what we do in a retail environment like how to greet customers and how to like tag things, tag clothes with a price. So far I've gone through the first and second levels and I can't wait to start the third.</p>

          <h2>Support from a Service Provider</h2>
          <p><span class="transcript-speaker">Alan:</span> When I worked with my job placement specialist, some of the things that she helped me with was perfecting and updating my resumé so it's more presentable to the employer so the employer has a better impression of my abilities and feels more confident in hiring me.</p>
          <p><span class="transcript-speaker">James:</span> I was recommended to take preparation classes, workshop classes. They help you with your interview skills. They help you fill out applications.</p>
          <p><span class="transcript-speaker">Bennie:</span> It's good to have people that you can turn to. That's why I called her my angel. And that's why I wanted to do the things because she stepped up and helped me and encouraged me to help others.</p>
          <p><span class="transcript-speaker">James:</span> They also help after you get the job to help you stay employed. And your financial end of it as well.</p>
          <p><span class="transcript-speaker">Alan:</span> And she helped me to follow up on job leads. She persuaded me to keep on working at it and going back and checking on the status of my application until I finally landed the job.</p>
          <p><span class="transcript-speaker">Laura:</span> Every Monday morning I'd meet with my job placement specialist, Heather, and we just talk about how my job is going. How it went last week. I just say what I have to say. Did I do this right in a good way? I just had to have some reassurance that I did something okay.</p>
          <p><span class="transcript-speaker">James:</span> They actually went out on to the streets with me to different job opportunities.</p>
          <p><span class="transcript-speaker">Alan:</span> And what would help is for a professional in that area to explain things thoroughly to me.</p>

          <h2>Disclosure and Accommodations</h2>
          <p><span class="transcript-speaker">Laura:</span> As far as accommodations, she was very understanding. She was out of this world understanding and she came up with all of the ways to make my job successful for me. When I chose to disclose my cerebral palsy to my interviewer, I chose to do it after the whole interview because I just wanted to go in there first of all and really state who I am and my disability is just one little limitation that I have. I just wanted to present myself without that. That was my decision to let them know because I would feel more comfortable.</p>
          <p><span class="transcript-speaker">James:</span> Being on Social Security, I needed to have the job to accommodate my work schedule to fit in with my RSDI benefits. They helped accommodate me and reduce my hours and reduce my days so that I could still be a working adult.</p>
          <p><span class="transcript-speaker">Alan:</span> I told them that I had a disability and that I'm going to need some accommodations. And he said no problem. There are larger labels on things so that I'm able to read them easier. And there are some people that are willing to show me where things go.</p>

          <h2>Finding Encouragement</h2>
          <p><span class="transcript-speaker">Bennie:</span> My momma always told me not to ever to give up. I started at the bottom. And look at me. I have a good job. Because I had faith and I can do it and I'm not going to put myself down and not think I can't do it because this is something in my heart that I really want to do.</p>
          <p><span class="transcript-speaker">Laura:</span> You just have to be yourself. Don't be anybody else and just don't give up. Keep on going for it. Do whatever, call up the managers to get their attention in a respectful way. Just present yourself.</p>
          <p><span class="transcript-speaker">James:</span> I kept myself encouraged by volunteering at a feline rescue and found some way I could fit into society by volunteering there.</p>

          <h2>Where to Look for Jobs</h2>
          <p><span class="transcript-speaker">James:</span> My job search didn't include as much personal contact as I was used to. A lot of it was done on computers so that made me feel little different because of the changes of the way people apply for jobs.</p>
          <p><span class="transcript-speaker">Zach:</span> I applied for a job online. I have a list of stores I want to work for.</p>
          <p><span class="transcript-speaker">Alan:</span> I just simply went about picking up applications, filling them out and then returning them.</p>
          <p><span class="transcript-speaker">James:</span> We found more job openings in the newspaper because it was kind of an old-fashioned way of finding a job.</p>
          <p><span class="transcript-speaker">Zach:</span> I go there where they have a cashier desk and ask for an application...if they do them online or if they have a paper application and if they do I would ask for a paper application and fill it out and bring it back to them.</p>

          <h2>Applications</h2>
          <p><span class="transcript-speaker">Laura:</span> The application process was overwhelming I would have to say but I didn't give up. You just have to fill out everything.</p>
          <p><span class="transcript-speaker">James:</span> It wasn't so bad to fill out many applications and hand in the resumés and go on the Internet to apply for jobs.</p>
          <p><span class="transcript-speaker">Zach:</span> I fill it out at home and sometimes when I'm at the mall, I fill it out and bring it back to the employer.</p>

          <h2>Resumés</h2>
          <p><span class="transcript-speaker">Laura:</span> I do have a resumé.</p>
          <p><span class="transcript-speaker">James:</span> I include my work skills.</p>
          <p><span class="transcript-speaker">Alan:</span> Where you graduated from high school.</p>
          <p><span class="transcript-speaker">Laura:</span> Volunteer work.</p>
          <p><span class="transcript-speaker">James:</span> Things that I have to offer from previous jobs.</p>
          <p><span class="transcript-speaker">Laura:</span> One-page...employers do not like more than one page.</p>
          <p><span class="transcript-speaker">Alan:</span> Your job experience in the last two years.</p>
          <p><span class="transcript-speaker">James:</span> Put each one in order so that they can see where I worked at last and where I worked at in the beginning.</p>
          <p><span class="transcript-speaker">Alan:</span> Brief. Concise. To the point.</p>
          <p><span class="transcript-speaker">James:</span> My education and hobbies.</p>
          <p><span class="transcript-speaker">Zach:</span> I have a chronological resumé that lists the things I did in high school.</p>
          <p><span class="transcript-speaker">Bennie:</span> I keep like a folder of all my paperwork that I went through from day one. I always go back and look at them things.</p>

          <h2>Preparing for the Interview</h2>
          <p><span class="transcript-speaker">Laura:</span> I did some mock interviews with my job placement specialist. The more and more we did mock interviews, the more I got comfortable. I wasn't nervous at all.</p>
          <p><span class="transcript-speaker">James:</span> Mock interview sessions helped to learn what to do when you apply at a real job.</p>
          <p><span class="transcript-speaker">Alan:</span> I did do some mock interviewing and that helped me a great deal.</p>

          <h2>The Interview</h2>
          <p><span class="transcript-speaker">Laura:</span> My first interview wasn't what I expected at all. The operations manager just gave out statements. Like if you were in this position, what would you do and why or how would you help a member in an emergency situation. It wasn't like, so tell me about yourself. What do you like to do? It was very different but I was prepared.</p>
          <p><span class="transcript-speaker">Alan:</span> I bring a copy of my resumé along and always make eye contact with the employer. Make sure you are well dressed and the things you say to the employer do not sound rehearsed but sound heartfelt, honest and professional.</p>
          <p><span class="transcript-speaker">James:</span> Always prepare ahead of time.</p>
          <p><span class="transcript-speaker">Alan:</span> Make sure you follow up so that the employer doesn't forget about you.</p>

          <h2>Future Goals</h2>
          <p><span class="transcript-speaker">Bennie:</span> Well, I want to go back to school and take up nursing. And be in a situation that I can help people.</p>
          <p><span class="transcript-speaker">Laura:</span> I ultimately want to be a manager of Lifetime Fitness. I just have to go to my employer, my bosses, and say this is ultimately what I want to do. What do I have to do? Of course I have to gain experience.</p>
          <p><span class="transcript-speaker">James:</span> The amount of time that I've been in the housekeeping department will be a stepping stone to a supervisor or a lead person.</p>
          <p><span class="transcript-speaker">Zach:</span> I hope to become a manager whenever I get the chance.</p>

          <h2>Good Advice</h2>
          <p><span class="transcript-speaker">Laura:</span> Just be yourself. Don't be anyone else. That's what I want to say to everybody out there. Don't give up. Just be yourself.</p>
          <p><span class="transcript-speaker">James:</span> Try to use those preparation classes sooner and more. Those really help to build strengths that were necessary to get the job like resumé writing and mock interview sessions.</p>
          <p><span class="transcript-speaker">Alan:</span> Reliability is one of the key factors in being to hold down a job and being trustworthy in the employer's eyes.</p>
          <p><span class="transcript-speaker">James:</span> Somewhere I heard it was said that you have to treat looking for a job like it is a job.</p>
          <p><span class="transcript-speaker">Laura:</span> Don't give up. Do everything you can. You got to be active. You got to put yourself out there because you never know until you try.</p>
          <p><span class="transcript-speaker">Bennie:</span> With the training they have today, there is a will and a way for everyone. Just go for it.</p>
          <p><span class="transcript-speaker">Laura:</span> I would highly recommend job shadowing to other seekers because it gives a person a sense of what they're jumping into before they enter their job. I just followed her around and just saw what she got to do, every little thing like interacting with the members and the guests.</p>
          <p><span class="transcript-speaker">James:</span> You should have stick-to-it-ness to keep on trying even when it seems like you're not getting anywhere.</p>
          <p><span class="transcript-speaker">Alan:</span> Do some volunteering while you're unemployed too so your employer knows your skills are fresh.</p>
          <p><span class="transcript-speaker">Laura:</span> I have to say honestly just don't give up. Don't give up. Don't!</p>
          <p><span class="transcript-speaker">Bennie:</span> I always said when I was hired when you put your mind to doing something. You can do it. It's a beautiful thing when you can put yourself in the situation and come out a winner.</p>

          <h2>Final Thoughts</h2>
          <p><span class="transcript-speaker">Zach:</span> It felt really great to know that I would be working again and I'm really happy with what I'm doing right now.</p>
          <p><span class="transcript-speaker">Laura:</span> When it felt like to get the phone call of the job offer it felt amazing. It felt like a dream, like I was living in a dream. I had to make sure I wasn't sleeping or something. But I was very excited. I was stunned.</p>
          <p><span class="transcript-speaker">James:</span> I felt like it wasn't really happening like that maybe I was having a dream so I was surprised and grateful.</p>
          <p><span class="transcript-speaker">Bennie:</span> When I do my time and I leave, I feel real good. When I walk in there in the morning, it seemed like I'd come there to help people and it makes me feel really good. The end of my day is just beautiful. I can walk out of there with a smile.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c1-t1-r5",
      categoryId: "c1",
      topicId: "c1-t1",
      title: "A Job vs. Career",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t1-r5_eht3pp.jpg",
      thumbnailDescription:
        "Young woman is wearing a Lifetime Fitness uniform.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1696523474/audio/c1-t1-r5_01_pswf48.mp3",
        text: `
        <div class="article">
          <p>Remember, a job is a specific set of tasks that an employer pays you to do. A career is a series of jobs that give you a chance to continue building your skills. A career is typically aligned to a person's long-term goals and provides opportunity for personal and professional growth.</p>
          <p>It's important to make a plan so your career stays on track. Your career plan may change as your interests, skills, and dreams change and grow. You may need to work at a job that you don't particularly enjoy or want to do in order to earn money, gain work experience, or get a positive reference that will help prove you're a good worker.</p>
          <p>Your career plan also should include short-term and long-term goals for:</p>
          <ul>
            <li>Getting the education or training you need.</li>
            <li>Developing skills.</li>
            <li>Gaining experience.</li>
            <li>Working in specific jobs within the career you've chosen.</li>
          </ul>

          <h2>Long-term goals</h2>
          <p>Long-term goals may take several years to achieve. Examples include:</p>
          <ul>
            <li>Getting a college degree.</li>
            <li>Getting a license for a skilled trade.</li>
            <li>Completing specialized training.</li>
            <li>Building new skills.</li>
          </ul>

          <h2>Short-term goals</h2>
          <p>Sometimes you have to complete several smaller steps which help you move closer to your long-term goal. These are short-term goals. Examples include:</p>
          <ul>
            <li>Getting your high school diploma or completing your GED.</li>
            <li>Getting a driver's license.</li>
            <li>Learning how to use a specific computer program.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t2-r1",
      categoryId: "c1",
      topicId: "c1-t2",
      title: "Discovery Process",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t2-r1_eole5h.jpg",
      thumbnailDescription: "Young man is working at Culver's Restaurant.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1696523469/audio/c1-t2-r1_01_caur6r.mp3",
        text: `
        <div class="article">
          <p>It's no surprise that people are more satisfied when they work in jobs that match their:</p>
          <ul>
            <li>Interests and work values.</li>
            <li>Personality.</li>
            <li>Skills.</li>
            <li>Education and experience.</li>
          </ul>
          <p>A successful career takes all of these things into account. You can learn about your interests and values, personality and skills, by taking self-assessments and reflecting. Now let's take a closer look at each element of this Discovery Process:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t2-r2",
      categoryId: "c1",
      topicId: "c1-t2",
      title: "Interests and Work Values",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t2-r2_tf6xgz.jpg",
      thumbnailDescription: "Keyboard with one of the keys labeled Assessment.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1696523474/audio/c1-t2-r2_01_vavrlt.mp3",
        text: `
        <div class="article">
          <p>Knowing what interests you will help you find jobs or careers that you might enjoy. This will increase your chances of success. Start by thinking about what you like to do in your free time, your favorite classes, and past jobs that you enjoyed or where you were successful.</p>

          <h2>Personal Interests</h2>
          <p>Most interests fall into four categories.</p>
          <ol>
            <li>Helping others.</li>
            <li>Numbers, data and technology.</li>
            <li>Hands-on activities.</li>
            <li>Creative and artistic activities.</li>
          </ol>

          <h2>Personal and Work Values</h2>
          <p>It's important to think about the things you value. Your values are what you believe are important to live by. Everyone values different things. What's important to you in a job and in your life? Do you want to:</p>
          <ul>
            <li>Be in charge?</li>
            <li>Have a lot of responsibility?</li>
            <li>Have the chance to find new ways to do things?</li>
            <li>Help others?</li>
            <li>Make a lot of money?</li>
            <li>Do a lot of different things during your workday?</li>
            <li>Be able to work independently?</li>
            <li>Have a flexible work schedule?</li>
            <li>Make a difference in your community?</li>
          </ul>
          <p>If your job is in line with the things that you believe in, you'll probably be more successful. You can complete a free online assessment to help you determine what's important in your work life.</p>

          <h2>Interest inventories and assessments</h2>
          <p>You also can take a self-assessment to learn about your interests. Interest assessments, or “interest inventories,” are not tests. There are no right or wrong answers! You can use the results to discover:</p>
          <ul>
            <li>What you like and don't like to do.</li>
            <li>How your interests match different careers.</li>
            <li>There are many free interest assessments available online.</li>
          </ul>

          <h2>Where to go for help online</h2>
          <p>There are many free interest assessments available on the Internet. Check out:</p>
          <ul>
            <li><a href="https://www.mynextmove.org/" target="_blank">My Next Move</a></li>
            <li><a href="https://careerwise.minnstate.edu/careers/interestassessment.html" target="_blank">MNCAREERS Interest Assessment</a></li>
            <li><a href="https://www.onetcenter.org/IP.html" target="_blank">O*NET Interest Profiler</a></li>
            <li><a href="https://www.onetcenter.org/WIL.html" target="_blank">O*NET Work Importance Profiler</a></li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t2-r3",
      categoryId: "c1",
      topicId: "c1-t2",
      title: "Your Personality",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t2-r3_jrrcfq.jpg",
      thumbnailDescription: "Black person is working at a restaurant.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1696523470/audio/c1-t2-r3_01_cqdefl.mp3",
        text: `
        <div class="article">
          <p>Consider your personality and how this connects with a job or career you would enjoy. For example, if you're outgoing and enjoy working with other people, a job in sales might be a great fit. However, you probably wouldn't be as happy working as a medical device assembler because they often work alone.</p>
          <p>So, who are you?</p>
          <p>You can take an online personality assessment to learn about yourself and your work style. The Myers-Briggs Type Indicator, or MBTI, is one of the most common. It analyzes four things:</p>
          <ol>
            <li>The activities that make you happy.</li>
            <li>How you learn.</li>
            <li>How you make decisions</li>
            <li>Your values.</li>
          </ol>
          <p>You can take the Myers-Briggs test online at <a href="https://www.careertest.net/" target="_blank">www.careertest.net</a>. Try it out yourself!</p>
          <p>After you get your results, be sure to click on the description link to learn more about your personality type and some of the jobs that you might want to check out.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t2-r4",
      categoryId: "c1",
      topicId: "c1-t2",
      title: "Skills and Abilities",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568054/images/c1-t2-r4_urpqc5.jpg",
      thumbnailDescription: "Woman on the phone is looking at a computer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658038/audio/c1-t2-r4_02_ekwrvd.mp3",
        text: `
        <div class="article">
          <p>A skill is something you can do.</p>
          <p>Every career requires specific skills and you often use several different skills to accomplish a single task.
          Understanding your skills will help you:</p>
          <ul>
            <li>Identify the kinds of jobs you can do now.</li>
            <li>Identify what skills you will need to reach your goals so you can do other jobs in the future.</li>
          </ul>
          <p>There are many different kinds of skills.</p>

          <h2>Basic Skills</h2>
          <p>These are important skills that most workers need. Examples include:</p>
          <ul>
            <li>Communication (verbal, nonverbal).</li>
            <li>Being able to listen, ask questions and understand what you're being told.</li>
            <li>Basic math, reading and writing.</li>
            <li>Open to learning new things.</li>
            <li>Working with others.</li>
          </ul>

          <h2>Job or Technical Skills</h2>
          <p>Job or technical skills are specific things an employer needs an employee to do in a specific job. Examples include:</p>
          <ul>
            <li>Operating a forklift.</li>
            <li>Using a keyboard.</li>
            <li>Using a specific computer program or Internet site.</li>
            <li>Answering telephones.</li>
            <li>Maintaining equipment.</li>
            <li>Preparing food.</li>
            <li>Balancing a checking account.</li>
          </ul>

          <h2>Personal and Social Skills</h2>
          <p>You use these skills to get along with other people, stay on task and solve problems. Examples include:</p>
          <ul>
            <li>Teamwork.</li>
            <li>Leadership.</li>
            <li>Acceptance of others.</li>
            <li>Positive attitude.</li>
            <li>Hard working.</li>
            <li>Creativity.</li>
            <li>Punctuality (being on time).</li>
          </ul>

          <h2>Management Skills</h2>
          <p>Management skills help you set priorities and organize, and use information, projects or time. Examples include:</p>
          <ul>
            <li>Supervising other people.</li>
            <li>Managing a budget.</li>
            <li>Meeting deadlines.</li>
            <li>Purchasing materials.</li>
            <li>Prioritizing and completing key tasks.</li>
          </ul>

          <h2>Transferrable Skills</h2>
          <p>These are skills that you learn at one job or in one part of your life that can be applied to a new job. For example, if you would like a supervisory job but have never worked as a supervisor before, you may have learned some of the skills you need through other experiences like coaching a sports team or teaching a new co-worker how to do a task. Examples include:</p>
          <ul>
            <li>Supervisory experience.</li>
            <li>Attention to detail.</li>
            <li>Technology skills.</li>
            <li>Writing – proofreading, letters, reports, etc.</li>
            <li>Speaking more than one language.</li>
            <li>Basic bookkeeping or budgeting.</li>
            <li>Customer service experiences.</li>
          </ul>

          <h2>Building Skills</h2>
          <p>The great thing about skills is that they can be learned and improved! You can:</p>
          <ul>
            <li>Take classes online or at a community or technical college.</li>
            <li>Get specialized training.</li>
            <li>Take on new job responsibilities.</li>
            <li>Ask someone to teach you a new skill.</li>
            <li>Work at an internship or apprenticeship.</li>
            <li>Find resources in your community.</li>
            <li>Volunteer or shadow someone doing a job.</li>
          </ul>

          <h2>Identifying Skills</h2>
          <p>A big part of the process of getting a job is showing that you have confidence in yourself and your skills. There are many ways to identify your skills. You can:</p>
          <ul>
            <li>Complete an online skills assessment.</li>
            <li>Make a list of the things you can do by examining your volunteer activities, work, education, and personal history.</li>
            <li>Review a general list of skills to see if there are any you should add to your list.</li>
            <li>Ask a friend, family member, or other person who knows you well to help you create a list of things you can do.</li>
          </ul>

          <h2>Online Skills Assessments</h2>
          <p>Check out these online tools to help you assess your skills:</p>
          <ul>
            <li><a href="https://careerwise.minnstate.edu/careers/interestassessment.html" target="_blank">iSeek Skills Assessment</a> (currently MNCareers Interest Assessment)</li>
            <li><a href="https://www.onetonline.org/skills/soft/" target="_blank">O*NET</a></li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t2-r5",
      categoryId: "c1",
      topicId: "c1-t2",
      title: "Education and Past Experiences",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t2-r5_gtejsy.jpg",
      thumbnailDescription: "Young man is in a classroom setting.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1696523473/audio/c1-t2-r5_01_yrcimw.mp3",
        text: `
        <div class="article">
          <p>Finally, you need to take a look at your education and past experiences to decide if you have the right education and experience or if you need more education or training.</p>
          <h2>Education</h2>
          <p>Most job postings in the United States require a high school degree. If you don't have a high school degree, consider finishing high school or taking the General Education Development (GED) test.
          There's often a link between your education and the jobs you will be considered for. Sometimes, more education can mean higher pay.</p>
          <h2>Certifications and Special Training</h2>
          <p>Special training or industry certifications can help you stand out from the competition. Special training can include:</p>
          <ul>
            <li>Seminars.</li>
            <li>Workshops.</li>
            <li>Classes.</li>
            <li>Apprenticeships.</li>
            <li>Certificate programs.</li>
          </ul>
          <h2>Past Experiences</h2>
          <p>Potential employers will want to know about your relevant work and personal experiences. This includes more than just paid jobs. Be sure to think about your:</p>
          <ul>
            <li>Hobbies.</li>
            <li>Life experiences.</li>
            <li>Community involvement.</li>
            <li>Volunteer activities.</li>
            <li>Classes you took in school.</li>
            <li>Extracurricular activities.</li>
            <li>Technology skills.</li>
          </ul>
          <p>Your goal is to highlight experiences and strengths that relate to the job you're applying for.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t3-r1",
      categoryId: "c1",
      topicId: "c1-t3",
      title: "Exploring Careers That Interest You",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568054/images/c1-t3-r1_y9gudg.jpg",
      thumbnailDescription:
        "Sign post outside displaying three signs with the word career and pointing in different directions.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598359/audio/c1-t3-r1_tylcv3.mp3",
        text: `
        <div class="article">
          <p>Before you start looking for a job, it's important to focus on careers or industries that interest you.</p>
          <p>There are many career exploration web sites and tools online. Most of them are free. Because there are so many kinds of jobs, most of the sites group career choices by “career families” or “career clusters” that share common features.</p>
          <p>Be sure to learn about:</p>
          <ul>
            <li>Several different careers.</li>
            <li>What people do in those careers.</li>
            <li>Education that you'd need.</li>
            <li>Required skills, abilities, and previous experience.</li>
            <li>Salaries.</li>
            <li>A career's growth potential.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t3-r2",
      categoryId: "c1",
      topicId: "c1-t3",
      title: "What's Right for You?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568054/images/c1-t3-r2_imo7la.jpg",
      thumbnailDescription: "O Net Logo.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658008/audio/c1-t3-r2_02_wyepn5.mp3",
        text: `
        <div class="article">
          <p>Researching various jobs will help you decide if your skills and interests match the career you've chosen. Here are four ways to learn more about a particular field and find out if it's right for you.</p>
          <h2>Online Resources</h2>
          <p>Web sites like O-Net, the U.S. Department of Labor's job search web site, describe many different jobs. Most states also have their own career exploration or job search web sites. These sites can help you learn:</p>
          <ul>
            <li>What you would do in a job.</li>
            <li>The skills and abilities you need to do a job.</li>
            <li>The average salary you can expect.</li>
            <li>The education and experience that you need.</li>
            <li>The job's outlook and common career paths.</li>
            <li>Where to get more information.</li>
          </ul>
          <p>Check out these websites:</p>
          <ul>
            <li><a href="https://www.onetonline.org/" target="_blank">O*NET</a></li>
            <li><a href="https://careerwise.minnstate.edu/" target="_blank">iSeek</a></li>
            <li><a href="https://www.mynextmove.org/" target="_blank">MyNextMove.org</a></li>
            <li><a href="https://www.careeronestop.org/" target="_blank">CareerOneStop</a></li>
          </ul>
          <h2>Work with a Placement Specialist</h2>
          <p>An employment specialist can help you learn more about different kinds of work that might work for you, based on your interests, education, and skills. They can assist you with:</p>
          <ul>
            <li>Finding out what jobs are available in the area you live.</li>
            <li>Matching you with open jobs that best fit your skills and personality.</li>
            <li>Filling out and submitting necessary paperwork.</li>
            <li>Scheduling orientation or training as needed.</li>
          </ul>
          <h2>Job Shadowing</h2>
          <p>You can watch someone do a job to learn more about what it is really like. This is called job shadowing and is usually unpaid. You can shadow someone you know or you can call or stop in at a business to find out if they would let you shadow someone on the job.</p>
          <p>When you shadow someone while they work, watch quietly, be polite, and ask questions at times that won't be disruptive. Sometimes you might even be given a chance to help out.</p>
          <h2>Talk to People You Know</h2>
          <p>Talking to people you know is a great way to learn about jobs and careers! Ask your family, friends and neighbors about the jobs they have now and jobs they've done in the past. Find out:</p>
          <ul>
            <li>What they do.</li>
            <li>What they like and don't like about their jobs.</li>
            <li>How they found their jobs.</li>
            <li>The education they needed to get the job.</li>
            <li>Suggestions they have for someone who wants to get a similar job.</li>
          </ul>
          <p>This is called networking.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c1-t3-r3",
      categoryId: "c1",
      topicId: "c1-t3",
      title: "Your Job Requirements",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568053/images/c1-t3-r3_xbkyxz.jpg",
      thumbnailDescription:
        "Woman operating a machine with a supervisor overseeing.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598360/audio/c1-t3-r3_ekod3v.mp3",
        text: `
        <div class="article">
          <p>No job is perfect so it's important to be realistic. It helps to know what you need from a job and what you want from a job.</p>
          <p>Wants and needs aren't always the same thing.</p>
          <p>The things that you need from a job are your job requirements because they have to be part of the job or it won't work for you.</p>
          <p>The things you want from a job are your preferences because they are things that you'd like the job to offer but they aren't essential.</p>
          <h2>Example of a Need:</h2>
          <p>Public transportation will be essential if you don't have a driver's license or access to a car. If a potential employer isn't located near a bus route, there's a good chance the job won't work for you.</p>
          <h2>Example of a Want:</h2>
          <p>You prefer to start work at 9:30 a.m. so you can sleep late. The job you're applying for requires employees to start at 8:00 a.m. In this case, starting at 9:30 would be considered a “want” because you don't need to sleep later, you just want to!</p>
          <h2>Hours</h2>
          <ul>
            <li>How many hours per week can you work or want to work?</li>
            <li>Do you want to work part-time or full-time?</li>
            <li>Are there things you should take into account, like childcare arrangements, regular appointments, or classes?</li>
          </ul>
          <h2>Days</h2>
          <ul>
            <li>Can you work every day of the week?</li>
            <li>Are there any days you can't work?</li>
            <li>Are you willing to work weekends and holidays?</li>
          </ul>
          <h2>Shifts</h2>
          <ul>
            <li>Are you able to work any shift? Work shifts are sometimes called 1st shift (days), 2nd shift (late afternoon through the evening), and 3rd shift (overnight)?</li>
          </ul>
          <h2>Salary/Benefits</h2>
          <ul>
            <li>What salary do you need to make?</li>
            <li>What can you realistically expect to earn?</li>
            <li>Do you need benefits like health care insurance?</li>
          </ul>
          <h2>Location</h2>
          <ul>
            <li>Does your job need to be close to home?</li>
            <li>How long are you willing to travel to get to and from work?</li>
          </ul>
          <h2>Transportation</h2>
          <ul>
            <li>How will you get to work?</li>
            <li>Do you need a job that is close to a bus line or other kinds of public transportation?</li>
            <li>Do you have a reliable car or ride?</li>
          </ul>
          <h2>Work Environment</h2>
          <ul>
            <li>What kind of work environment is best for you?</li>
            <li>Do you want to work outdoors or indoors?</li>
          </ul>
          <h2>Interactions with Other People</h2>
          <ul>
            <li>Are you a “people person” who likes to work with many people?</li>
            <li>Do you prefer to work alone or with one or two others?</li>
            <li>Do you want to work with customers?</li>
          </ul>
          <h2>Type of Tasks</h2>
          <ul>
            <li>Do you need to move around during the day or stay in one place?</li>
            <li>Work with computers?</li>
            <li>Do something physical, like landscaping or maintenance?</li>
          </ul>
          <h2>Physical Abilities</h2>
          <ul>
            <li>Do you have any physical limitations? Some job postings list specific physical requirements, such as being able to stand for long periods of time or lift a specific number of pounds.</li>
          </ul>
          <h2>Accommodations and Supports</h2>
          <ul>
            <li>Will you need any accommodations? Employers are required by law to make “reasonable accommodations” so qualified employees with disabilities have an equal chance to be successful.</li>
          </ul>
          <p><b>Note:</b> Many employers offer “natural supports” that are available to all employees. These can help you be successful at work.</p>
        </div>`,
      },
      keywords: [],
    },
    // Category 2 resources
    {
      id: "c2-t1-r1",
      categoryId: "c2",
      topicId: "c2-t1",
      title: "Think C-A-S-H",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568061/images/c2-t1-r1_rbqskb.jpg",
      thumbnailDescription:
        "Application background with the word C-A-S-H overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533193/audio/c2-t1-r1_p8lzq9.mp3",
        text: `
        <div class="article">
          <p>Think C-A-S-H when you're filling out applications and other job-related paperwork.</p>
          <h2>CLEAR</h2>
          <p>Make it easy for a potential employer to read and understand your application or resumé.</p>
          <h2>ACCURATE</h2>
          <p>Everything that you include on your application or resumé must be correct. This includes spelling, dates for past employment, your education, skills, job titles, and information related to your criminal history if you have one. Most employers will conduct a background check to verify the information you've provided.</p>
          <h2>SIMPLE</h2>
          <p>Present all information as simply as possible. Use bullets, simple language, active words, and short sentences.</p>
          <h2>HONEST</h2>
          <p>Your goal is to present yourself, your skills and past experiences in the best possible light. However, don't exaggerate. You don't want to be hired based on skills, or experiences that you don't have!</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t1-r2",
      categoryId: "c2",
      topicId: "c2-t1",
      title: "Common Job Hunt Paperwork",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568061/images/c2-t1-r2_e0sd1q.jpg",
      thumbnailDescription:
        "Person at a computer with various icons of paperwork overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533191/audio/c2-t1-r2_k1bhxu.mp3",
        text: `
        <div class="article">
          <p>There are five common forms of paperwork that you may use during your job hunt.</p>
          <h2>A Job Application</h2>
          <p>A job application is a common way for an employer to gather the same information about every person who wants to be considered for a job.</p>
          <p>Most job applications capture an applicant's:</p>
          <ul>
            <li>Personal information.</li>
            <li>Work history.</li>
            <li>Salary requirements.</li>
            <li>Education.</li>
            <li>Criminal history.</li>
            <li>Personal references.</li>
          </ul>
          <h2>Resumé</h2>
          <p>Resumés and job applications contain a lot of the same information. However, they are not the same thing:</p>
          <ul>
            <li>A job application gathers information that the employer wants to know and that you and other applicants are asked to provide.</li>
            <li>A resumé highlights what you want the employer to know about you, especially the experiences, education, and personal qualities that set you apart from other applicants.</li>
          </ul>
          <h2>Cover Letter</h2>
          <p>A cover letter tells a potential employer a few key reasons why you should be considered for the job! You should include a cover letter with any resumé that you give to a potential employer, any completed job application that you send in the mail, or any online application that you submit.</p>
          <h2>Letter of Explanation</h2>
          <p>Letters of explanation help employers understand things on your application or resumé that may raise questions about your ability to do the job, such as a gap in employment or criminal history. A letter of explanation should be simple, honest, and matter-of-fact.</p>
          <h2>Work Samples or Portfolios</h2>
          <p>You can help an employer better understand your skills by providing samples of your work that highlight your experience, skills, how you would do a task, etc.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t2-r1",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "The Purpose of the Job Application",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568061/images/c2-t2-r1_yh8yk2.jpg",
      thumbnailDescription:
        "Clipboard with a piece of paper that says Application for Employment.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533191/audio/c2-t2-r1_vjbymh.mp3",
        text: `
        <div class="article">
          <p>Employers use job applications to:</p>
          <ol>
            <li>Gather the same information about every person who applies for a specific job so they can compare candidates more easily.</li>
            <li>Decide which candidates might be best qualified for the job.</li>
            <li>Select people to interview.</li>
          </ol>
          <p>Even though job applications and resumés contain much of the same information, they're not the same thing! If an employer asks you to fill out an application, you can't just provide your resumé. You must fill out the application if you want to be considered for the job.</p>
          <h2>Contents of a Job Application</h2>
          <p>Most job applications ask for the same information. However, they may ask for the information to be entered in a specific way, such as entering your last name first. That's why it's important to review the entire application carefully so you know exactly what information the employer is requesting.</p>
          <p>Most applications will ask:</p>
          <ul>
            <li>For your name, address, and telephone number.</li>
            <li>For your Social Security number.</li>
            <li>The title of the position you're applying for.</li>
            <li>Which shifts you're willing to work and when you can start.</li>
            <li>For your work experience and education.</li>
            <li>If you can prove that you are eligible to work.</li>
            <li>For information related to salary requirements and criminal history.</li>
            <li>For two or three personal references, as well as their contact information.</li>
          </ul>
          <p>You'll also be asked to sign a statement certifying that the information you've provided is accurate and correct.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t2-r2",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Types of Job Applications",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568061/images/c2-t2-r2_bzlysf.jpg",
      thumbnailDescription:
        "Person completing an online application on a computer screen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658002/audio/c2-t2-r2_01_a4kacc.mp3",
        text: `
        <div class="article">
          <p>You can complete a job application in different ways.</p>
          <p>For many years, all job applications were paper forms. However, now many organizations and businesses allow applicants to apply for jobs online. Some even require it!</p>
          <p>Here are the most common ways to obtain and submit job applications:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r3",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Paper Applications",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568061/images/c2-t2-r3_zk1aow.jpg",
      thumbnailDescription: "Paper application for employment.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533191/audio/c2-t2-r3_d55gon.mp3",
        text: `
        <div class="article">
          <p>A paper application is a printed form that you fill out and return to the employer in order to be considered for a specific job. If you get the job, your completed job application may be placed in your personnel file.</p>
          <p>You can:</p>
          <ul>
            <li>Ask for and fill out an application at the employer's location.</li>
            <li>Ask for an application at an employer's location, then complete it elsewhere and return it to the employer either in person or by mail.</li>
            <li>Go online, then download and print out a copy of the application to fill in and return to the employer.</li>
          </ul>
          <p>If you complete an application at an employer's location:</p>
          <ul>
            <li>Dress appropriately.</li>
            <li>Be polite and professional.</li>
            <li>Bring a copy of your Master Application for reference.</li>
            <li>Go alone…friends should wait outside.</li>
            <li>Turn off your cell phone and remove ear buds, Bluetooth devices, etc.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r4",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Online Applications",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r4_qcfiwt.jpg",
      thumbnailDescription:
        "Close-up of an online application on a computer screen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658005/audio/c2-t2-r4_01_awhbkn.mp3",
        text: `
        <div class="article">
          <p>An online application is a computerized form that captures your information electronically and automatically submits it to the employer through the Internet.</p>
          <p>More and more organizations have converted their process for applying for jobs from paper forms to an online process.</p>
          <p>Many online applications require you to create a profile before completing an application. This profile may simply gather your contact information or it may be more extensive and ask you to enter information about your work experience, the types of jobs that you're interested in, etc. You also will be required to create a User Name and Password that you can use to check on the status of your application later.</p>
          <p>You can complete and submit an application online by:</p>
          <ul>
            <li>Going to an employer's website and following the directions on the screen.</li>
            <li>Filling out an electronic application at an employment kiosk at an employer's location.</li>
            <li>Submitting applications through an employment website such as <a href="https://www.indeed.com/" target="_blank">Indeed.com</a>.</li>
          </ul>
          <p>The employer may ask you to upload a cover letter and resumé as part of your online application.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t2-r5",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Sensitive Questions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r5_ejjmzp.jpg",
      thumbnailDescription: "A black man is thoughtfully thinking.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658004/audio/c2-t2-r5_01_cioi0o.mp3",
        text: `
        <div class="article">
          <p>Most job applications ask several questions that may be difficult or uncomfortable to answer. You should think about how you'll answer these questions before you begin filling out job applications. These tough questions often relate to:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r6",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Your Reasons for Leaving a Current or Past Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r6_lul86t.jpg",
      thumbnailDescription:
        "A woman in a wheelchair talking with an interviewer and the word FIRED with a red X is overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658006/audio/c2-t2-r6_01_b8epai.mp3",
        text: `
        <div class="article">
          <p>Many applications will ask why you left a previous job. Respond truthfully and positively. Try not to say: "Fired," "Quit," or "Illness."</p>
          <p>Here are some more specific and more positive responses:</p>
          <p>If you resigned, consider saying:</p>
          <ul>
            <li>“Resigned to accept a different position.”</li>
            <li>“Resigned to return to school.”</li>
            <li>“Resigned to relocate to another area.”</li>
            <li>“Resigned due to health needs.”</li>
          </ul>
          <p>If you were laid off, consider saying:</p>
          <ul>
            <li>“Staff reduction.”</li>
            <li>“Reduction in force.”</li>
            <li>“Laid off due to lack of work.”</li>
            <li>“Laid off because the job was seasonal.”</li>
            <li>“Laid off because the location was closing.”</li>
            <li>“Laid off because of corporate downsizing.”</li>
          </ul>
          <p>If you were fired, simply say:</p>
          <ul>
            <li>“Terminated.”</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r7",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Employment Gaps",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r7_zkr9ii.jpg",
      thumbnailDescription:
        "Work experience section of an application with a red arrow pointing to a gap.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533192/audio/c2-t2-r7_gku0p8.mp3",
        text: `
        <div class="article">
          <p>When employers notice a gap in an applicant's work history, they usually want to know why the person was unemployed for a long period of time.</p>
          <p>If you have gaps in your work history, describe the time you were unemployed as positively as possible.</p>
          <p>Keep your answers brief and simple. Examples include:</p>
          <ul>
            <li>“Caring for family/household.”</li>
            <li>“Attending school.”</li>
          </ul>
          <p>Describe what you were doing while you were unemployed. Examples include:</p>
          <ul>
            <li>Volunteer activities.</li>
            <li>Training.</li>
            <li>Skills development.</li>
          </ul>
          <p>You also can address a gap in employment in a letter of explanation.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r8",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Wage or Salary Expectations",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r8_lcjn5k.jpg",
      thumbnailDescription: "Employee being handed a paycheck.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658008/audio/c2-t2-r8_01_mnh732.mp3",
        text: `
        <div class="article">
          <p>Due to laws and regulations to address inequity in pay, employers are less likely to ask about your salary history or specific requirements on the application. Instead, this may be negotiated as part of the interview and hiring process. However, some employers may still ask you to provide a salary or pay range on the application. Be prepared to provide a range (for example $20-25 per hour) that is considerate of your experience and the typical pay range for this type of job.</p>
          <p>You can research pay ranges for the type of job you are applying for by asking people who are already doing the job what they're being paid, talking to your job placement specialist for advice or checking online resources.</p>
          <p>If the application doesn't require a specific number, write “open,” “negotiable,” or “comfortable with starting wage.”</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r9",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Legal Authorization to Work",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r9_xits7q.jpg",
      thumbnailDescription:
        "A young female interviewer with the words Race, Religion, Disability and a red X overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658006/audio/c2-t2-r9_01_cv3b09.mp3",
        text: `
        <div class="article">
          <p>On job applications, employers will ask if you are legally authorized to work in this country. You may be required to provide documentation, so be sure to answer honestly. You are legally authorized to work in the United States if:</p>
          <ul>
            <li>You have a US birth certificate or</li>
            <li>Are an immigrant who has the necessary work authorization, permit or visa.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r10",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Disabilities and Your Need for Accommodations",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r10_qxmp5g.jpg",
      thumbnailDescription:
        "ADA background overlaid with the words reasonable accommodations.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658009/audio/c2-t2-r10_02_locdr4.mp3",
        text: `
        <div class="article">
          <p>Even though a job application should not include questions about disabilities, some do. The question may be worded something like, “Can you perform the functions of this job with or without accommodations?” Your job placement specialist can help you decide the best way to respond.</p>
          <p>The only time that you are required to disclose a disability is if it will make it difficult for you to do the job without some adjustment or modification. You can discuss accommodations you might need during an interview or after you're been hired.</p>
          <p>It's important to know that all employers are required by law to make “reasonable accommodations” to help make sure that all applicants have an equal chance of success.</p>
          <p>Some job applications have Equal Employment Opportunity (EEO) survey questions at the end of the application. The EEO Commission is a federal agency that helps to enforce discrimination laws and keep employers from discriminating against people based on their age, gender, national origin or citizenship, race, disability and religion. The form is voluntary, meaning you can choose whether to answer or not.  Your choice cannot be held against you by the potential employer. These types of questions are still prohibited or illegal during an interview.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t2-r11",
      categoryId: "c2",
      topicId: "c2-t2",
      title: "Criminal History",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t2-r11_tpekxq.jpg",
      thumbnailDescription:
        "Stack of papers with the words Criminal Conviction overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658011/audio/c2-t2-r11_02_g9kn3j.mp3",
        text: `
        <div class="article">
          <p>If you have a criminal history, it may be tempting to skip this section. DON'T! Remember, you're required to answer all legal questions on a job application as honestly as possible. Plus, most employers routinely verify information on an applicant's background, so the truth will come out!</p>
          <p>Here are some things to keep in mind if you have a criminal history:</p>
          <h2>Be Honest</h2>
          <ul>
            <li>Answer truthfully if the question applies to you. Consider attaching a Letter of Explanation to explain the situation and the steps you've taken to grow and learn since then.</li>
          </ul>
          <h2>Attach Written Recommendations</h2>
          <ul>
            <li>Letters of recommendation from former employers, people at organizations where you have volunteered, former teachers, and others who know you well can help prove that you can do the work and should be trusted.</li>
          </ul>
          <h2>Consider the Types of Jobs You'll Apply For</h2>
          <ul>
            <li>Many companies will consider hiring someone who has been convicted of a crime if it doesn't relate directly to the job you're applying for or the company's business. For example, a bank might be more worried about hiring someone who has been convicted of a financial crime than a landscaping firm might be.</li>
          </ul>
          <h2>Read the Question Carefully Before Answering</h2>
          <ul>
            <li>If you don't have a criminal history, simply write “N/A” or “Not Applicable.”</li>
            <li>Make sure that you understand the question. Does it ask if you've ever been convicted of a felony? A misdemeanor? A crime? Some applications may ask if you have been charged with a crime or have ever been arrested.</li>
            <li>The way the question is worded can make a big difference in the way you answer! Say, for example, that you have been convicted of a misdemeanor but not a felony. If the application asks, “Have you ever been convicted of a felony?” you can honestly answer “No.” If the application asks, “Have you ever been convicted of a misdemeanor?” you should answer “Yes.”</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t3-r1",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "What You Need to Know About Resumés",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t3-r1_a9c7rg.jpg",
      thumbnailDescription:
        "A man and woman reading a resume while sitting in front of a young woman.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533192/audio/c2-t3-r1_msgnou.mp3",
        text: `
        <div class="article">
          <p>A resumé is typically a written document that contains important information that you want an employer to know about you. It's an important job search tool for every job seeker, not just people who are applying for high level, professional jobs or who have a lot of experience or education.</p>
          <p>A resumé has many uses. You can:</p>
          <ul>
            <li>Customize it to highlight things that show you're the best candidate for a specific job.</li>
            <li>Bring it with you when you apply for a job in case the employer wants to interview you on the spot.</li>
            <li>Attach a copy, along with a cover letter requesting an interview, when applying for job openings.</li>
            <li>Upload and submit a copy when applying for jobs online.</li>
            <li>Send a copy along with a cover letter to request an interview or inquire about job openings.</li>
            <li>Give it to people you know so they can tell you if they hear about jobs you might be interested in.</li>
          </ul>
          <p>Learn more:</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r2",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "What Every Resumé Should Include",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t3-r2_gy1asu.jpg",
      thumbnailDescription: "A person holding a resume.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658009/audio/c2-t3-r2_01_nn9y41.mp3",
        text: `
        <div class="article">
          <p>Every resumé should include:</p>
          <h2>1. Your Personal Information</h2>
          <ul>
            <li>Your name.</li>
            <li>Mailing address.</li>
            <li>A telephone number, with area code, where you can be reached.</li>
            <li>Your email address.</li>
          </ul>
          <h2>2. Key Skills</h2>
          <ul>
            <li>Identify or describe your key skills. Always emphasize skills that the employer is looking for.</li>
          </ul>
          <h2>3. Work and Volunteer Experience</h2>
          <ul>
            <li>List your work, internships, community service projects or volunteer experiences, beginning with your most recent. Be sure to include:</li>
            <ul>
              <li>Each employer's name and address.</li>
              <li>The range you were there.</li>
              <li>Your job title.</li>
              <li>Your job duties, including key accomplishments.</li>
            </ul>
          </ul>
          <h2>4. Education</h2>
          <ul>
            <li>Start with your highest level of education, then work backwards. Include the name of the institution, the city where it is located, any degree or certificate earned, and the year you graduated. If you attended or graduated from college, include your major.</li>
            <li>If you have not yet graduated from high school, you can put the name of your school, location and expected graduation date. You can also mention any awards, honors or relevant coursework.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r3",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "Other Things to Consider for Your Resumé",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t3-r3_nmfu2t.jpg",
      thumbnailDescription: "A person holding a resume.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697767550/audio/c2-t3-r3_02_wccura.mp3",
        text: `
        <div class="article">
          <p>If you have room, you can include the following optional information:</p>
          <h2>1. Objective or Summary Statement</h2>
          <ul>
            <li>A specific employment objective that directly relates to the position you're applying for or</li>
            <li>A summary statement that shares your goals, interests and hopes for you career.</li>
          </ul>
          <h2>2. Important Accomplishments</h2>
          <ul>
            <li>Briefly list your key accomplishments.</li>
          </ul>
          <h2>3. References</h2>
          <ul>
            <li>Include the name and contact information for two or three professional references. If space is limited, indicate that you can provide references upon request.</li>
          </ul>
          <h2>4. Special Interests or Achievements</h2>
          <ul>
            <li>Including special interests or achievements is a good way for people with limited work history to highlight experiences or skills that they have learned outside of paid work.</li>
          </ul>
          <h2>5.	Skills</h2>
          <ul>
            <li>Identify any skills you have that are relevant to the job and how you will work with other people. For example, you might include:</li>
            <ul>
              <li>Speaking, reading or writing in another language.</li>
              <li>Technology knowledge and abilities.</li>
              <li>Workshops and special training that don't fit under the Education heading.</li>
              <li>Transferable skills such as communication, teamwork, flexibility, problem-solving, organization, time-management, emotional intelligence and stress management.</li>
            </ul>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r4",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "Tips for Creating a Good Resumé",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t3-r4_ivl2iu.jpg",
      thumbnailDescription:
        "A person reading a resume and typing in a computer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658010/audio/c2-t3-r4_01_ds48um.mp3",
        text: `
        <div class="article">
          <p>To make your resumé shine:</p>
          <h2>Keep it short, neat and easy to read</h2>
          <ul>
            <li>Use active words.</li>
            <li>One page is best but your resumé should not be more than two pages.</li>
            <li>It should be typed using a simple 11- or 12-point typeface and printed on standard white or ivory resumé paper.</li>
            <li>Don't fold your resumé. If you're mailing or dropping off a copy to a potential employer, use a 9” x 12” envelope.</li>
          </ul>
          <h2>Be sure the information is accurate</h2>
          <ul>
            <li>Be honest. Don't exaggerate or overstate your qualifications or experience.</li>
            <li>Proofread your resumé carefully for spelling errors and correct punctuation.</li>
            <li>Update your resumé regularly.</li>
          </ul>
          <h2>Make sure it is professional</h2>
          <ul>
          <li>Don't use short-hand or slang terms.</li>
          <li>Limit abbreviations.</li>
          </ul>
          <h2>Focus on the employer's needs</h2>
          <ul>
            <li>Don't include every skill or accomplishment. Instead, choose the ones that directly relate to the job you're applying for.</li>
            <li>Use the key words and phrases that the employer used in the job posting.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r5",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "Types of Resumés",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568062/images/c2-t3-r5_jc1chl.jpg",
      thumbnailDescription: "Collage of different types of resumés.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533192/audio/c2-t3-r5_iwzxbe.mp3",
        text: `
        <div class="article">
          <p>There are three common types of resumés. Let's look at each more closely:</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r6",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "Work History (Chronological)",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669223/images/c2-t3-r6_01_yqmyta.jpg",
      thumbnailDescription: "Close-up of Work History resume.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533193/audio/c2-t3-r6_lji0sh.mp3",
        text: `
        <div class="article">
          <p>A Work History resumé focuses on your past work experience. It is sometimes called a “Chronological” resumé because it lists your work experience in order, starting with your current or most recent job.</p>
          <p>Many employers prefer this type of resumé because it can be skimmed quickly to find specific skills or experiences. However, this format sometimes makes it harder for a job seeker to show skills that transfer from one job to another.</p>
          <p>A Work History resumé is a good choice for job seekers who:</p>
          <ul>
            <li>Have worked regularly.</li>
            <li>Don't have significant gaps in work history.</li>
            <li>Have a track record in the same type of job or industry.</li>
            <li>Are working or have worked recently.</li>
          </ul>
          <p>Work History Resumé includes the following elements:</p>
          <ol>
            <li>Personal information</li>
            <li>Work Experience</li>
            <li>Key Skills and Accomplishments</li>
            <li>Education</li>
            <li>Special Interests or Achievements (optional)</li>
            <li>References</li>
            <li>Date created or updated</li>
          </ol>
          <h2>Sample File</h2>
          <p><A href="https://res.cloudinary.com/der67rfj1/image/upload/v1658518766/pdf/Sample_Work_History_Resume.pdf" target="_blank">Sample Work History Resumé</a></p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r7",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "Skills (Functional)",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669223/images/c2-t3-r7_01_qemmyi.jpg",
      thumbnailDescription: "Close-up of Skills resume.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533193/audio/c2-t3-r7_eentrp.mp3",
        text: `
        <div class="article">
          <p>A Skills resumé, or “Functional” resumé, focuses on what you know and what you can do. A Skills resumé emphasizes what you can do rather than when and how long you worked somewhere.</p>
          <p>This type of resumé is a good option for job seekers:</p>
          <ul>
            <li>Who want to highlight specific skills or qualifications.</li>
            <li>With limited work experience.</li>
            <li>Who have long gaps in employment.</li>
            <li>Who are changing careers or industries.</li>
            <li>Who have skills that will transfer to new opportunities.</li>
          </ul>
          <p>A Skills Resumé includes the following elements:</p>
          <ol>
            <li>Personal information</li>
            <li>Important Skills</li>
            <li>Work Experience</li>
            <li>Education</li>
            <li>Special Interests or Achievements (optiona)</li>
            <li>References</li>
            <li>Date created or updated</li>
          </ol>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t3-r8",
      categoryId: "c2",
      topicId: "c2-t3",
      title: "Online (Scannable)",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568063/images/c2-t3-r8_ddkhav.jpg",
      thumbnailDescription:
        "Close-up of an Online resume on a computer screen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533193/audio/c2-t3-r8_vsp9hg.mp3",
        text: `
        <div class="article">
          <p>The third type of resumé is an online, or Scannable, resumé. This type of resumé can be organized as either a Work History or Skills resumé. The key difference is that an online resumé is formatted differently so that it can be uploaded as part of an online application and easily scanned for key words.</p>
          <p>The Online resumé contains the same information as the Skills resumé except with these formatting differences.</p>
          <ul>
            <li>Use a simple, common typeface, such as Times or Arial, in 11- or 12-point.</li>
            <li>Use all capital letters to set off sections.</li>
            <li>Do not use bold, italics, underlining, etc.</li>
            <li>Format text in a single column and justified on the right side of the page.</li>
            <li>Do not use bullets.</li>
            <li>Do not use italics.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t4-r1",
      categoryId: "c2",
      topicId: "c2-t4",
      title: "Why Do You Need Supporting Materials?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568063/images/c2-t4-r1_c02mem.jpg",
      thumbnailDescription: "Collage of Supporting Materials.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658533193/audio/c2-t4-r1_eplthf.mp3",
        text: `
        <div class="article">
          <p>There are a number of supporting materials that you can use to showcase your skills, explain special situations, and present yourself professionally during your job search.</p>
          <p>These include:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c2-t4-r2",
      categoryId: "c2",
      topicId: "c2-t4",
      title: "Cover Letters and Cover Emails",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568063/images/c2-t4-r2_meqcmb.jpg",
      thumbnailDescription: "Woman reading a cover letter.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697825185/audio/c2-t4-r2_03_eldgvm.mp3",
        text: `
        <div class="article">
          <p>A cover letter introduces:</p>
          <ul>
            <li>You.</li>
            <li>Your skills.</li>
            <li>Your education.</li>
            <li>Your work experience and achievements.</li>
          </ul>
          <p>This may sound like the same information contained in your resumé, but a cover letter or email serves a different purpose. A resumé includes information about all of your skills, education and work history, while a cover letter highlights only key facts. A cover letter gives an employer a sneak peek at the contents of your resumé.</p>

          <h2>When to Use a Cover Letter</h2>
          <p>You should include a cover letter any time that you mail or drop off information about yourself to a potential employer. It can be:</p>
          <ul>
            <li>Printed and sent with a completed job application and/or resumé.</li>
            <li>Sent with a completed job application and/or resumé as an attachment to an email.</li>
            <li>Sent as an email with an attached job application and/or resumé.</li>
            <li>Uploaded with your resumé as part of an online job application.</li>
          </ul>

          <h2>Key Elements of a Cover Letter</h2>
          <p>An effective cover letter is:</p>
          <ul>
            <li>Personalized.</li>
            <li>Unique.</li>
            <li>Memorable.</li>
            <li>Accurate.</li>
            <li>Businesslike.</li>
          </ul>

          <p>Here are some important elements:</p>

          <h2>1. Heading</h2>
          <ul>
            <li>Your name, address and contact information.</li>
            <li>The date.</li>
            <li>The employer's name, title, and address.</li>
          </ul>

          <h2>2. Salutation – Who You're Addressing</h2>
          <ul>
            <li>Address the letter to a specific person. If the job posting doesn't have a specific person to address it to, you might call the company to get the name of the hiring manager. If you can't find out the name of a specific person who should receive the letter, use the title noted in the job posting. Or use a generic greeting such as Dear Hiring Manager.</li>
            <li>Don't use the person's first name. Address the letter to Mr., Ms., Dr., etc. – whichever is appropriate – followed by the person's last name.</li>
            <li>Use a colon or comma after the salutation or last name.</li>
          </ul>

          <h2>3. Opening Paragraph – What You're Applying For</h2>
          <ul>
            <li>Keep it short – one or two sentences should be enough.</li>
            <li>Name the specific position you're applying for.</li>
            <li>Say how you learned about the job (i.e., on the Internet, from a current employee, etc.)</li>
            <li>Briefly state that you'd like to be considered for the job and share a little knowledge that connects to the position or company mission.</li>
          </ul>

          <h2>4. Middle Paragraph(s) – What You Have to Offer</h2>
          <ul>
            <li>Describe 2-3 skills, experiences or accomplishments that match those listed in the job description.</li>
            <li>Give specific examples that show what you have to offer in regards to this job.</li>
            <li>Highlight any relevant and notable projects or recognition you've received.</li>
          </ul>

          <h2>5. Closing paragraph</h2>
          <ul>
            <li>Restate your interest in the position and request an interview.</li>
            <li>Say that your resumé is enclosed or attached.</li>
            <li>State the best way to contact you.</li>
            <li>Thank the employer for considering your application.</li>
          </ul>

          <h2>6. Closing</h2>
          <ul>
            <li>Use “Sincerely” or “Best Regards,” followed by a comma.</li>
            <li>Leave several spaces before typing your full name.</li>
            <li>If printed, sign your first and last name in black or blue ink in the space above your typed name.</li>
          </ul>

          <h2>Key Elements of a Cover Email</h2>
          <p>To submit a cover letter and resumé electronically you can:</p>
          <ul>
            <li>Attach a copy of your cover letter and your resumé to an email.</li>
            <li>Adapt your cover letter to be sent as the content of the email and attach a copy of your resumé.</li>
            <li>If you choose the second option, the body of the email is the same as a traditional cover letter with different formatting.</li>
          </ul>
          <p>Here are a few things to keep in mind:</p>
          <ul>
            <li>Omit or don't include the inside address (hiring manager's name, title, company, and address).</li>
            <li>Write a clear email subject line, such as: “Application for Food Service Assistant.”</li>
            <li>There's no need to include the date.</li>
            <li>Mention that your resumé is attached (and be sure to attach it!).</li>
            <li>Use “Sincerely” or “Best Regards,” followed by a comma, then press return/enter to go down a line and then type your first and last name, followed by your complete contact information.</li>
          </ul>

          <h2>Do's and Don'ts for Cover Letters and Cover Emails</h2>
          <p>Here are some things that can help your cover letter or email leave the right impression:</p>
          <ul>
            <li>Proofread your letter or email for misspellings and grammatical errors.</li>
            <li>Ask someone else to read your letter/email before you send it.</li>
            <li>Vary how you start sentences. Don't start every sentence with “I.”</li>
            <li>Address your letter or email to a specific person when possible.</li>
            <li>Include specific examples of 2-3 skills and experiences.</li>
            <li>Request a specific action, such as an interview.</li>
            <li>Include your address and contact information.</li>
            <li>Single space your letter/email and use a common font like Times New Roman or Arial in 11 or 12 point.</li>
            <li>If you send a printed cover letter, print it on standard resumé paper and sign it with a black or blue pen.</li>
            <li>Be sure to enclose or attach your resumé and/or completed job application.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t4-r3",
      categoryId: "c2",
      topicId: "c2-t4",
      title: "References/Letters of Recommendation",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568063/images/c2-t4-r3_zkoudw.jpg",
      thumbnailDescription: "Close-up of references section of a resumé.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658015/audio/c2-t4-r3_01_lopb4h.mp3",
        text: `
        <div class="article">
          <h2>Why References?</h2>
          <p>Potential employers listen to people who know your skills, attitude and work habits, and who can confirm that you have the skills that you claim to have.</p>
          <p>Most employment references are provided in a two-way conversation between someone who knows you and a potential employer.</p>
          <p>All job applications ask for references so be ready to provide at least three professional references when you apply for a job.</p>

          <h2>Possible Reference Sources</h2>
          <p>Some of the people who might be able to provide a reference for you are:</p>
          <ul>
            <li>Past employers.</li>
            <li>Current or past coworkers.</li>
            <li>People with whom you've volunteered.</li>
            <li>Current or former teachers and coaches, if you're in school or a recent graduate.</li>
            <li>Faith-based centers you are active in.</li>
          </ul>
          <p>References from family and friends usually don't carry much weight because employers expect them to talk about you in the best possible way.</p>

          <h2>Using References</h2>
          <p>Here are some ways to make the most of your job references:</p>
          <p class="text-weight-bold">Ask First.</p>
          <ul>
            <li>Ask potential references if they're willing to provide a reference before you give out their names and contact information.</li>
            <li>Find out how they prefer to be contacted by a potential employer.</li>
          </ul>
          <p class="text-weight-bold">Keep References Informed.</p>
          <ul>
            <li>Some references find it helpful if you give them some ideas of things they can talk about with a potential employer.</li>
            <li>Tell your references when you have used their names on a job application or on your resumé.</li>
            <li>Let them know the name of the potential employer who may be contacting them and the type of job you have applied for.</li>
            <li>Let your references know when you've been hired. Thank them for their support and help.</li>
          </ul>

          <h2>References vs. Letters of Recommendation</h2>
          <p>A letter of recommendation is different from a personal or professional reference. A Letter of Recommendation:</p>
          <ul>
            <li>Is written.</li>
            <li>Addresses your skills, experience and character in a general way.</li>
            <li>Is not addressed to a specific person.</li>
            <li>Does not refer to a specific job being applied for.</li>
            <li>Can be sent along with a cover letter, job application or resumé.</li>
          </ul>
          <p>There are several reasons to include letters of recommendation when submitting a job application or resumé. For example, attaching a letter of recommendation to an application or resumé might convince an employer to interview you.</p>
          <p>Another plus: A potential employer can read a letter of recommendation at any time. They don't need to make extra efforts to connect with the writer.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t4-r4",
      categoryId: "c2",
      topicId: "c2-t4",
      title: "Work Samples and Portfolios",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568063/images/c2-t4-r4_bzhvbm.jpg",
      thumbnailDescription: "Young man showing some graphic design samples.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658015/audio/c2-t4-r4_02_ug1wnt.mp3",
        text: `
        <div class="article">
          <h2>Why Work Samples and Portfolios?</h2>
          <p>Some employers may ask you to send samples of your work along with your cover letter, job application, and/or resumé.</p>
          <p>You may want to bring work samples with you to the interview. This can help reassure an employer that you have the required skills.</p>

          <h2>Purpose of a Work Sample Portfolio</h2>
          <p>A work sample portfolio is a visual resumé of things you've done. Anyone can use a work portfolio to:</p>
          <ul>
            <li>Show their experience.</li>
            <li>Highlight examples of specific types of work.</li>
            <li>Show how they would accomplish specific tasks.</li>
            <li>Show that they know how to use specific types of technology.</li>
          </ul>

          <h2>Creating a Work Sample Portfolio</h2>
          <p class="text-weight-bold">Get Organized</p>
          <ul>
            <li>Collect samples of your work before you need them. If you're on good terms with a past employer, ask for samples of your work. If that's not possible, can you recreate them?</li>
            <li>Show your best work. Don't be tempted to include everything you've done.</li>
          </ul>
          <p class="text-weight-bold">Customize Your Portfolio</p>
          <ul>
            <li>Change the samples in your portfolio to suit the potential employer and the job you're interested in.</li>
            <li>Choose samples that a prospective employer can relate to. For example, a person who is applying to work for a fancy caterer wouldn't want to include photos of his work as a landscaper.</li>
          </ul>

          <h2>Decide How You'll Present Your Samples</h2>
          <p>There are many ways to present your work samples. You can:</p>
          <ul>
            <li>Use a binder with clear pockets to display photographs or printed pieces.</li>
            <li>Scan your samples onto a USB drive or other electronic storage.</li>
            <li>Create an online or digital portfolio.</li>
            <li>Use a pocket folder to hold printouts of your work.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c2-t4-r5",
      categoryId: "c2",
      topicId: "c2-t4",
      title: "When Letters of Explanation Can Be Helpful",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657568061/images/c2-t4-r5_ostye0.jpg",
      thumbnailDescription: "Man reading a letter of explanation.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658038/audio/c2-t4-r5_01_ithodd.mp3",
        text: `
        <div class="article">
          <p>Employers don't like surprises. You may be able to avoid some tough questions by writing a Letter of Explanation explaining a situation that a potential employer may not understand. This letter can be attached to a completed job application or resumé or given to the employer before, during or after an interview.</p>
          <p>Your job placement specialist can help you decide if you should prepare a letter of explanation.</p>
          <p>A good Letter of Explanation should:</p>
          <ul>
            <li>Be honest.</li>
            <li>Be simple.</li>
            <li>Focus on the positives.</li>
          </ul>
          <p>A Letter of Explanation may be helpful if you:</p>
          <ul>
            <li>Have a significant gap in work history.</li>
            <li>Have a felony conviction or criminal history.</li>
          </ul>

          <h2>Explaining a gap in work history</h2>
          <p>There are a lot of reasons why a person might not be able to work for a long period of time. Some examples include taking care of family, returning to school, recovering from an illness or accident or having a tough time finding a job because of the economy. Be honest about your situation and focus on the positive things that you did while you were unemployed, such as new skills that you've learned, unpaid work that might relate to the job, etc.</p>
          <p>Example:</p>
          <blockquote>“I have not worked full-time work since I was laid off from my job as a machinist ten months ago. During this time, I have taken community education classes to improve my computer skills and become more proficient at using Microsoft Word, PowerPoint and Excel. I also helped organize a fundraiser at my children's school that raised nearly $50,000 for new computers. I'm eager to return to work and to put my new skills to use.” </blockquote>

          <h2>Explaining a felony conviction or criminal history</h2>
          <p>If you have a criminal conviction that must be disclosed, a Letter of Explanation can be a good way to explain the circumstances. Your letter should be honest and matter-of-fact.</p>
          <p>An effective letter explaining a conviction should:</p>
          <ul>
            <li class="text-weight-bold">Present the facts.</li>
            <ul>
              <li>A brief explanation of the nature of the crime you were convicted of, when you were convicted, and the length of your sentence.</li>
            </ul>

            <li class="text-weight-bold">Take responsibility.</li>
            <ul>
              <li>Admit that you did something wrong or illegal. You do not have to provide the details of the offense or of your sentence. It is all right to say, "I made a poor choice and now I have put the past behind me. I am looking forward to the future."</li>
            </ul>

            <li class="text-weight-bold">Explain how you've changed.</li>
            <ul>
              <li>Identify positive things you've done, such as classes or training that you completed, treatment for chemical dependency, volunteer work, moving to a new community, etc.</li>
            </ul>

            <li class="text-weight-bold">Reassure the employer.</li>
            <ul>
              <li>Make it clear that your criminal history will not affect your job performance.</li>
            </ul>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    // Category 3 resources
    {
      id: "c3-t1-r1",
      categoryId: "c3",
      topicId: "c3-t1",
      title: "The Hiring Process",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t1-r1_rxd4m4.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription:
        "New employee is sitting opposite of an employer and smiling.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658016/audio/c3-t1-r1_01_besofl.mp3",
        text: `
        <div class="article">
          <p>Although each employer hires workers differently, most follow five common steps. Let's go over each step in more detail.</p>
          <h2>STEP 1: Defining the Need</h2>
          <p>The hiring process often begins when a current employee leaves a job or a new job must be created. Before the position is posted, the employer reviews or defines the job's requirements, decides the kind of person who will be most successful at it, and identifies the skills, education and experience that the new employee should have.</p>
          <h2>STEP 2: Recruitment</h2>
          <p>Employers find qualified applicants in many ways. This is called “recruitment.” Some ways that employers recruit potential employees include:</p>
          <ul>
            <li>Advertising – Ads placed in local, community or trade newspapers, postings on community boards, signs in storefront windows, etc.</li>
            <li>Internet – Company web sites, employment job sites like <a href="https://www.indeed.com/" target="_blank">indeed.com</a>, and special interest web sites.</li>
            <li>Referrals – Employers tell other people they're looking to hire a new employee and ask them to refer people who might be interested.</li>
            <li>Placement services – Public and private placement services can be hired to link qualified candidates with potential employers.</li>
            <li>Staffing agencies – Sources for temporary and contract employment.</li>
            <li>Job fairs – Many employers gather in one place to showcase their companies and gather information from job applicants.</li>
          </ul>

          <h2>STEP 3: Screening</h2>
          <p>Employers review job applications and resumés to identify applicants that meet the job's critical requirements. Employers do this by:</p>
          <ul>
            <li>Scanning job applications and resumés for key words.</li>
            <li>Pre-employment tests or skills testing.</li>
            <li>Conducting telephone, video-based or virtual initial interviews or pre interviews.</li>
            <li>Contacting references or past employers, etc.</li>
            <li>Conducting background checks to verify information provided by an applicant.</li>
            <li>Checking social networking sites.</li>
          </ul>

          <h2>STEP 4: Interviews</h2>
          <p>An employer usually interviews several good candidates before deciding which applicant is best qualified. Interviews may be in person or virtually.</p>

          <h2>STEP 5: Selection</h2>
          <p>Employers decide which candidate to hire based on information they gather from applicants during one-on-one interviews, personal references from past employers, the results of a background check, etc. This person is then offered the job either in person, on the telephone, virtually, through email or regular mail.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t1-r2",
      categoryId: "c3",
      topicId: "c3-t1",
      title: "Important Job Search Reminders",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t1-r2_i27wku.jpg",
      thumbnailDescription: "Woman is using both a laptop and a smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658016/audio/c3-t1-r2_01_cahw1y.mp3",
        text: `
        <div class="article">
          <p>Before you start searching for jobs, it's important to get into job search mode! Remember, every contact with a potential employer leaves an impression – good or bad.</p>
          <p>Here are six reminders to help you start off right.</p>
          <ol>
            <li class="text-weight-bold">Be sure your email address is appropriate.</li>
            <ul>
              <li>Be sure your email address is professional. If you were an employer, would you be more likely to interview sweetiepie@internetprovider.com or first_lastname@internetprovider.com? It takes just a few minutes to open a free email account but it's well worth the effort!</li>
            </ul>

            <li class="text-weight-bold">Change your voice mail message.</li>
            <ul>
              <li>Be sure that the voice mail message on your phone is businesslike, simple, and straightforward.</li>
              <li>Say your name, ask the person to leave a message, and promise to return the call as soon as possible. No nicknames, ringback tones or “fun” messages.</li>
            </ul>

            <li class="text-weight-bold">Check your email account and voice mail every day.</li>
            <ul>
              <li>If an employer contacts you, it's important to get back to them as soon as possible.</li>
              <li>Check your email or voice mail at least daily so you don't miss an important opportunity. If you wait too long, the employer may decide to move on to the next candidate!</li>
            </ul>

            <li class="text-weight-bold">Treat your job search as a job.</li>
            <ul>
              <li>Looking for a job is hard work. Get organized. Schedule time every day for job search activities.</li>
            </ul>

            <li class="text-weight-bold">Tidy up your appearance.</li>
            <ul>
              <li>Make sure you have clothes that are appropriate for filling out applications in-person and going on job interviews.</li>
            </ul>

            <li class="text-weight-bold">Update your social media sites.</li>
            <ul>
              <li>Be sure that your profiles on job and networking sites are up to date. Check your social media profiles and make sure they don't contain inappropriate information and photos.</li>
              <li>Also update your privacy settings so potential employers only have access to information that you want them to see!</li>
            </ul>

          </ol>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t2-r1",
      categoryId: "c3",
      topicId: "c3-t2",
      title: "Where to Look for Jobs",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1675386662/images/c3-t2-r1-replacement_ct5fn7.jpg",
      thumbnailDescription:
        "Collage of newspaper, website, person opening a door, woman talking with an employer and a Workforce Center computer lab.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658014/audio/c3-t2-r1_01_eg0ktj.mp3",
        text: `
        <div class="article">
          <p>It can be expensive and time-consuming to advertise jobs in print or to post in online job websites. That's why many companies – particularly small businesses – never advertise many open positions and rely instead on networking and referrals from coworkers or friends to fill them.</p>
          <p>You can tap into this “hidden job market” by:</p>
          <ul>
            <li>Talking with people you know.</li>
            <li>Networking online and at industry events, conferences and job fairs.</li>
            <li>Dropping in at local businesses to ask about job openings.</li>
            <li>Checking company social media and websites 'career' section.</li>
            <li>Checking postings on local community notice boards and websites.</li>
            <li>Join trade and professional associations in the industry and check their job boards.</li>
            <li>Understand which industries are growing in your area.</li>
            <li>Consider temporary or contract work, which can lead to permanent positions and help you make connections.</li>
            <li>You also can use daily newspapers or weekly community newspapers to find hints about companies that are growing and may need help now or in the near future.</li>
          </ul>
          <p>Let's look at each item for some places you can check out for publicly posted job openings.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t2-r2",
      categoryId: "c3",
      topicId: "c3-t2",
      title: "Newspapers and Other Local Media",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t2-r2_vae43q.jpg",
      thumbnailDescription: "Pile of newspapers.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658015/audio/c3-t2-r2_01_fkmtos.mp3",
        text: `
        <div class="article">
          <p>For centuries, job seekers have relied on newspapers to find job openings. While many people use internet job sites as a primary source for job openings, don't ignore help wanted ads in your local media, which can be in print and/or online. Regularly check:</p>
          <ul>
            <li>Your local daily newspaper.</li>
            <li>Local weekly or community newspapers.</li>
            <li>Local trade, associations or special interest publications.</li>
            <li>Community boards at libraries, community centers, colleges, businesses, etc.</li>
          </ul>

          <h2>Where to Find Postings in Newspapers</h2>
          <p>Notices of job openings are usually found in the classified section of a newspaper or trade publication under the heading “Employment Opportunities” or “Careers.”
          A newspaper “help wanted” ad usually contains:</p>
          <ul>
            <li>The position's functional title.</li>
            <li>Available shifts.</li>
            <li>The job's salary range and eligibility for benefits.</li>
            <li>Whether the position is full-time, part-time or seasonal.</li>
            <li>How to contact the employer or apply for the job.</li>
          </ul>
          <p>The posting also may include the company name, a brief description of the job duties, and required skills, abilities, and experience.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t2-r3",
      categoryId: "c3",
      topicId: "c3-t2",
      title: "Using the Internet",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t2-r3_zmcz8v.jpg",
      thumbnailDescription: "Job search function on a company website.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658041/audio/c3-t2-r3_01_bf44aj.mp3",
        text: `
        <div class="article">
          <p>Job seekers can use the Internet to:</p>
          <ul>
            <li>Access hundreds of employment web sites and job boards.</li>
            <li>Search for job openings in a specific industry.</li>
            <li>Search for certain types of jobs or jobs in specific locations.</li>
            <li>Search for local, national or international job openings.</li>
            <li>Check out employment opportunities at specific employers.</li>
            <li>Apply for jobs online.</li>
            <li>Post a resumé on job boards or resumé banks for specific industries.</li>
            <li>Search for jobs whenever and wherever they want – the Internet is open for business 24 hours a day!</li>
          </ul>

          <h2>Using the Internet Effectively in Your Job Search</h2>
          <p>It's easy to get sidetracked on the Internet. Stay focused! Here are a few things you can do to improve your Internet job search:</p>
          <ul>
            <li>Check out several online job sites and choose one or two that you think will work best for you. Create a profile and bookmark the sites on your computing device. Be sure to write down and save the User Name and Password that you're going to use. Check back regularly!</li>
            <li>Check both national and local job sites. Remember, many smaller businesses only post job openings close to home.</li>
            <li>Identify key words that you will use when searching jobs databases. This will help you save time.</li>
            <li>If a site allows you to post your resumé, upload the Online (Scannable) resumé that you created in Course 2: Filling Out the Paperwork. This will make it easier to find and apply for jobs that interest you. It also will make it easier for potential employers to find you! For some job sites, your profile setup is basically your application, so you can apply to jobs instantly.</li>
            <li>Sign up to receive updates about jobs that meet your criteria.</li>
            <li>Use the same User Name and Password for each site. They will be easier to remember and make your search more efficient!</li>
            <li>Be cautious. There are many online employment scams. Never provide your banking information or your driver's license number. You should only provide your Social Security number when you're actually completing a job application.</li>
          </ul>

          <h2>How Job Search Sites Work</h2>
          <p>Job banks and employment portals are large databases that contain job postings that have been submitted by employers with open jobs. Job seekers use keywords to search the database for postings that meet their criteria. You usually can search in several ways, including by job title, industry, key skills, and/or location. Job seekers can also create application profiles and/or post resumés on the job site for employers to review.</p>
          <p>If possible, create a profile and sign up to be notified automatically when new postings become available.</p>
          <p>Job sites are also a great place to find tips and techniques to make your job search more effective.</p>

          <h2>Contents of a Typical Online Job Posting</h2>
          <p>There is no standard format for online job postings that all employers use. Most online postings contain the same information as a newspaper advertisement, as well as a job identification number. An online job posting typically contains:</p>
          <ul>
            <li>A job identification number.</li>
            <li>The company name.</li>
            <li>The position's functional title.</li>
            <li>A brief description of the job duties.</li>
            <li>Required skills, abilities and experience.</li>
            <li>Available shifts and whether the position is full-time, part-time or seasonal.</li>
            <li>The job's salary range and eligibility for benefits.</li>
            <li>Instructions for uploading your resumé and/or a cover letter.</li>
            <li>How to follow up with the employer or check the status of your application.</li>
          </ul>

          <h2>Company Web Sites</h2>
          <p>If you're interested in working for a specific company, check the “Careers” or “Employment” section of the company's web site for job openings. This section usually provides a list of open positions, as well as job descriptions and information on how to apply.</p>
          <p>You may be required to create a profile before you can apply for a job online.</p>
          <p>Company web sites also are a good way to stay informed of company activities and growth plans, find contact information, etc.</p>

          <h2>College Career Counseling Web Sites</h2>
          <p>Many colleges post information about job openings that they receive from local employers and alumnae. These are often entry-level openings. Check out the web sites for colleges in your area.</p>

          <h2>State Employment Web Sites</h2>
          <p>Every state has a public web site that serves as a central source of job-related information for their state. Most sites allow you to:</p>
          <ul>
            <li>Access the state's economic development division, including unemployment benefits.</li>
            <li>Access local and state job boards.</li>
            <li>Find job hunting information and tips.</li>
            <li>Find a Workforce Center location near you.</li>
            <li>Learn about the state's Vocational Rehabilitation services.</li>
          </ul>
          <p>You can find your state's employment web site by going to <a href="https://www.job-hunt.org/" target="_blank">job-hunt.org</a>.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t2-r4",
      categoryId: "c3",
      topicId: "c3-t2",
      title: "Walk-in Visit",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t2-r4_j5w0aj.jpg",
      thumbnailDescription: "Person walking into a restaurant.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658021/audio/c3-t2-r4_01_onnsm5.mp3",
        text: `
        <div class="article">
          <p>You may be missing a lot of local opportunities if you rely exclusively on the Internet or local newspapers to find a job. Be sure to visit local businesses in your area regularly to ask about current job openings and fill out applications. Even if they don't have current openings, many employers keep resumés and completed job applications on file in case a job opens up.</p>
          <p>The employer may:</p>
          <ul>
            <li>Ask you to fill out a paper application before you leave.</li>
            <li>Give you an application to fill out and return.</li>
            <li>Require you to complete an application online using an on-site computer terminal or kiosk.</li>
            <li>Ask you to go online to complete an application on their company's web site.</li>
            <li>You'll learn about completing job applications in Module</li>
          </ul>

          <h2>Things to Keep in Mind</h2>
          <p>When you go to a business to inquire about job openings:</p>
          <ol>
            <li class="text-weight-bold">Be polite to everyone you encounter.</li>

            <li class="text-weight-bold">Look and act businesslike and confident.</li>
            <ul>
              <li>Dress appropriately, as though you were going to an interview.</li>
              <li>Show positive body language (example: an open and attentive posture and friendly facial expression like a smile).</li>
              <li>Speak clearly and loudly enough that you can be heard.</li>
            </ul>

            <li class="text-weight-bold">Decide what you plan to say before you enter the business.</li>
            <ul>
              <li>Examples for smaller stores or businesses:</li>
              <ul>
                <li>“Is there someone I can talk to about possible job openings?” OR</li>
                <li>“I'd like to fill out a job application. May I please speak to a manager?”</li>
              </ul>
              <li>Example for larger businesses:</li>
              <ul>
                <li>“I'm interested in filling out an application for employment. Can you direct me to the human resources department?”</li>
              </ul>
            </ul>

            <li class="text-weight-bold">Visit stores and businesses when the owner or manager will have more time.</li>
            <ul>
              <li>Example: Don't go to a restaurant to apply for a job during the lunch hour.</li>
            </ul>

            <li class="text-weight-bold">Go alone and turn off your cell phone and other devices.</li>
            <ul>
              <li>Don't bring a friend along for support. If you and a friend are looking for jobs together, go into the business separately. If someone is driving you, ask him or her to wait in the car or in a nearby public area.</li>
              <li>Turn your cell phone and any other electronic devices off completely.</li>
              <li>Turn off your iPod and other devices.</li>
              <li>Remove earbuds, Bluetooth headphones, etc. before entering the building.</li>
            </ul>

            <li class="text-weight-bold">Be prepared.</li>
            <ul>
              <li>Some employers may want to interview you on the spot so think about the way you would answer common questions. You'll learn more about interviewing for jobs in Course 4: Interviewing for a Job.</li>
              <li>Bring your completed Master Application with you so you can fill in the job application quickly and accurately.</li>
              <li>Bring a black or blue pen and several copies of your resumé.</li>
            </ul>
          </ol>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t2-r5",
      categoryId: "c3",
      topicId: "c3-t2",
      title: "Other Employment Resources",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1675386662/images/c3-t2-r5-replacement_mt0y4b.jpg",
      thumbnailDescription:
        "Workforce Center computer lab with young people searching for information.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658019/audio/c3-t2-r5_01_v9alxg.mp3",
        text: `
        <div class="article">
          <h2>Workforce Center Services</h2>
          <p>Workforce Centers offer tools and resources that all job seekers can use to find jobs. You don't need special referrals or permission to use Workforce Center resources.</p>
          <p>Most Workforce Centers have staff members who can help you with your job search, think about accommodations, etc. Many resources and services are available virtually as well.</p>
          <p>Most offer:</p>
          <ul>
            <li>Access to computers and other office equipment.</li>
            <li>Free Internet access.</li>
            <li>Access to state job sites and job posting information.</li>
            <li>A reference library.</li>
            <li>Job hunting workshops, training, and networking events.</li>
            <li>Job and career fair notices.</li>
          </ul>

          <h2>Vocational Rehabilitation Services</h2>
          <p>Many Workforce Centers share space with or are located near a state Vocational Rehabilitation office. Vocational Rehabilitation Services:</p>
          <ul>
            <li>Can help determine your goals for work and longer-term career building.</li>
            <li>Make referrals to other agencies, community partners and job placement resources.</li>
            <li>Identify vocational education, resources and training options.</li>
            <li>Arrange interpreter services if needed.</li>
            <li>Identify potential assistive technology.</li>
            <li>Suggest job accommodations.</li>
            <li>Refer you to additional job placement resources.</li>
            <li>Help you to educate employers about your situation.</li>
            <li>Work with you on job retention strategies after you've been hired.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t2-r6",
      categoryId: "c3",
      topicId: "c3-t2",
      title: "Personal Network",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t2-r6_phcslx.jpg",
      thumbnailDescription: "Two people talking in an office.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598187/audio/c3-t2-r6_vzrii9.mp3",
        text: `
        <div class="article">
          <p>The most common way that people find a job is through networking and personal referrals. The more people who know that you're looking for a job, the more job leads you're likely to get!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t3-r1",
      categoryId: "c3",
      topicId: "c3-t3",
      title: "What is Career Networking?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t3-r1_ofj3vr.jpg",
      thumbnailDescription:
        "Man is looking at a smart phone with an overlaid graphic design of people being networked together.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598187/audio/c3-t3-r1_chj33s.mp3",
        text: `
        <div class="article">
          <p>Career networking means communicating with people you know to find job opportunities.</p>
          <p>Networking is especially important when it comes to finding unadvertised jobs. Remember, most jobs are filled through referrals. In fact, most job seekers find a job through referrals from a family member, friend or coworker!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t3-r2",
      categoryId: "c3",
      topicId: "c3-t3",
      title: "Networking Basics",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t3-r2_ugvspd.jpg",
      thumbnailDescription:
        "Close-up of graphic design of people being networked together with the words Networking 101 overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598187/audio/c3-t3-r2_ame97d.mp3",
        text: `
        <div class="article">
          <p>Career networking means communicating with people you know to find job opportunities.</p>
          <p>Trust is at the heart of effective networking because it relies on the people you know and trust to connect you with people they know and trust.</p>
          <p>Networking is not a matter of calling or e-mailing complete strangers or randomly distributing business cards or resumés.</p>
          <p>A career network is made up of people you know personally and professionally who:</p>
          <ul>
            <li>Agree to tell you when they learn of job openings.</li>
            <li>Can discuss your skills and work experiences with potential employers.</li>
            <li>Want to help.</li>
          </ul>

          <h2>Who Should Be Part of Your Network</h2>
          <p>Every person's career network is unique. A career network can include:</p>
          <ul>
            <li>Family members.</li>
            <li>Friends and neighbors.</li>
            <li>Job counselors.</li>
            <li>Teachers or coaches.</li>
            <li>Former employers.</li>
            <li>People who share your interests.</li>
          </ul>

          <h2>Networking Tips</h2>
          <p>Although you might spend a lot of time with family and friends, work might not be something you talk about every day. Here are some ways to make the most of your contacts:</p>
          <ul>
            <li>Tell people that you're looking for work and what kind of jobs you're looking for.</li>
            <li>Be specific about your skills and experience.</li>
            <li>Offer to provide a resumé. You'll get more valuable information if your contacts know exactly what kind of jobs you're looking for and why you're qualified.</li>
            <ul>
              <li>Example: “I want to work as a computer help desk resource person. I did that for a year at XYZ company, and I'm pretty familiar with most Microsoft and Adobe programs.”</li>
            </ul>
            <li>Stay in touch by calling or e-mailing your contacts regularly.</li>
            <li>Thank the people who help you.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t3-r3",
      categoryId: "c3",
      topicId: "c3-t3",
      title: "Using Social Media for Networking",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t3-r3_wq3stl.jpg",
      thumbnailDescription: "Variety of social media logos.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598189/audio/c3-t3-r3_o6rxes.mp3",
        text: `
        <div class="article">
          <p>Some job seekers use social networking sites like LinkedIn and Facebook to contact and stay in touch with family members, friends, classmates, and business acquaintances. This can be a good way to let people know that you're looking for a job, but there are some drawbacks that you need to think about.</p>
          <p>If you choose to use social media, remember:</p>
          <ul>
            <li>Many employers check social network sites to learn about applicants. This is an acceptable business practice. Make sure that anything you put on your “page” sends the right message about you.</li>
            <li>Remove anything that you don't want a potential employer to know about and be sure to set your security preferences to restrict access.</li>
            <li>Check your profile – does it include information about your job goals and experience? Limit your profile to information that might interest a potential employer.</li>
            <li>Include a message telling your “friends” that you're looking for a job and describing the type of work you're interested in.</li>
            <li>Regularly check comments posted by others and remove anything that is inappropriate or sends the wrong message.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t3-r4",
      categoryId: "c3",
      topicId: "c3-t3",
      title: "Information Interviews",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720160/images/c3-t3-r4_rt0rre.jpg",
      thumbnailPosition: "0% 50%",
      thumbnailDescription: "Two people talking within a coffee shop.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658020/audio/c3-t3-r4_02_ldh46r.mp3",
        text: `
        <div class="article">
          <p>Networking sometimes leads to information interviews. An information interview is not the same as a job interview.</p>
          <p>Information interviews can:</p>
          <ul>
            <li>Help you understand what it would be like to work at a specific job or company.</li>
            <li>Help you learn about a career.</li>
            <li>Help you decide what to emphasize in your resumé and cover letter.</li>
            <li>Give you a chance to practice discussing your job skills and experiences.</li>
            <li>Add new people to your network.</li>
            <li>Lead to referrals for job postings.</li>
            <li>Help you decide if you have the skills you need to work at a specific job or career.</li>
          </ul>

          <h2>The Typical Information Interview Process</h2>
          <p>Here are some things to remember about information interviews:</p>
          <ul>
            <li>Arrange a time to call or meet. If you're meeting in person or virtually, dress professionally as if you were going on a job interview. You will learn more about what to wear to an interview in Course 4.</li>
            <li>Think about the questions you want to ask. In general, focus on getting information about job duties, required skills and training, and any job-related words that you should use when applying for this type of job. You also may ask if they can suggest anyone else you should talk to.</li>
            <li>Bring several copies of your resumé to the meeting.</li>
            <li>Be prepared to discuss your background and strengths, and to explain why you're interested in this line of work.</li>
            <li>Be respectful of your contact's time. Ask how much time the person has to spend with you. Use the time wisely!</li>
            <li>Send a thank-you email or handwritten note on the day of the interview or the day after.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t4-r1",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Creating a Job Search Plan",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r1_odshlr.jpg",
      thumbnailDescription:
        "Laptop with the words Job Search displayed on the screen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598187/audio/c3-t4-r1_zvci2x.mp3",
        text: `
        <div class="article">
          <h2>Elements of an Effective Job Search Plan</h2>
          <p>A good job search plan:</p>

          <h3>1. Is focused</h3>
          <p>Your chances of success increase when you focus your efforts on specific industries, types of jobs or specific employers.</p>

          <h3>2. Records your progress</h3>
          <p>Use a job log to track applications and resumés that you submit, thank you notes that you send or need to send, and other follow-up activities.</p>

          <h3>3. Sets deadlines</h3>
          <p>Set daily deadlines and create a weekly job search schedule.</p>

          <h3>4. Is up-to-date</h3>
          <p>Keep your plan up-to-date. Revise it regularly so it reflects what you're doing now, rather than what you thought you might be doing when you created your plan weeks ago!</p>

          <h2>Creating a Job Search Plan</h2>
          <p>Your job search plan brings together all of the information that you've learned about so far. It should include:</p>

          <h3>1. A List of Jobs that You're Interested In</h3>
          <p>Remember, every organization is different and may refer to the same job in different ways. You'll find it helpful to identify some different titles that may be used for the job you're interested in. This will make it easier to focus your search in the right places.</p>

          <h3>2. A List of Companies and Industries</h3>
          <p>This is particularly important if you want to work in a specific industry.</p>

          <h3>3. Referral Sources</h3>
          <p>Document the family, friends, and other sources that you will contact, or have contacted, about your job search and how that person might be able to help you.</p>

          <h3>4. Places You Plan to Visit Regularly</h3>
          <p>In Module 2, you learned about some common online job posting sites. Check them out, then note which ones you plan to visit regularly. Be sure to create a profile.</p>
          <p>Also, identify other places that you'll check, such as local newspapers or community posting boards, local businesses, specific company web sites, etc.</p>

          <h3>5. Your User Name and Passwords</h3>
          <p>Many job seekers use the same User Name and Password when filling out online applications and submitting resumés. This makes it easier to update your profile, check on the status of applications, and apply for other positions at the company. Be sure to write down the User Name and Password you've chosen.</p>

          <h3>6. Create a Schedule</h3>
          <p>Treat your job search like a job. It's a good idea to start your week by creating a job search schedule and setting deadlines for specific activities.</p>
          <p>Be sure to schedule time to search for jobs on the Internet, to network and follow up on referrals, and to improve your skills!</p>
          <p>A job search log is a key element of your job search plan.</p>

        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t4-r2",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "What is a Job Search Log?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r2_ddhgoy.jpg",
      thumbnailDescription:
        "Person is writing in a notebook displaying the words Job Search Log.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598188/audio/c3-t4-r2_gzihk7.mp3",
        text: `
        <div class="article">
          <p>A detailed job search log documents every step you've taken in your job search. A good job log contains six elements. Let's go over each one.</p>

          <h3>1. Company name and address</h3>
          <p>The business or organization's full street address and mailing address, if they're different, and the web site address.</p>

          <h3>2. Contact Person</h3>
          <p>The name, job title, telephone number, and email address of the person you have contacted or will be contacting.</p>

          <h3>3. Type of Job</h3>
          <p>Type of job that you've applied for or are interested in applying for. Make a copy of the posting and save it for future reference.</p>

          <h3>4. Referral Source</h3>
          <p>Document where you learned about the job or who told you about it, such as a referral from someone in your network, an online job posting or a walk-in visit.</p>

          <h3>5. Date and Type of Contacts</h3>
          <ul>
            <li>Phone: The date you contacted an organization to learn about job openings.</li>
            <li>Application Submitted: The date that you submitted a completed application.</li>
            <li>Resumé Submitted: The date you sent a resumé and cover letter.</li>
            <li>Interview: The date you interviewed for the job.</li>
            <li>Other: Your in-person contacts, etc.</li>
          </ul>

          <h3>6. Follow-up Activities</h3>
          <p>Document when and how you followed up on a job search activity, such as when you telephoned or sent a thank you note.</p>

        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t4-r3",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Completing Job Applications",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r3_rs5z4e.jpg",
      thumbnailDescription:
        "Person is pointing at a tablet displaying a job application.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598188/audio/c3-t4-r3_aqzwbg.mp3",
        text: `
        <div class="article">
          <p>Learn some important information about filling out job applications.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t4-r4",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "General Job Application Tips",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r4_smazvs.jpg",
      thumbnailDescription: "Person is completing a paper job application.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658020/audio/c3-t4-r4_01_sp33ox.mp3",
        text: `
        <div class="article">
          <h2>Always Answer Completely</h2>
          <ul>
            <li>Read the question carefully so you're sure you understand what the employer is asking.</li>
            <li>Fill in ALL of the blanks. If a question doesn't apply to you, write N/A (for “not applicable”) in the space provided. This shows that you read the question but that it doesn't apply.</li>
            <li>Always enter a specific job title for the “Position Desired.” If you're answering an ad or online job posting, use the job title mentioned. If you're not applying for a specific position, write in the type of job you want to do or the department you want to work in.</li>
            <li>Complete an application for each position that interests you.</li>
            <li>List your most relevant skills and experiences first. Many applications won't have enough room to list all of your skills.</li>
            <li>Gather all of your information before you begin applying for jobs so that you can complete the application quickly. Use the Master Application that you created in Course 2: Filling Out the Paperwork. This contains all of your important information.</li>
          </ul>

          <h2>Be Honest</h2>
          <ul>
            <li>Answer all questions honestly. If an employer discovers that you lied on your application, you can be fired.</li>
            <li>Don't guess. If you're not sure about some information, find the answer before you return the completed application.</li>
          </ul>

          <h2>Review Your Application</h2>
          <ul>
            <li>Carefully read the completed application before you submit it. Be sure that you have answered all questions accurately and honestly. Also check for grammar and spelling errors.</li>
            <li>If possible, have someone else proofread your completed application.</li>
            <li>Sign and date the application and check the box that says that the information you're submitting is accurate.</li>
            <li>Mail, submit or return the application to the correct person or place.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t4-r5",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Completing Applications in Person",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r5_cvucef.jpg",
      thumbnailDescription:
        "Woman hands a job application to a young man in an office setting.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658021/audio/c3-t4-r5_01_yoneh3.mp3",
        text: `
        <div class="article">
          <p>When applying for a job in person, it's important to create a good impression. Be sure to dress nicely. Some employers might interview you on the spot, so you should be ready to answer questions.</p>
          <p>Here are a few general guidelines:</p>

          <h2>Dress Appropriately</h2>
          <ul>
            <li>Shower or bathe, brush your teeth, and use deodorant.</li>
            <li>Wear clean, appropriate clothes – business casual pants or skirt, a polo shirt or blouse and closed toed shoes are usually best. No hat, jeans, t-shirts, sandals, shorts or mini-skirts.</li>
          </ul>

          <h2>Be Prepared</h2>
          <ul>
            <li>Bring a copy of the Master Application that you completed in Course 2: Filling Out the Paperwork. This will help you complete applications quickly and help make sure the information you include on your application is accurate.</li>
            <li>Bring a black or blue pen and use it to fill out the application.</li>
            <li>Bring several copies of your resumé in case the employer wants to interview you on the spot or you want to attach a copy to your completed application.</li>
          </ul>

          <h2>Start Off Right</h2>
          <ul>
            <li>Be polite and respectful to everyone you encounter at the business or organization.</li>
            <li>Don't wear headphones, chew gum or bring food or beverages.</li>
            <li>Turn off your cell phone before entering the business and keep it off until you leave.</li>
            <li>Friends should wait outside.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t4-r6",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Completing PAPER Applications",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r6_r89hs1.jpg",
      thumbnailDescription: "Man shows a paper job application in a clipboard.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598188/audio/c3-t4-r6_oxhp2d.mp3",
        text: `
        <div class="article">
          <p>Employers consider a lot of things when making a hiring decision. Sometimes, the way a paper application looks after you've completed it can be as important as the information it contains.</p>
          <p>Here are some things to remember:</p>

          <h2>Make Copies</h2>
          <ul>
            <li>If possible, make several copies of the blank application. Use one for a “sloppy copy” to gather your answers. You can use this as a guide to complete a neat, final copy that you will submit to the employer.</li>
            <li>If you've completed the application at home, make a copy of it before you submit it so you can refer to it when you follow up with the employer. You also can use it as a “cheat sheet” when you're filling out future applications.</li>
          </ul>

          <h2>Read and Follow Directions</h2>
          <ul>
            <li>Answer all questions. DO NOT write “See resumé.”</li>
            <li>Read the questions carefully so you're sure what information the employer is asking for and how they want the information entered.</li>
            <li>Always spell things out completely.</li>
            <li>DO NOT use abbreviations. If there isn't enough room, only abbreviate the common words.</li>
            <li>DO NOT write in sections that say "Do Not Write Below This Line" or "For Office Use Only."</li>
          </ul>

          <h2>Neatness Counts</h2>
          <ul>
            <li>Print neatly and clearly.</li>
            <li>Use a black or blue pen – no pencil!</li>
            <li>If you make a mistake, DO NOT scratch it out. Instead, cover the error with correction fluid, then write the correct answer.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t4-r7",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Completing Applications ONLINE",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r7_xvmahe.jpg",
      thumbnailDescription:
        "Person is completing a job application on a smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598189/audio/c3-t4-r7_quaegu.mp3",
        text: `
        <div class="article">
          <p>Keep these things in mind when completing an application online:</p>

          <h2>Create an Online Profile</h2>
          <ul>
            <li>Most employers require you to create a profile before you can fill out the application.</li>
            <li>You will be asked to select a User Name and Password. Many job seekers use the same User Name and Password for every online application. Your User Name is usually your email address. Your Password is a unique word or sequence of letters and numbers.</li>
            <li>Use your job search log to document the User Name and Password for each application so you can check on the status of your application.</li>
          </ul>

          <h2>Review It Before You Save</h2>
          <ul>
            <li>Some online applications won't allow you to return to previous pages so proofread your answers before moving to the next screen.</li>
            <li>Some online applications will not let you move on unless you answer all questions.</li>
          </ul>

          <h2>Beware of the “Time Out” Trap</h2>
          <ul>
            <li>Read the instructions before you start to find out how much time you will have to complete the application. Sometimes, the employer's computer system will “time out” if you take too much time forcing you to start over.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c3-t4-r8",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Following Up on an Application",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r8_fipe2x.jpg",
      thumbnailDescription:
        "Young woman who is blind is using her smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658021/audio/c3-t4-r8_01_e9xizu.mp3",
        text: `
        <div class="article">
          <p>After you've applied for a job or submitted a resumé, it's important to follow-up regularly. Try to check in by phone, email or in-person within a week after submitting your application and then follow-up weekly after that. Be sure to check your email and voice mail every day to see if a potential employer has contacted you. Keep following up until you learn that the job has been filled or you find another job.</p>
          <p>If possible, follow-up with the hiring manager directly. If you have their email address, start with that. If you don't know their name or have their contact information, call the company's main number, say that you're following up on a job application and ask to speak to a manager. Be prepared to leave a message.</p>
          <p>When following up by telephone:</p>
          <ul>
            <li>Practice what you want to say before calling.</li>
            <li>Keep the conversation short and professional.</li>
            <li>Include your first name, last name and phone number.</li>
            <li>Explain why you're calling. Refer to the position that you applied for and when you applied.</li>
            <li>Repeat that you're interested in the job.</li>
            <li>Ask when the person expects to make a decision.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c3-t4-r9",
      categoryId: "c3",
      topicId: "c3-t4",
      title: "Setting Up an Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657720161/images/c3-t4-r9_qrfife.jpg",
      thumbnailDescription:
        "Woman is talking on a smart phone while looking at her laptop.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697763820/audio/c3-t4-r9_02_sm1zil.mp3",
        text: `
        <div class="article">
          <p>With persistence and a bit of luck, your efforts will pay off with an interview. A potential employer may contact you by telephone or email to arrange an interview.</p>
          <p>Review the four things you should find out if an employer calls you.</p>

          <h2>1. The Type of Interview</h2>
          <p>There are lots of different types of job interviews. For example, you might be interviewed over the telephone, virtually or in person. Ask the person who contacted you what you should expect.</p>

          <h2>2. Who You'll Be Meeting</h2>
          <ul>
            <li>Ask for the name, email, job title and phone number of the person you'll be interviewing with.</li>
            <li>Ask for the street address where the interview will take place, including an office or suite number.</li>
            <li>Repeat the address to the person or print a copy of the email so you're sure.</li>
            <li>If you're interviewing by phone, find out if you're calling in or if someone will be calling you.</li>
            <li>If you're interviewing virtually, make sure you receive the link and any passcodes needed.</li>
            <li>Plan to arrive a few minutes early to get settled.</li>
          </ul>

          <h2>3. Where and When to Meet</h2>
          <p>It's important to find out the date, day of the week and time of the interview. Write this information down!</p>

          <h2>4. Ask If You'll Need to Take an Employment Test</h2>
          <p>Some employers may ask you to take a skills test or other type of employment test. Ask the employer if this will be required and how the test will be conducted.</p>
        </div>`,
      },
      keywords: [],
    },
    // Category 4 resources
    {
      id: "c4-t1-r1",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Overview of the Interview Process",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669429/images/c4-t1-r1_01_bfadui.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription:
        "Warehouse Manager is interviewing a young black man in a warehouse setting",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740325/audio/c4-t1-r1_01_dcz0f0.mp3",
        text: `
        <div class="article">
          <h2>BEFORE the Interview</h2>
          <ul>
            <li>Learn about the company and job</li>
            <li>Match your skills to a job</li>
            <li>Get ready for questions</li>
            <li>Plan what to wear, bring, and how you'll get there</li>
            <li>Review what to say and do</li>
          </ul>
          <h2>DURING the Interview</h2>
          <ul>
            <li>Meet the interviewer</li>
            <li>Make a good impression</li>
            <li>Answer and ask questions</li>
            <li>Act your best</li>
          </ul>
          <h2>AFTER the Interview</h2>
          <ul>
            <li>Send a thank-you message or email</li>
            <li>When and how to follow up with the employer</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r2",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Purpose of the Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965984/images/c4-t1-r2_p1ltkv.jpg",
      thumbnailDescription:
        "Woman in a wheelchair is shaking the hand of an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219734/audio/c4-t1-r2_z2btur.mp3",
        text: `
        <div class="article">
          <p>An interview is a chance for you and the employer to meet and get to know one another. But each of you has different goals.</p>
          <h2>The Employer Wants to Know:</h2>
          <ul>
            <li>How you talk and act.</li>
            <li>If you have the right skills.</li>
            <li>About your education and past employment experiences.</li>
            <li>If you will fit in with other employees.</li>
          </ul>
          <p>The interviewer will ask about you and your skills. You may be asked to take a test or show what you can do.</p>
          <h2>YOU'RE Trying to:</h2>
          <ul>
            <li>Make a good impression.</li>
            <li>Show the employer that you look and act professional.</li>
            <li>Show how well you get along with people.</li>
            <li>Show that your skills and experiences are what the employer is looking for.</li>
            <li>Learn if the job and the employer are a good match for you.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r3",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Types of Job Interviews",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965984/images/c4-t1-r3_g5mclu.jpg",
      thumbnailDescription: "Young woman is talking with an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740335/audio/c4-t1-r3_02_cx0cdt.mp3",
        text: `
        <div class="article">
          <p>There are many types of job interviews. Here are some common types of interviews:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r4",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Telephone Screening",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965984/images/c4-t1-r4_aupfr8.jpg",
      thumbnailDescription: "Person is talking to an interviewer via a laptop.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740336/audio/c4-t1-r4_01_esnkjr.mp3",
        text: `
        <div class="article">
          <p>The point of a telephone screening is to find out who should be invited for a full interview. Usually, a Human Resources person calls you and asks some basic questions before setting a more formal interview with another person.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r5",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Work Sample Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965984/images/c4-t1-r5_x9an3p.jpg",
      thumbnailDescription:
        "Person is showing samples of different graphic designs.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740338/audio/c4-t1-r5_01_w5cxp9.mp3",
        text: `
        <div class="article">
          <p>You may be asked to show how you would do the work, shadow someone else while they do the job or bring samples of your work.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r6",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "One-On-One Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t1-r7_gfzwbs.jpg",
      thumbnailDescription: "Person is meeting an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740339/audio/c4-t1-r6_01_qltrrt.mp3",
        text: `
        <div class="article">
          <p>After the employer has looked at your application, they want to learn more about you and see how you'll fit in. The goal is to represent yourself, your skills and experiences in the best way possible. One-on-one interviews may occur in person or virtually.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r7",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Group Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t1-r6_pa8lxc.jpg",
      thumbnailDescription:
        "Interviewer is talking with four people via a laptop.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740340/audio/c4-t1-r7_01_q73jxo.mp3",
        text: `
        <div class="article">
          <p>There are two kinds of group interviews. A group of people may interview you. Or, in some cases, you may be part of a group of applicants interviewing for the job at the same time. This may occur in person or virtually.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t1-r8",
      categoryId: "c4",
      topicId: "c4-t1",
      title: "Video Screening",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697766676/images/c4-t1-r8_02_jg4tea.jpg",
      thumbnailDescription:
        "Young white man is using a company website to produce a video answering the following question: Why would you be a good person for this job?",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740341/audio/c4-t1-r8_01_cnd0ff.mp3",
        text: `
        <div class="article">
          <p>Like a telephone screening, but instead you record and submit a video in the company's system. The video typically includes introducing yourself and responding to a few basic questions.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r1",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Learning About the Company",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r1_cgyxgy.jpg",
      thumbnailDescription:
        "Company's website displaying a web page called Creating Opportunities.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740315/audio/c4-t2-r1_02_bgv5dk.mp3",
        text: `
        <div class="article">
          <h2>Learning About an Employer</h2>
          <p>It's important to learn about the employer before you go to the interview. This will help you to feel more comfortable during the interview.</p>
          <p>You should try to find out:</p>
          <ul>
            <li>Basic information about the company – size, number of locations, number of employees, industry, etc.</li>
            <li>How it makes its money.</li>
            <li>The company's mission and vision.</li>
            <li>Its competition.</li>
          </ul>
          <h2>Do Your Homework!</h2>
          <p>There are lots of places to find information on potential employers.</p>
          <ul>
            <li>Company web site</li>
            <li>Social media channels</li>
            <li>Other online sources</li>
            <li>Newspapers</li>
            <li>Company brochures and reports</li>
            <li>Current and former employees</li>
          </ul>
          <p>There are a lot of people who can help. Talk to:</p>
          <ul>
            <li>Your job counselor or placement specialist</li>
            <li>Workforce Center staff</li>
            <li>Library staff</li>
            <li>Friends and relatives</li>
          </ul>
          <p>Once you know more about the company, you should find out more about the job you are interviewing for.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r2",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Learning About the Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r2_ztzci9.jpg",
      thumbnailDescription:
        "Hand is holding a smart phone displaying the words Job Descriptions.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740308/audio/c4-t2-r2_02_nsn9t9.mp3",
        text: `
        <div class="article">
          <p>Knowing about the specific job you're interviewing for will help you:</p>
          <ul>
            <li>Answer questions better.</li>
            <li>Know which skills set you apart from other people applying for the job.</li>
            <li>Show the employer your level of interest.</li>
          </ul>
          <p>You can learn about the job in several ways. You can:</p>
          <ul>
            <li>Ask the employer for a copy of the job description.</li>
            <li>Review the job description in the online job posting.</li>
            <li>Ask your placement specialist or job counselor to share what they know about this type of job.</li>
            <li>Watch someone else do the job.</li>
            <li>Talk to others who are doing the job now or who have done it in the past.</li>
            <li>Research some online resources such as O*Net OnLine and ISEEK.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r3",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Matching Your Skills to the Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r3_ttji6b.jpg",
      thumbnailDescription: "Woman is talking with an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740307/audio/c4-t2-r3_02_fuivpp.mp3",
        text: `
        <div class="article">
          <p>Most interviews last less than 30 minutes. You can show the interviewer that you are the best person for the job by focusing on the skills, experiences and talents that are most important for this particular position. This will make it a lot easier to answer questions.</p>
          <h2>Why It's Important</h2>
          <p>Matching your skills and experience to the job requirements will help:</p>
          <ul>
            <li>You sell yourself during the job interview.</li>
            <li>Make you feel more comfortable.</li>
            <li>Boost your confidence.</li>
          </ul>
          <p>Answer these questions to help you match your skills to the job:</p>
          <ul>
            <li>Which skills do you think will be most important in this job?</li>
            <li>What skills do you have that make you a good candidate for this position?</li>
            <li>Are you missing any critical skills?</li>
            <li>What are they?</li>
            <li>Is there a way that you can get these skills?</li>
            <li>Do you have a similar skill?</li>
            <li>What experiences have you had that make you a good candidate? Don't forget volunteer activities or hobbies!</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r4",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "How to Describe Yourself",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r4_wpyfp5.jpg",
      thumbnailDescription:
        "Young man with a disability is professionally dressed.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219729/audio/c4-t2-r4_cmlunc.mp3",
        text: `
        <div class="article">
          <p>There's one last thing you need to think about before you tackle the best way to answer interview questions. The missing piece? You!</p>
          <p>How will you describe yourself during the interview? Most interviewers will ask you to tell them about yourself. How will you answer?</p>
          <h2>Don't Be Shy!</h2>
          <p>It's always difficult to talk about yourself. A job interview isn't the time to be shy or worry about bragging!</p>
          <p>What words would you use to describe yourself as a person, employee and co-worker?</p>
          <p>Here are some sample words:</p>
          <div class="grid-list">
            <ul>
              <li>Organized</li>
              <li>Loyal</li>
              <li>Fast learner</li>
              <li>Eager</li>
              <li>Hard worker</li>
              <li>Educated</li>
              <li>Efficient</li>
              <li>Fun-loving</li>
              <li>Likes a challenge</li>
              <li>Flexible</li>
              <li>Loyal</li>
              <li>Pleasant</li>
              <li>Funny</li>
              <li>Bright</li>
              <li>Positive</li>
              <li>Detail-oriented</li>
              <li>Easy-going</li>
              <li>Dedicated</li>
              <li>Problem solver</li>
              <li>Reliable</li>
              <li>Focused</li>
              <li>Enjoys helping others</li>
              <li>Competent</li>
              <li>Likes variety</li>
              <li>Creative</li>
              <li>Talented</li>
              <li>Dependable</li>
              <li>Outgoing</li>
              <li>Kind</li>
            </ul>
          </div>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r5",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Answering Interview Questions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r5_ppeeeb.jpg",
      thumbnailDescription:
        "Woman in a wheelchair is talking with an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740309/audio/c4-t2-r5_02_f7esav.mp3",
        text: `
        <div class="article">
          <p>There's no list of questions that every interviewer asks. Most employers are trying to find out the same kind of information. If you know what the questioner is trying to find out, you'll have a better chance of giving the right answer!</p>
          <p>Most interviewers are trying to learn:</p>
          <ul>
            <li>About you.</li>
            <li>How your skills and experience match the job requirements.</li>
            <li>If you are dependable and work hard.</li>
            <li>How you get along with others.</li>
          </ul>
          <h2>Questions, Questions</h2>
          <p>Interview questions usually fall into one of three categories:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r6",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Direct Questions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r6_wjjgir.jpg",
      thumbnailDescription: "Interviewer is asking questions.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219730/audio/c4-t2-r6_apl3dc.mp3",
        text: `
        <div class="article">
          <p>These questions typically require a simple, clear answer.</p>
          <p>Examples:</p>
          <ul>
            <li>Do you currently have a job?</li>
            <li>Have you ever worked in customer service before?</li>
            <li>Do you know how to operate a cash register?</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r7",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Indirect or Open-Ended Questions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r7_yfw6h3.jpg",
      thumbnailDescription: "Interviewer is asking questions.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219730/audio/c4-t2-r7_mrzrti.mp3",
        text: `
        <div class="article">
          <p>Indirect questions allow you to explain your answers in more detail. You shouldn't answer these questions with a simple “yes” or “no.”</p>
          <p>Examples:</p>
          <ul>
            <li>What are your greatest strengths?</li>
            <li>What do you want to be doing a year from now?</li>
            <li>Why should I hire you?</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r8",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Problem-Solving Questions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r8_gto6e8.jpg",
      thumbnailDescription: "Interviewer is asking questions.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740306/audio/c4-t2-r8_02_pcmm7a.mp3",
        text: `
        <div class="article">
        <p>Interviewers ask problem-solving questions to decide if you are a good problem solver.</p>
        <p>Examples:</p>
        <ul>
          <li>How would you respond to an angry customer?</li>
          <li>What would you do if you knew that a coworker was violating company policy?</li>
        </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r9",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "It's All About You: What Every Interviewer Wants to Know",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r9_kn9ukr.jpg",
      thumbnailDescription: "Interviewer is asking questions.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740322/audio/c4-t2-r9_02_nd5k6h.mp3",
        text: `
        <div class="article">
          <p>Potential employers interview candidates for a job because they need to hire someone with specific skills or personality traits.</p>
          <p>Employers are interested in:</p>
          <ul>
            <li>How you get along with others.</li>
            <li>Your background, education, skills and experience.</li>
            <li>Your interests, strengths and weaknesses.</li>
            <li>Your career goals.</li>
          </ul>
          <p>Remember, even if a question is phrased differently, the intent is still the same. Here are some examples of questions that an interviewer might ask.</p>
          <ul>
            <li>Tell me about yourself.</li>
            <li>What do you think are your strengths?</li>
            <li>What are your weaknesses?</li>
            <li>Why are you interested in this job?</li>
            <li>How would others describe you?</li>
            <li>What do you think a former employer would say about you?</li>
            <li>What are your career goals?</li>
            <li>What specific skills do you bring to this job?</li>
            <li>Describe your technology skills.</li>
            <li>Why do you want this job?</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r10",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Interview Questions About “Will You Fit In?”",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r10_nf751f.jpg",
      thumbnailDescription: "Interviewer is smiling.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740310/audio/c4-t2-r10_02_tm0a3y.mp3",
        text: `
        <div class="article">
          <p>Companies spend a lot of time and money hiring and training employees. They want to be sure that you will get along well with other employees.</p>
          <p>Here are examples of the kinds of questions that an employer might ask to decide if you'll fit in.</p>
          <ul>
            <li>Why do you think I should hire you?</li>
            <li>Do you think you are a team player?</li>
            <li>What experiences make you the right person for this job?</li>
            <li>Describe your ideal work environment.</li>
            <li>What do you know about our company?</li>
            <li>What can you contribute to our organization?</li>
            <li>Describe a conflict you've had and how you resolved it.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r11",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Interview Questions You Might Want to Ask",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965981/images/c4-t2-r11_kyqpxk.jpg",
      thumbnailDescription:
        "Young woman is looking pensive as the interviewer reviews her resumé.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740314/audio/c4-t2-r11_02_qdpbc5.mp3",
        text: `
        <div class="article">
          <p>Remember, a good interview is really just a formal conversation between two people. Most interviewers will ask if you have any questions. The interviewer will be impressed if you do! You might want to ask.</p>
          <ul>
            <li>How would you describe your company's culture?</li>
            <li>What is the company's vision for the future?</li>
            <li>When do you plan to make a decision?</li>
            <li>How many people work in this group?</li>
            <li>Is this a new position?</li>
            <li>Why is this position open?</li>
            <li>What would a typical day be like?</li>
            <li>What training will I receive?</li>
            <li>What is the next step in the hiring process?</li>
            <li>What do you like best about working here?</li>
            <li>What is the company dress code?</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r12",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Interview Questions You Don't Need to Answer",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r12_di0jyg.jpg",
      thumbnailDescription: "Interviewer is talking with a young woman.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740334/audio/c4-t2-r12_02_uglp4b.mp3",
        text: `
        <div class="article">
          <p>Potential employers can only ask questions directly related to:</p>
          <ul>
            <li>The position they are trying to fill.</li>
          </ul>
          <p>In general, potential employers cannot ask questions related to:</p>
          <ul>
            <li>Race</li>
            <li>Religion</li>
            <li>National origin</li>
            <li>Gender</li>
            <li>Age</li>
            <li>Marital status</li>
            <li>Sexual orientation</li>
            <li>Military or veteran status</li>
            <li>Health or disability – However, an employer can ask limited questions about reasonable accommodations should a disability be obvious, was voluntarily disclosed or you requested an accommodation. They may not ask about the nature or severity of the disability.</li>
          </ul>
          <p>Most employers aren't trying to trick you. This can be confusing for employers too!</p>
          <p>For example, an interviewer can't ask if you are a legal U.S. citizen but can ask if you're authorized to work in the United States.</p>
          <p>If an employer asks an inappropriate or illegal question, you don't have to answer. Try this approach:</p>
          <ul>
            <li>Politely respond that you'd prefer not to answer because it will not impact your job performance.</li>
            <li>Be calm and polite and ask the interviewer why the question is relevant to the job.</li>
            <li>Stay professional. Don't get defensive or angry.</li>
            <li>Avoid the question and steer the conversation elsewhere.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r13",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Answering the Tough Questions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r13_xlmap1.jpg",
      thumbnailDescription: "Young black man is listening to the interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219730/audio/c4-t2-r13_re2cn9.mp3",
        text: `
        <div class="article">
          <p>Everyone has done things that they'd like to forget or don't want to talk about. Potential employers have the right to ask you about issues that they think may affect their business risks or your ability to do the job.</p>
          <p>The best advice is to be honest, direct and upfront if you're asked about sensitive issues like criminal history or gaps in employment.</p>
          <p>Before you go to a job interview, look over your resumé or application. Are there areas that interviewers are likely to want more information about? Decide in advance how you'll answer tough questions related to:</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c4-t2-r14",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Addressing Criminal History",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r14_ppirjl.jpg",
      thumbnailDescription:
        "Stack of paper with the words Convicted Criminal overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740331/audio/c4-t2-r14_01_mvcvoy.mp3",
        text: `
        <div class="article">
          <p>Be upfront if you have a criminal conviction in your past. Most applications include a question on felony convictions. Always answer as honestly as possible. Remember, a background check will bring your criminal history to light.</p>
          <p>It is illegal for a potential employer to ask if you've ever been arrested or been in jail. However, an employer can ask about any convictions as long as:</p>
          <ul>
            <li>All applicants are asked a question.</li>
            <li>The question relates to your ability to do the job.</li>
          </ul>
          <h2>How should you respond?</h2>
          <p>If an employer asks if you have been convicted of a crime and you have been:</p>
          <ul>
            <li>Say yes.</li>
            <li>Briefly explain the situation. You do not need to over-explain or provide details of the offense and sentence.</li>
            <li>It is all right to say, "I have put the past behind me. I am looking forward to the future."</li>
            <li>You may also share a little about what you learned and/or how you've changed.</li>
            <li>Provide a letter of explanation if you have one.</li>
          </ul>
          <p>Honesty is always the best policy. If you are hired and didn't tell the truth about your criminal history, you could be fired for lying on your application.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r15",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Employment Gaps",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r15_u0zztt.jpg",
      thumbnailDescription: "Resume is split in two depicting a gap.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740317/audio/c4-t2-r15_01_qf2etd.mp3",
        text: `
        <div class="article">
          <p>Be prepared to answer questions about gaps in your work history. The best advice is to be direct and honest. The most common ways that job seekers explain gaps in employment are:</p>
          <ul>
            <li>Personal reasons.</li>
            <li>Going back to school.</li>
            <li>Taking time off for additional training.</li>
            <li>Job loss or layoff.</li>
          </ul>
          <p>Whatever the reason, focus on the positive. There's no need to go into a lot of detail. Use this as a chance to discuss how you built your skills during that time. For example, mention volunteer activities or a new computer program that you have learned.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r16",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Should You Disclose a Disability?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r16_h9gx0s.jpg",
      thumbnailDescription:
        "Young man is meeting with a supervisor in the cafeteria.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740311/audio/c4-t2-r16_02_ebggz7.mp3",
        text: `
        <div class="article">
          <p>Deciding whether or not to share about or disclose a disability can be a tough decision. You will need to decide if (and when) to share that information with an employer. The only time that you are required to disclose a disability is if it will interfere with your ability to do the job you've applied for.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r17",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Disclosing a Visible Disability",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r17_kc4yac.jpg",
      thumbnailDescription:
        "Young woman with Down Syndrome is sitting at her desk.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740316/audio/c4-t2-r17_02_gslnue.mp3",
        text: `
        <div class="article">
          <p>If you have an obvious disability, consider giving the employer a heads-up by disclosing your disability before the interview. Be matter-of-fact and positive when you bring the issue up.</p>
          <p>For example, if you use a wheelchair, you may want to disclose this to the interviewer so they can change the location of the interview to an accessible place or make other adjustments to make the experience more comfortable for you. You don't have to make a big deal of it. Simply ask if the location of the interview is wheelchair accessible.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r18",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Disclosing an Invisible Disability",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r18_ildgau.jpg",
      thumbnailDescription:
        "Young man is standing in front of his desk with a laptop.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740318/audio/c4-t2-r18_02_crxy4m.mp3",
        text: `
        <div class="article">
          <p>If you have an invisible or hidden disability, such as mental health condition, chronic pain or fatigue, the decision to share that information is more complex. Disclosure is a personal decision.</p>
          <p>You are not required to tell a potential employer about your disability or condition, either during the interview or after you've been hired unless you are asking for a reasonable accommodation.</p>
          <p>However, if you ask for an accommodation after you're hired and use a disability as the reason, you will be required to disclose your disability at that time.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r19",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Pros and Cons of Disclosure",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r19_tzjgxk.jpg",
      thumbnailDescription: "Dictionary page is showing the word Disclose.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740329/audio/c4-t2-r19_02_rg0smp.mp3",
        text: `
        <div class="article">
          <p>There are many pros and cons to consider when disclosing a disability. Each individual situation is different, so this may be something to discuss with your job counselor or placement specialist.</p>

          <h2>Pros (Upsides)</h2>
          <ul>
            <li>You can openly discuss reasonable accommodations.</li>
            <li>You will have legal protection against discrimination.</li>
            <li>It can reduce stress. Feeling something needs to be 'secretive' takes a lot of energy.</li>
            <li>Advocating for yourself can be empowering.</li>
            <li>If hired, the employer can judge your performance and support needs more fairly.</li>
            <li>Employers often have financial incentives like tax breaks if they hire people with disabilities.</li>
          </ul>

          <h2>Cons (Downsides)</h2>
          <ul>
            <li>Even though the law protects people with disabilities from work-place discrimination, it could still impact your chances of being hired.</li>
            <li>It can feel stressful and uncomfortable to share personal and sensitive information.</li>
            <li>It can impact how others treat or view you.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r20",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Tips for Disclosing a Disability",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r20_rc3y23.jpg",
      thumbnailDescription:
        "Woman with a disability is working at a fast-food restaurant in a mall.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740319/audio/c4-t2-r20_02_ndzj1j.mp3",
        text: `
        <div class="article">
          <p>If you decide to disclose a disability:</p>
          <ul>
            <li>Decide how you will describe your disability.</li>
            <li>Write a disclosure letter that briefly explains your situation and includes reasonable accommodations.</li>
            <li>Work with a job coach or other person to script, role play or practice what you want to say.</li>
            <li>Be positive. For example, you might share how the disability has helped you be empathetic, understanding or good at adapting to change.</li>
            <li>Stay focused on your skills and what you can do.</li>
            <li>Suggest simple accommodations that will help you succeed.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r21",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Companies May Require Testing",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r21_l4tcpl.jpg",
      thumbnailDescription: "Man is answering questions in an employment test.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740346/audio/c4-t2-r21_01_cf56sx.mp3",
        text: `
        <div class="article">
          <p>Some companies may ask you to take a test as part of the interview process or as part of a job offer. This is acceptable as long as all candidates for the job are required to take or pass the same test.</p>
          <p>When you schedule the interview, remember to ask the employer to describe the interview process.
          If he or she mentions that you will need to take a test, ask:</p>
          <ul>
            <li>The type of tests that are required.</li>
            <li>What the employer hopes to learn from the test.</li>
            <li>When and where you will need to take the test.</li>
          </ul>
          <p>There are five general categories of pre-employment tests:</p>
          <ol>
            <li><b>Personality Tests:</b> These tests help an employer decide if you have the type of personality that they think is important for the job. Personality tests can be filling out a questionnaire or a psychological interview.</li>
            <li><b>Skills Assessments:</b> A skills assessment makes sure that you actually have the skills that you said you have. You may be asked to complete a task that is part of the job, use a computer, etc.</li>
            <li><b>Aptitude Tests:</b> Aptitude tests help an employer decide if you are able to learn the new skills you'll need to be successful in a job.</li>
            <li><b>Honesty Tests:</b> These tests help employers evaluate if an applicant is honest. Questions generally relate to how you have handled past situations, how you would react in a situation, personal and workplace ethics, etc.</li>
            <li><b>Medical and Drug Tests:</b> Some employers require applicants to pass a physical examination to make sure they don't have a physical condition that prevents them from doing the job. An employer can only require a physical examination after you have been offered the job.</li>
          </ol>
          <p>More and more employers require drug screenings. All job candidates must be required to pass the same screening test. The candidate is required to provide a sample of urine or saliva, which is then tested by a professional laboratory. The goal is to determine if the applicant has taken illegal drugs.
          You can refuse to have a physical exam or take a drug test. However, if all candidates must pass these tests, refusing to do so would most likely result in the job offer being withdrawn.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t2-r22",
      categoryId: "c4",
      topicId: "c4-t2",
      title: "Background Tests",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t2-r22_hnilkf.jpg",
      thumbnailDescription:
        "Request for Criminal Background Check form in a clipboard.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657562008/audio/c4-t2-r22_kqfvdf.mp3",
        text: `
        <div class="article">
          <p>Many employers hire an independent third-party to check job candidates' backgrounds using public records. These investigations usually focus on work history, financial situation, criminal history and education.</p>
          <p>You must first sign a consent form that allows a potential employer to look into your background. While you can refuse, this may also result in the job offer being withdrawn.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r1",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Importance of First Impressions",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t3-r1_fx1cmk.jpg",
      thumbnailDescription:
        "Young Asian man in a wheelchair is smiling and shaking the interviewer's hand.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740323/audio/c4-t3-r1_01_vlrlqt.mp3",
        text: `
        <div class="article">
          <p>It won’t take you long to form an impression for you in an interview.</p>
          <ul>
            <li><b>Body language</b> means non-verbal signals and cues. Facial expressions, body movement (gestures), eye contact, posture, touch and proximity to others are all aspects of body language.</li>
            <li>With practice, you can make your body language or non-verbal communication work for you in an interview.</li>
            <li>Your physical appearance makes a big impression. Dressing professionally shows your respect for the interview process and the company.</li>
            <li>Your behavior, how you act, your attitude and communication style leave the biggest impression on the interviewer and greatly impact whether you are offered the job.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r2",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Good Hygiene Tips",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t3-r2_mq07ow.jpg",
      thumbnailDescription:
        "Variety of towels, soaps, shampoos and toothbrushes.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658023/audio/c4-t3-r2_01_tcfpo9.mp3",
        text: `
        <div class="article">
          <p>Good hygiene is part of a good first impression, so pay close attention to your cleanliness and grooming. Here are some quick reminders:</p>
          <ol>
            <li>Take a shower or bath.</li>
            <li>Wash your hair.</li>
            <li>Clean and trim your fingernails.</li>
            <li>Brush your teeth.</li>
            <li>Use deodorant.</li>
            <li>Men should shave.</li>
            <li>Women's makeup should be toned down.</li>
            <li>Comb or style your hair in a simple professional way — nothing fancy.</li>
            <li>Don't wear heavy perfume or a lot of aftershave.</li>
          </ol>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r3",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Dress for Success",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669429/images/c4-t3-r3_01_kamre2.jpg",
      thumbnailDescription:
        "Three people are in the waiting room and are wearing professional pants, shoes, suits and blouses.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740326/audio/c4-t3-r3_01_fw7oq3.mp3",
        text: `
        <div class="article">
          <ul>
            <li>Most employers think people's clothing, hygiene and grooming show their attitude toward the job.</li>
            <li>Dress professionally, which means wearing clothes and accessories that match the professional workplace.</li>
            <li>Do some research on the company's dress code. For example, you can:</li>
            <ul>
              <li>Visit the company's location to see how employees are dressed (if convenient).</li>
              <li>Email or call your interview point of contact for guidance.</li>
              <li>Review their website 'about us' section and/or social media for clues.</li>
            </ul>
          </ul>
          <p>Choose clothing, hairstyles and accessories that will help you feel confident and make a good impression.</p>
          <p>Here are general do's and don'ts:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r4",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Clothes",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t3-r4_up725d.jpg",
      thumbnailDescription:
        "Two people are in the waiting room and are wearing professional pants, shoes, suits and blouses.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740345/audio/c4-t3-r4_01_rrq6ac.mp3",
        text: `
        <div class="article">
          <ul>
            <li>Wear clean and ironed (pressed, wrinkle-free) clothes that are free of rips, holes or stains.</li>
            <li>Dress better than what you believe is required. For example, if employees wear jeans to work, you should wear khakis to the interview. Dress one step up from what those in the workplace environment typically wear.</li>
            <li>Choose modest, properly fitting clothing (not too loose, baggy, low cut, revealing or tight).</li>
            <li>Don't show your undergarments, lingerie, underwear or bra. If your pants are baggy, wear a belt – no sagging!</li>
            <li>NO shorts, sweatpants, athletic-ware, light-colored or ripped jeans, Capri pants, t-shirts, heels 4 inches or higher, sunglasses, flip-flops or clothes with attention-grabbing images or graphics.</li>
            <li>Choose solids and mostly neutral colors (black, navy, gray, brown) for the majority of what you wear.</li>
            <li>Bright colors and prints can work well as accents, but you want to look professional and limit any possible distractions. Keep it simple for the interview.</li>
            <li>If you have a pet, be sure to remove any fur from your clothing.</li>
            <li>Balance and blend ethnic, cultural or traditional clothing with contemporary or formal clothing.</li>
            <li>If you aren't sure about if your clothing is appropriate for the job interview, show it to an employed adult and ask for their advice.</li>
            <li>Prioritize feeling confident and comfortable.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r5",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Accessories",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965983/images/c4-t3-r5_buap4a.jpg",
      thumbnailDescription:
        "Young man is wearing a nose ring and has tattoos on his arms.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740332/audio/c4-t3-r5_01_ifjpev.mp3",
        text: `
        <div class="article">
          <p>Employers differ in what is accepted for accessories or types of self-expression. As a general rule, anything that interferes with your (and others) ability to perform the job effectively and safely should be left at home, covered or removed.</p>
          <p>Keep accessories simple. For example, minimal jewelry and a basic solid belt. If you wear a scarf, headscarf or hijab, consider a solid and more neutral color. Make sure your shoes are clean, too!</p>
          <p>Ask yourself, “Will wearing or showing this detract from my possibility of getting the job?”</p>
          <ul>
            <li>Hats or big hair accessories</li>
            <li>Visible face and body piercings</li>
            <li>Big, loud or flashy jewelry</li>
            <li>Tattoos</li>
            <li>Logos, graphics or words on clothing that are likely offensive to others</li>
          </ul>
          <p>It's important to dress appropriately for the location of the interview and the type of company or employer. You will also want to consider the weather and the season or time of year. For example, if you are wearing a hat due to the cold winter, be sure to remove the hat and tidy up your hairstyle before the interview. Avoid 'hat hair'!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r6",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "What to Wear: Business Formal and Professional",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r6_01_xetqbg.jpg",
      thumbnailPosition: "50% 50%",
      thumbnailDescription:
        "Young black man is in the waiting room holding his smart phone and is wearing a shirt, tie, blazer, dress pant and dress shoes.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740321/audio/c4-t3-r6_01_nd5lvr.mp3",
        text: `
        <div class="article">
          <ul>
            <li>Bottoms: suit, pantsuit, slacks, trousers, knee-length or longer dress, skirt suit, dark tights.</li>
            <li>Tops: button-down shirt, tie, jacket, blouse, blazer.</li>
            <li>Shoes: close-toed - formal flats, pumps, moderate or low height heels, Oxfords, loafers, dress shoes.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r7",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "What to Wear: Business and Smart Casual",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965982/images/c4-t3-r7_xylixx.jpg",
      thumbnailDescription: "Two men are wearing khakis",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740328/audio/c4-t3-r7_01_abwtya.mp3",
        text: `
        <div class="article">
        <ul>
          <li>Bottoms: chinos, khakis, trousers, dress pants, pencil or long skirts, knee-length or longer dresses, dirac, leggings, jumpsuits, dark-wash or black jeans, slim pants, dress cropped pants.</li>
          <li>Tops: collared shirts, polos, button-down shirts, blouses, cardigans, sports jacket, blazers, sweaters, tunics, tanks with full coverage.</li>
          <li>Shoes: close-toed, flats, boots, pumps, moderate or low height heels, dress sneakers, loafers, mules (slides).</li>
        </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r8",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "What to Wear: Industry Specific",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r8_01_sc9uxq.jpg",
      thumbnailDescription:
        "Young black woman is wearing scrubs and caring for an elderly black woman.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697740312/audio/c4-t3-r8_01_fc2ufl.mp3",
        text: `
        <div class="article">
          <p>Once hired, some jobs have a specific dress-code, such as wearing scrubs at a medical office or athletic ware for a trainer. For the interview, however, you should dress business and smart casual, formal or professional. Do not dress casually for interviews.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r12",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Interview Preparation: What to Bring or Leave Behind",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1675386662/images/c4-t3-r12-replacement_nr5dt1.jpg",
      thumbnailDescription:
        "Young black woman is smiling as she rides a city bus.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658024/audio/c4-t3-r12_01_pybtjv.mp3",
        text: `
        <div class="article">
          <p>Decide what to bring to the interview and what to leave behind.</p>
          <ul>
            <li><b>Map/route</b> - Figure out where the interview will be held.</li>
            <li><b>Bus pass</b> - Plan your transportation and have a backup plan.</li>
            <li><b>Money</b> - Bring a little money or a bank card in case it is needed.</li>
            <li><b>File/portfolio</b> - Bring your papers in a folder or portfolio so they won't get crumpled.</li>
            <li><b>Note with contact info</b> - Bring your interviewer's name, address, and phone number.</li>
            <li><b>Notebook paper, pen</b> - Bring a notebook and pen or pencil so you can take notes.</li>
            <li><b>Resumé and References</b> - Bring an extra copy of your resume and a list of 3 references with phone numbers and e-mail addresses and any letters of explanation.</li>
            <li><b>Kleenex or hanky</b> - Carry a facial tissue or clean handkerchief in your pocket.</li>
            <li><b>Soda</b> - Beverages like soda or coffee will make you seem too casual or even rude. Plus, you could spill and lose your focus. If you need to bring a water bottle, dispose of it before the interview or keep it in your bag, purse or briefcase.</li>
            <li><b>Sandwich</b> - Do not bring food since it makes it seem like you're not serious about the job, and it's messy.</li>
            <li><b>Earbuds</b> - Do not listen to music—even with headphones or earbuds in the reception area—it can be distracting and considered rude.</li>
            <li><b>Music</b> - Do not listen to music—even with headphones in the reception area—since it's distracting and rude.</li>
            <li><b>Cell Phone</b> - Turn OFF your cell phone. Even if it's on silent or do not disturb, you may be tempted to look at it during your interview, which will seem rude and distracting to your interviewer.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume", "ipod", "mp3", "player", "mobile"],
    },
    {
      id: "c4-t3-r13",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Arriving at the Interview Tips",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965983/images/c4-t3-r13_gl6itj.jpg",
      thumbnailDescription:
        "Young man in a suit is waiting in the reception area.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658024/audio/c4-t3-r13_01_qqellq.mp3",
        text: `
        <div class="article">
          <p>Before You Walk into the Interview:</p>
          <ul>
            <li>Arrive 15 minutes early.</li>
            <li>Stop in a restroom.</li>
            <li>Check to see your hair, teeth, and clothes are still...</li>
            <li>Review your resumé and think about what you want the interviewer to know about you.</li>
            <li>If you forgot and brought gum, a toothpick, coffee or beverage, throw it out now.</li>
            <li>Turn off your cell phone.</li>
            <li>Take a few deep breaths to calm down.</li>
            <li>Give yourself a pep talk. You can do this!</li>
          </ul>
          <p>Inside the Reception or Greeting Area:</p>
          <ul>
            <li>Keep in mind that the interview begins the minute you enter the building.</li>
            <li>Greet the receptionist or other employees in a way that is authentic for you. For example, say hello - smile a little and be warm or friendly.</li>
            <li>As you wait, hold your body or posture in a way that shows you are interested and care about the interview and job. For example, you don't want to slouch down low in a chair, put your feet up on the furniture or pace around the room, which can send signals that you don't care about the job or are extremely nervous.</li>
            <li>Wait quietly and patiently. Keep your cell phone turned OFF.</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c4-t3-r14",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Body Language During the Interview Process",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1653965983/images/c4-t3-r14_lglenj.jpg",
      thumbnailDescription:
        "Confident young man is talking with an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219733/audio/c4-t3-r14_fyngs7.mp3",
        text: `
        <div class="article">
          <p>Body language is very important during the interview. 55% of the impression you make comes from how you look and act. Only 7% of the impression you make comes from your actual words.</p>
          <p>Body language means attitude and friendliness, eye contact, handshake and posture. With practice, you can make your body language and tone of voice work for you in an interview.</p>
          <p>See a series of videos showing bad and good body language:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r15",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Handshake Greeting",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r15_01_jvxm3s.jpg",
      thumbnailDescription:
        "Young man is firmly shaking the hand of the interviewer.",
      type: "video",
      content: {
        media: "pt_1nz02BHo",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> A handshake is a common greeting in U.S job settings. if you intend to greet with a handshake, make sure your shaking hand, usually the right, is clean and dry. Let the interviewer initiate or extend their hand to you first as the interviewer approaches. Position your body language to be ready to greet them. In this example, Sadiq could have stood up a little sooner to meet the interviewer. Try to match the grip pressure of the other person, not too hard, not too soft. Palm-to-palm contact for two to five seconds is the goal. A limp or partial handshake, like this example, can be an awkward start to the interview and indicate a lack of confidence. Here is an example of a firm two-to-five second handshake greeting.</p>
          <p><span class="transcript-speaker">Alan:</span> Hi, I'm Alan.</p>
          <p><span class="transcript-speaker">Sadiq:</span> Nice to meet you. I'm Sadiq.</p>
          <p><span class="transcript-speaker">Alan:</span> Good to meet you too. Alright. Let's get started.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r16",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Greeting Alternatives",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697765410/images/c4-t3-r16_02_fxprwh.jpg",
      thumbnailDescription:
        "Collage of four images displaying different greeting alternatives.",
      type: "video",
      content: {
        media: "iMS7VpxxVcE",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> In many job settings in the United States, a person may extend their hand out to you with the intent of greeting with a handshake. However, not everyone is comfortable with or able to shake hands and that is truly okay. There are many ways to greet someone. For example, you might decline and suggest an alternative. such as a shoulder or elbow bump like this:</p>
          <p><span class="transcript-speaker">Alan:</span> Hi Brad, I'm Alan.</p>
          <p><span class="transcript-speaker">Brad:</span> Hello Alan. Nice to meet you.</p>
          <p><span class="transcript-speaker">Alan:</span> Nice to meet you.</p>
          <p><span class="transcript-speaker">Brad:</span> I prefer an elbow bump.</p>
          <p><span class="transcript-speaker">Alan:</span> Okay, sure. Well, let's get going.</p>
          <p><span class="transcript-speaker">Narrator:</span> The key is to speak your preferences politely and confidently while remaining professionally friendly. Here are a few examples of politely declining a handshake greeting.</p>
          <p><span class="transcript-speaker">Alan:</span> Hi Mazin. I'm Alan.</p>
          <p><span class="transcript-speaker">Mazin:</span> Hi Alan. Nice to meet you. I don't shake hands. In my culture, I'm not allowed to shake hands.</p>
          <p><span class="transcript-speaker">Alan:</span> No worries. No worries. Well, let's get started. Shall we?</p>
          <p><span class="transcript-speaker">Mazin:</span> Yes, alright.</p>
          <p><span class="transcript-speaker">Alan:</span> You can follow me.</p>
          <p><span class="transcript-speaker">Alan:</span> Hi Alecia. I'm Alan.</p>
          <p><span class="transcript-speaker">Alecia:</span> It's wonderful to meet you too. But I'm not shaking hands right now. I'm sorry.</p>
          <p><span class="transcript-speaker">Alan:</span> No worries. Let's get started. Won't you follow me?</p>
          <p><span class="transcript-speaker">Alecia:</span> Okay.</p>
          <p><span class="transcript-speaker">Narrator:</span> You don't even have to decline. Instead, you can just greet the person orally with simple and friendly words. When you keep your hands down or behind your back, the body language will let the person know you aren't greeting with a handshake or physical touch.</p>
          <p><span class="transcript-speaker">Alan:</span> Hi Nicki. I'm Alan.</p>
          <p><span class="transcript-speaker">Nicki:</span> It's nice to meet you.</p>
          <p><span class="transcript-speaker">Alan:</span> It's nice to meet you too. Well, shall we get started? Why don't you follow me, okay?</p>
          <p><span class="transcript-speaker">Narrator:</span> A hand on the heart, a bow or a head nod is another common greeting. Some folks will also press their hands together toward their chest with fingertips pointing upward as they nod or bow the head.</p>
          <p><span class="transcript-speaker">Alan:</span> Hi Mazin. I'm Alan.</p>
          <p><span class="transcript-speaker">Mazin:</span> Hi Alan. Nice to meet you.</p>
          <p><span class="transcript-speaker">Alan:</span> Good to meet you. Well, you want to follow me? We'll get things started.</p>
          <p><span class="transcript-speaker">Mazin:</span> Yeah!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r17",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Personal Space",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r17_01_a36ury.jpg",
      thumbnailDescription:
        "Young black man is greeting the interviewer and is getting too close.",
      type: "video",
      content: {
        media: "hJzkrKQdmw0",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Most people prefer to have some personal space especially when it is someone they do not know. This first scene shows an example of inappropriate personal space. Being tightly near or too close of proximity to the interviewer can make you appear overly eager. Also, notice how the interviewer is uncomfortable having his personal space invaded and so he moves quickly away. Here is an example of appropriate personal space where you generally keep 2 to 3 feet away from the other person and follow the interviewer's lead.</p>
          <p><span class="transcript-speaker">Alan:</span> Hello, I'm Alan.</p>
          <p><span class="transcript-speaker">Sadiq:</span> Nice to meet you. I'm Sadiq.</p>
          <p><span class="transcript-speaker">Alan:</span> It's good to meet you too. Why don't you follow me and we'll get started.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r18",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Manners",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r18_01_qouflo.jpg",
      thumbnailDescription:
        "Young woman is politely talking with the interviewer.",
      type: "video",
      content: {
        media: "2dJzEGveYdw",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Don't display poor manners during the interview process.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Please Erin, have a.....OK. [Candidate takes a seat before the interviewer offers a seat.]</p>
          <p><span class="transcript-speaker">Interviewer:</span> So, tell me about you.</p>
          <p><span class="transcript-speaker">Candidate:</span> Well...</p>
          <p>[The scene is repeated.]</p>
          <p>Narrator: Show good manners and act as if you are a guest.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Please Erin, have a seat. [Candidate waits for an invitation to take a seat.]</p>
          <p><span class="transcript-speaker">Candidate:</span> Oh, thank you.</p>
          <p>Narrator: Follow the interviewer's lead.</p>
          <p><span class="transcript-speaker">Interviewer:</span> It's good of you to come in.</p>
          <p><span class="transcript-speaker">Candidate:</span> Thank you.</p>
          <p><span class="transcript-speaker">Interviewer:</span> So, tell me about you.</p>
          <p><span class="transcript-speaker">Candidate:</span> Well...</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r21",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Body Language",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r21_01_uqb6kx.jpg",
      thumbnailDescription:
        "Young black man is wearing a polo shirt and is demonstrating direct eye contact in an interview.",
      type: "video",
      content: {
        media: "ezQuSqcnHaI",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Body language is an important part of an interview. Here is an example of appropriate or good body language. Sit up straight and lean in slightly toward the interviewer. Smiling comfortably throughout the interview communicates you are enthusiastic, friendly, confident and trustworthy.</p>
          <p>Mostly direct eye contact or looking near the interviewer's face or head shows sincerity, confidence and can even be considered a sign of respect.</p>
          <p>An occasional slight head nod can show the interviewer you are listening, understanding or agree with what they're saying or asking. While some nods to show that you're listening or in agreement are good. don't go overboard with a lot of nodding as this can signal anxiety or over excitement.</p>
          <p>Poor body language can indicate a lack of interest in the job or be interpreted as a sign of disrespect to the interviewer. Avoid moving your eyes all over or allowing your gaze to wander a lot as this can send a message of boredom or dishonesty.</p>
          <p>You also want to keep your posture relatively still and upright. So, avoid turning around frequently slouching or swiveling in your chair. During the interview, keep hand arm and bigger body movements to a minimum.</p>
          <p>Yawning, burping and rubbing your eyes or face does not send a positive message to the interviewer so avoid this. Avoid picking at or biting your fingernails. Also, avoid scratching your body, clenching or grinding your teeth and tapping your hands or fingers. Remember, body language mistakes like these can hurt your chances of getting a job.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r22",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Ending the Interview Tips",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697764393/images/c4-t3-r22_02_hbyh7c.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription:
        "Young black man is ending his interview in a warehouse setting.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219733/audio/c4-t3-r22_ohsszs.mp3",
        text: `
        <div class="article">
          <p>Ending the interview is a great time to summarize the job and express your interest. Before you say good-bye:</p>
          <ul>
            <li>Review your understanding of the job's duties.</li>
            <li>Review hours, location, dress code and any other important information.</li>
            <li>Ask when they expect to have the position filled.</li>
            <li>Thank the interviewer for their time.</li>
            <li>Remind them that you're interested and would like to be considered for the job.</li>
            <li>Shake hands firmly and confidently.</li>
          </ul>
          <p>Say goodbye with warm confidence, like you did when you greeted the interviewer. You might say something like: "It was wonderful to meet you and thank you for considering me for this position. I look forward to hearing from you."</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r23",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "The Successful Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r23_01_uobapk.jpg",
      thumbnailDescription:
        "Collage of four images of four different young people interviewing.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697765652/audio/c4-t3-r23_01_y6rehv.mp3",
        text: `
        <div class="article">
          <p>Now let's put together everything you've learned so far in three interviews in three different settings:</p>
          <ol>
            <li>Max – Data Entry Clerk in an Office Building</li>
            <li>Sadiq – Warehouse Worker in a Local Warehouse</li>
            <li>Dan Rose – Courtesy Clerk in a Supermarket</li>
          </ol>
          <p>As you view each video, think about which tips are most valuable for you and what you should practice before your next interview.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r24",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Max: Data Entry Clerk in an Office Building",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669430/images/c4-t3-r24_01_exc8fv.jpg",
      thumbnailDescription:
        "Young white man interviewing in an office setting.",
      type: "video",
      content: {
        media: "YDKw6hfSxZo",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Let's go along with Max on an example of a good interview in an office setting. Before Max even enters the building, he checks that his appearance is clean and professional.</p>
          <p><span class="transcript-speaker">Receptionist:</span> Hi. How can I help you?</p>
          <p><span class="transcript-speaker">Max:</span> Hi. I'm Max Anderson I'm here to meet with Alan Lewis for an interview.</p>
          <p><span class="transcript-speaker">Receptionist:</span> Perfect. Why don't you take a seat and I'll let him know you're here.</p>
          <p><span class="transcript-speaker">Max:</span> Thank you.</p>
          <p><span class="transcript-speaker">Receptionist:</span> Alan, your interview is ready for you. Sounds good. He'll be up in just a moment.</p>
          <p><span class="transcript-speaker">Max:</span> Thank you.</p>
          <p><span class="transcript-speaker">Narrator:</span> Notice how Max takes a few deep breaths to calm his nerves. He also makes sure to turn off his phone so that there are no disruptions or notifications during his interview. Max demonstrates calm, quiet and friendly body language and posture, even in the reception area, because he knows that the interview started the moment he walked in the door.</p>
          <p><span class="transcript-speaker">Alan:</span> Hi. You must be Max Anderson. I'm Alan Lewis thanks for coming in.</p>
          <p><span class="transcript-speaker">Max:</span> It's great to meet you. I'm looking forward to our interview.</p>
          <p><span class="transcript-speaker">Narrator:</span> Max does a nice job of keeping his eye contact at or near the interviewer. A friendly and professional greeting might include a firm handshake.</p>
          <p><span class="transcript-speaker">Alan:</span> Excellent. Well let's come on back and we'll talk in my office.</p>
          <p><span class="transcript-speaker">Max:</span> Of course.</p>
          <p><span class="transcript-speaker">Alan:</span> Alright. Max, so you're interested in the data entry position. Well before we get into that, why don't you just tell me a little bit about yourself.</p>
          <p><span class="transcript-speaker">Max:</span> Okay, I'm graduating from high school next month. I have pretty good grades. I'm really interested in a career in technology, especially in web design. I decided not to go to college because, well, I've taught myself so many tech skills in coding languages and I want to get some real job experience. Since this will be my first full-time job, I thought a data entry position would be a great place to start.</p>
          <p><span class="transcript-speaker">Narrator:</span> Max knew there would be a general question about himself and so he prepared a response that is positive and connected to his skills, job-related experience and career goals.</p>
          <p><span class="transcript-speaker">Alan:</span> Well, it's good to know about your interest in coding and web design. That's great. We often look to promote people from within the company to careers like that. So, tell me more about why you think you'd be a good fit for this entry-level data entry position.</p>
          <p><span class="transcript-speaker">Narrator:</span> Max wants to let the interviewer know that he does best with written communication but he prefers not to disclose that he is autistic during the interview. Let's observe how he weaves this accommodation into his response.</p>
          <p><span class="transcript-speaker">Max:</span> I'm really good at following directions, staying focused and noticing small details. I am very precise in what I do. I'm also a fast typer. I learned best when I'm given written instructions for a task. I actually prefer written communication for most things...it's just easier to memorize. For example, when I wanted to learn HTML5 for web development, I bought a how-to book and after I read it a few times, I was able to program a really professional looking website for my mom's new business called Julie's Nail Salon. Here you can take a look at that.</p>
          <p><span class="transcript-speaker">Alan:</span> Oh yeah. This is a very nice website. Yes, and you're so right. Data entry requires someone who can be focused and precise with what they're doing. Now tell me, what would you do if you weren't going to be able to meet a deadline or keep up with your workload?</p>
          <p><span class="transcript-speaker">Max:</span> Well, when I have a deadline at school, I've almost always made it because I think deadlines are important. But, if there was a bigger than usual project, I could email my supervisor and let them know what is happening and see if I could have a extension or some help to meet said deadline.</p>
          <p><span class="transcript-speaker">Alan:</span> Yes, it's always a good plan to communicate with your supervisor.</p>
          <p><span class="transcript-speaker">Narrator:</span> Let's see how the interview wraps up. Pay attention to how Max asks relevant questions and confirms the key information for the job before the interview ends.</p>
          <p><span class="transcript-speaker">Alan:</span> So, do you have any questions for me?</p>
          <p><span class="transcript-speaker">Max:</span> I'd like to make sure I'm clear about the job schedule and duties. Is it correct that if hired I would work Monday through Friday from eight to four and that I will be transferring information from the paper files into the computer database.</p>
          <p><span class="transcript-speaker">Alan:</span> Yes, that's right.</p>
          <p><span class="transcript-speaker">Max:</span> Alright. Great. Do I always need to be in the office here or can I sometimes work remotely from home?</p>
          <p><span class="transcript-speaker">Alan:</span> Good question. Because the data being entered is confidential information, we cannot have you take files out of the office for entry. Therefore, you would always be in the office. We have cubicles set up in a shared space. Is that a problem?</p>
          <p><span class="transcript-speaker">Max:</span> No, I just want to make sure I understood where I would go. Can you tell me when you expect to make your hiring decision?</p>
          <p><span class="transcript-speaker">Alan:</span> Well, we have two more candidates to interview but we hope to make a decision by early next week.</p>
          <p><span class="transcript-speaker">Alan:</span> Okay, sounds good. I'm very interested in this job. Thank you so much for your time today.</p>
          <p><span class="transcript-speaker">Narrator:</span> Notice in a moment how to help viewers non-verbal cue the interview is over and how he remains professionally friendly until he leaves the building.</p>
          <p><span class="transcript-speaker">Alan:</span> Well, thank you for coming in today. It was a pleasure meeting you.</p>
          <p><span class="transcript-speaker">Max:</span> It's a pleasure meeting you as well have a good day.</p>
          <p><span class="transcript-speaker">Alan:</span> Take care.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r25",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Sadiq: Warehouse Worker in a Local Warehouse",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669431/images/c4-t3-r25_01_yxisvm.jpg",
      thumbnailDescription:
        "Young black man interviewing in a warehouse setting.",
      thumbnailPosition: "40% 50%",
      type: "video",
      content: {
        media: "S6jzxZDBN_s",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Let's go along with Sadiq on an example of a good interview in a warehouse setting.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Alright, Sadiq. So, you're interested in the warehouse worker position. So, let's start by having you share what qualities make you the best candidate for the role.</p>
          <p><span class="transcript-speaker">Narrator:</span> Look at how Sadiq is dressed professionally and his body language is making a good impression.</p>
          <p><span class="transcript-speaker">Sadiq:</span> Okay. Well, I think I would be good at this job because I'km honest and hardworking.</p>
          <p><span class="transcript-speaker">Narrator:</span> Sadiq often speaks in more of a monotone voice. He wants to make a good first impression so he has been practicing matching his voice inflection to express his interest and hope for getting this job.</p>
          <p><span class="transcript-speaker">Sadiq:</span> I'm organized so I can easily keep up with product orders, packing and shipping. Once I know where items are located, I move at a fast pace. My last job was in a warehouse and as you can see from my reference letter, I was dependable and did good work.</p>
          <p><span class="transcript-speaker">Narrator:</span> Sadiq is doing great. He knew to come prepared to the interview with a copy of his resume, letter of reference and his prepared questions.</p>
          <p><span class="transcript-speaker">Sadiq:</span> Before the company closed down, I had just been trained to operate a forklift and power pallet truck. The skills I learned there will be valuable in this position as well.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Oh, good to know that you'd be a good candidate for operating machinery for moving our products. That's something we would likely have you learn to do so sounds like you have a head start.</p>
          <p>Alright, quick scenario. What would you do if you notice products were damaged?</p>
          <p><span class="transcript-speaker">Sadiq:</span> First, I would take photos of the damage and fill out the necessary forms or report it to my supervisor. I'm guessing the specific directions about what to do would be in the employee handbook?</p>
          <p><span class="transcript-speaker">Interviewer:</span> Yes, it would be and you're right that there is a form to fill out to give to your supervisor if you find any product damaged. Now there are many differences from company to company, so why do you want to work here?</p>
          <p><span class="transcript-speaker">Sadiq:</span> I would like to work here because I really like the idea of working at a company who is focused on the customer and your product is ultimately helping people! This company also has a good reputation of helping employees advance their careers and that possibility is really exciting to me!</p>
          <p><span class="transcript-speaker">Interviewer:</span> I appreciate your enthusiasm. That's great!</p>
          <p><span class="transcript-speaker">Narrator:</span> Sadiq is nailing this interview. He has friendly and professional body language, his answers highlighted his strengths, past job experience, showed knowledge of the company's mission and he also expressed interest in advancing his career. Let's see how Sadiq does as the interview comes to a close.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Before we wrap up, do you have any questions for me?</p>
          <p><span class="transcript-speaker">Sadiq:</span> I'd like to make sure I am clear about all of the job duties. So, I will be receiving orders checking and sorting products and I would also be picking, packing and preparing customer orders? Is that correct?</p>
          <p><span class="transcript-speaker">Interviewer:</span> Yes, that's right.</p>
          <p><span class="transcript-speaker">Sadiq:</span> Sounds good!</p>
          <p><span class="transcript-speaker">Interviewer:</span> Oh, I also need to confirm are you able to lift up to 50 lbs and fulfilling orders requires a lot of quick walking around the warehouse. Will you be able to meet the physical demands of this job?</p>
          <p><span class="transcript-speaker">Sadiq:</span> Yes, I can lift up to 50 lbs. I will need a step stool or ladder for anything that requires me to reach above my head. I'm strong and can move quickly, but just have less mobility in this shoulder for reaching above my head.</p>
          <p><span class="transcript-speaker">Narrator:</span> Did you notice how smoothly Sadiq asked for a ladder or step stool for anything that required lifting above his head? I bet Sadiq thought about any modifications or accommodations he would need to be successful in the position and practiced how we would ask for them. There are pros and cons for disclosing a disability in an interview so it's good to be prepared for how you want to handle this.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Okay, that's fine. We always have step stools and ladders available. Any other questions?</p>
          <p><span class="transcript-speaker">Sadiq:</span> Is it correct, that if hired, I would work Sunday through Thursday from 9:30 to 5:30.</p>
          <p><span class="transcript-speaker">Interviewer:</span> Yes, that's the schedule. Around the holidays we also sometimes have overtime.</p>
          <p><span class="transcript-speaker">Sadiq:</span> Okay, sounds good. When do you expect to make your hiring decision?</p>
          <p><span class="transcript-speaker">Interviewer:</span> Actually, you are the last interview today so we will be in touch before the end of the week. So, thanks so much for coming in.</p>
          <p><span class="transcript-speaker">Sadiq:</span> I appreciate the opportunity and look forward to hearing from you. Thanks. Have a good rest of your day.</p>
          <p><span class="transcript-speaker">Narrator:</span> Sadiq ended the interview like a pro. He confirmed the work schedule and found out when the company will make their hiring decision. His polite and friendly attitude will certainly be appreciated by the interviewer.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r26",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Dan Rose: Courtesy Clerk in a Grocery Store",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669431/images/c4-t3-r26_01_frfhim.jpg",
      thumbnailDescription:
        "Young white man interviewing in an office of a grocery store",
      type: "video",
      content: {
        media: "9eutPaZP5_E",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Dan Rose is interested in a position as a courtesy clerk in a grocery store. After submitting his resume, the employer sent Dan Rose a link to create and upload a two-minute or less video responding to the screening question "Why would you be a good person for this position?" Let's see how Dan Rose prepares for this video pre-interview.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> I would be good at this job because I'm reliable hard working, good at organizing things...oh yeah...and I'm strong.</p>
          <p><span class="transcript-speaker">Narrator:</span> Good thing Dan Rose is practicing. It is important to speak clearly and with confidence. as well as to look into the camera with a friendly expression. He might also say a little more about his skills and how it connects to the position. After a few more practices, let's see what Dan Rose does for the final video screening he submits.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> I'd be good at this job because I'm a reliable, hard-working, good at organizing things and I'm strong.</p>
          <p><span class="transcript-speaker">Narrator:</span> With a little preparation and practice, Dan Rose submitted a good video screening or pre-interview. The employer will call him within a week if they decide to schedule an in-person interview.</p>
          <p>Great news! Dan Rose got the interview. This employer has open interviews which means that on a specific day and timeframe, all candidates come into interview. As a person on the autism spectrum, Dan Rose decided it would help him feel calmer if he went to the grocery store a few days beforehand to scope it out. That way he would know a bit more of what to expect on the day of the open interviews. Let's go along with Dan Rose on an example of a good interview in a grocery store.</p>
          <p><span class="transcript-speaker">Alan:</span> Come on in please. Have a seat.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> Hi. I'm Dan Rose. I'm here to interview for the courtesy clerk job.</p>
          <p><span class="transcript-speaker">Alan:</span> Nice to meet you. Dan. I'm Alan. I'm one of the managers here. Nice to meet you.</p>
          <p><span class="transcript-speaker">Narrator:</span> After the interviewer greeted him, Dan Rose chose to shake hands. However, he could have declined respectfully or offered an alternative greeting. When first meeting the employer or interviewer, this is also a time to provide the correct pronunciation of your name and to specify preferred pronouns like he/him or they/them. For example, the greeting of this interview might have instead sounded like "It's nice to meet you as well, however I prefer an elbow bump greeting right now. Also. I go by my full name Dan Rose and use he/him pronouns. Thank you."</p>
          <p><span class="transcript-speaker">Alan:</span> Alright Dan Rose. So, you're interested in the position of courtesy clerk. So, tell me a little bit about why you want to work here.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> Well, I am graduating from high school next month and someday I would like to be a cashier here but since this would be my first full-time job, my father recommended I apply for the courtesy clerk first and then work my way up to cashier over time. My family has shopped for groceries here before and a cashier and bagger who helped us were very nice.</p>
          <p><span class="transcript-speaker">Alan:</span> Okay, great! How would you describe the perfect bagging technique?</p>
          <p><span class="transcript-speaker">Dan Rose:</span> Well. I like to plan and organize. So, I would first look at what the person is buying and then quickly plan what needs to go at the bottom of the bag, like boxes or cans. I saw in a movie once a bagger putting way too much stuff in a bag and then ripping and spilling it all over. So, I'd make sure not to overload it. Oh, and also, anything like eggs or bread that could easily be smushed I put them on top of other groceries.</p>
          <p><span class="transcript-speaker">Alan:</span> Sounds like a good technique. Now, if a customer was acting grumpy and then told you to push their carts and help them load their groceries into the car, how would you respond?</p>
          <p><span class="transcript-speaker">Dan Rose:</span> I think that in this job it's important to be friendly and helpful to customers. So, I get their groceries to the car for them. I'm still working on making eye contact with people but I always try to smile a little and am polite. I could tell the customer "I hope you have a nice day" after I load their groceries where they want them.</p>
          <p><span class="transcript-speaker">Alan:</span> In addition to bagging groceries, you will also need to do some light cleaning, return items to shelves and bring in carts from the parking lot. So, how will you stay calm when it's very busy and you have to handle multiple tasks at once?</p>
          <p><span class="transcript-speaker">Dan Rose:</span> I think technically you can only do one thing at a time. But I understand your question. When I feel stressed, I pause and take two slow breaths. If it were very busy and I had lots of things to do, I would take my two breaths and then decide what task needs to be completed first and then what I would do after that. One step at a time as my dad likes to say!</p>
          <p><span class="transcript-speaker">Narrator:</span> Dan Rose gave good responses with concrete examples to the interviewer. He honored that making direct eye contact was a challenge for him and shared this in a way that also highlighted his many strengths. As this interview wraps up, pay attention to how Dan Rose navigates the job offer and confirms the key information for the job before the interview ends.</p>
          <p><span class="transcript-speaker">Alan:</span> Well good news, Dan Rose. I think you would do really well as a bagger here and I'm prepared to offer you the job today.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> That is great! Thank you! Before I accept, I'd like to make sure I'm clear about the job schedule. Is it correct that if hired, I would work Tuesday through Saturday, from 11 to 8.</p>
          <p><span class="transcript-speaker">Alan:</span> Yes, that's right. For the most part, sometimes, we may ask you to move days or times or cover someone else's shift. But, myself or another manager will coordinate that with you.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> Alright. I do prefer a consistent shift time but I can probably change that from time to time. Would you please confirm the hourly pay and if there's any benefits?</p>
          <p><span class="transcript-speaker">Alan:</span> Certainly. The pay is $8.50 an hour and after six months, you're eligible for a 50-cent raise. We have health, dental and vision benefits available and you'll learn more about that at the new employee orientation day. Oh, and we also have a tuition reimbursement program if you want to continue your education after High School.</p>
          <p><span class="transcript-speaker">Dan Rose:</span> Oh, that is good to know! I am ready to accept the job. Thank you very much.</p>
          <p><span class="transcript-speaker">Alan:</span> That's great!</p>
          <p><span class="transcript-speaker">Narrator:</span> If you are offered a job at the interview, don't feel pressured to accept or decline it right away. You can ask for 24 hours or more to make your decision. Say something like "Thank you so much but I would like 48 hours to consider my decision" and don't be shy about asking relevant questions. For example, Dan Rose might have asked what the employee orientation or onboarding would be like to ensure he understood the support and structure of the job before making his decision.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r27",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Accommodations During the Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669432/images/c4-t3-r27_01_x7vxar.jpg",
      thumbnailDescription: "Young woman of color talking with an interviewer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658026/audio/c4-t3-r27_02_fvhkw7.mp3",
        text: `
        <div class="article">
          <p>The Americans with Disabilities Act (ADA) prohibits discrimination against individuals with disabilities in all public and private spaces that are also generally open to the public. For people with qualifying disabilities, the ADA protection begins at the very start of the job search process. This includes requesting or asking for reasonable accommodations before, during and after the interview process, as well as at any point once hired for the job.</p>
          <p>In module 2, we learned about some pros and cons for disclosing a disability, as well as some general tips if you do decide to disclose. In asking for an accommodation, you are not required to specifically use the terms “ADA”, “accommodation” or “disability.” Here are some examples of asking for an adjustment to the hiring process because of a disability:</p>
          <ul>
            <li>May I have extended time with the pre-interview assessment because of my reading challenges?</li>
            <li>It is painful for me to walk far. Can the interview take place near the first-floor entrance?</li>
            <li>I am confused by how to use your website to submit the required paperwork. Can someone assist me?</li>
            <li>Would you please slowly repeat each interview question twice? I am neurodiverse and need more processing time.</li>
          </ul>
          <p>When asking for an accommodation during an interview, it is helpful to decide in advance how much you want to share. However, sometimes an accommodation need becomes known during the interview process. For example, if an applicant shows up for an interview and discovers it is with a large group or panel, they may decide right then to disclose that they have Autism and request that they have a small group or 1-1 interview to avoid sensory overload.</p>
          <p>As a general recommendation when asking for an accommodation, only share what is relevant for your success during the interview or while on the job. Requesting an accommodation is often an interactive process and a conversation between the person with the disability and the potential employer. Remember to stay positive in how you frame the accommodation request, while also being very clear on what you need.</p>
          <p>This video highlights a few examples of asking for an accommodation during an interview:</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t3-r28",
      categoryId: "c4",
      topicId: "c4-t3",
      title: "Asking for an Accommodation During the Interview",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1697669432/images/c4-t3-r28_01_qppkql.jpg",
      thumbnailDescription: "Young woman of color pointing to her hearing aid",
      type: "video",
      content: {
        media: "lLDfkIiZTis",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Narrator:</span> Sometimes you may need to ask for an accommodation or modification before or during an interview. For example, if you use a wheelchair or mobility aid, you may want to communicate before the interview to confirm the location of an accessible entrance and interview space. Sometimes there are written tests as part of the application or interview process. If needed, ask in advance for an accommodation such as an alternative form, usage of assistive technology or extended time on the test. If you are deaf or hard of hearing, you may request a sign language interpreter or auxiliary aids. Asking for an accommodation before, during and after the interview process is your right. Here's an example of asking for an accommodation during the interview:</p>
          <p><span class="transcript-speaker">Kiana:</span> Would you mind moving the chairs a little? I have a hearing aid and lip read. It would be great if we sat across from each other.</p>
          <p><span class="transcript-speaker">Alan:</span> Absolutely...no problem. I'm glad you asked!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t4-r1",
      categoryId: "c4",
      topicId: "c4-t4",
      title: "Appropriate Time to Follow Up",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1654002256/images/c4-t4-r1_alvxq2.jpg",
      thumbnailDescription:
        "Person is filling out a calendar and looking at a smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219734/audio/c4-t4-r1_urcp3y.mp3",
        text: `
        <div class="article">
          <p>Be sure to thank the person you interviewed with and then stay in touch. You want to be sure they know you're interested in the job. You may not hear back from the employer right away and knowing when to follow up will help keep your job search on track. You can use a blank calendar page to plan how and when to follow-up. I think you'll find it helps.</p>
          <p>It's important to follow-up with an interviewer as soon as possible. This shows that you:</p>
          <ul>
            <li>Respect their time.</li>
            <li>Are interested in the job.</li>
            <li>Are polite and professional.</li>
          </ul>
          <p>Following up also gives you a chance to reinforce key things you want the employer to remember. Let's use a calendar to come up with a plan.</p>
          <ul>
            <li>Send a thank you email or letter as soon as you can but no later than the day after the interview. Many people send an email because it will arrive right away.</li>
            <li>If the interviewer told you when they planned to make their hiring decision, note that on your plan. Follow up with a phone call that day.</li>
            <li>If the employer didn't say when they planned to decide, follow up with a phone call one week after the interview.</li>
            <li>Plan to follow up by phone every week until you are offered the job or you find out the position is filled.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t4-r2",
      categoryId: "c4",
      topicId: "c4-t4",
      title: "Writing a Thank-You Note",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1654002256/images/c4-t4-r3_h5gpg3.jpg",
      thumbnailDescription:
        "Person is using a computer to write a thank-you note via email.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658024/audio/c4-t4-r2_01_ditjpw.mp3",
        text: `
        <div class="article">
          <p>Your thank-you message should be:</p>
          <ul>
            <li>Unique, personalized and specific to you, the interviewer and the job – no form letters!</li>
            <li>Different from the cover letter that you sent with your resumé, but still typed.</li>
            <li>Short and clear. No spelling errors!</li>
          </ul>
          <p>Your thank-you message should avoid:</p>
          <ul>
            <li>Giving excuses or explanations if you felt you could have done better in the interview.</li>
            <li>Being super pushy or overexplaining why you should get the job.</li>
            <li>Being very long, this is not an essay, it is a note!</li>
          </ul>
          <p>A good thank-you email contains ten key elements:</p>
          <div class="detail-pair">
            <p>Subject: Follow-up to interview with Ima Phonin</p>
            <p class="detail-indent">[1: Use the subject line to tell the purpose of the email.]</p>
          </div>
          <div class="detail-pair">
            <p>Dear Ms. Smith:</p>
            <p class="detail-indent">[2: Personal greeting - Be sure to send the message to the right person and that the name is spelled correctly.]</p>
          </div>
          <div class="detail-pair">
            <p>Thank you for taking time out of your busy schedule to interview me for the office assistant position in the Sales department.</p>
            <p class="detail-indent">[3: Thank the person for meeting with you - Include when you met and what position you interviewed for.]</p>
          </div>
          <div class="detail-pair">
            <p>I enjoyed meeting with you and learning more about the job and the company's sales goals for the coming year.</p>
            <p class="detail-indent">[4: Mention one or two specific things that you discussed - This will help the interview remember your meeting.]</p>
          </div>
          <div class="detail-pair">
            <p>I am confident that you will find that my computer and organizational skills will help the department achieve its goals.</p>
            <p class="detail-indent">[5: Remind the employer of one or two key skills you bring to the job.]</b></p>
          </div>
          <div class="detail-pair">
            <p>I am very interested in the position and believe that I will fit in well with the sales team.</p>
            <p class="detail-indent">[6: Tell the employer you're interested in the job.]</p>
          </div>
          <div class="detail-pair">
            <p>I hope to hear from you soon.</p>
            <p class="detail-indent">[7: Ask for an update.]</p>
          </div>
          <div class="detail-pair">
            <p>Please let me know if you need additional information or if you would like to meet again. I would be happy to come in for another interview.</p>
            <p class="detail-indent">[8: Offer to provide additional information.]</p>
          </div>
          <div class="detail-pair">
            <p>Again, thank you for meeting with me.</p>
            <p class="detail-indent">[9: Thank the person again.]</p>
          </div>
          <div class="detail-pair">
            <p>Sincerely,</p>
            <p>Ima Phonin</p>
            <p>ima.phonin@internetprovider.com<br/>
            333-444-5555<br/>
            1234 Any Street<br/>
            Anytown, State 55666 <br/>
            </p>
            <p class="detail-indent"><b>[10: Sign off and include all of your contact information.] </b></p>
          </div>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c4-t4-r3",
      categoryId: "c4",
      topicId: "c4-t4",
      title: "Following Up by Phone",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1654002256/images/c4-t4-r2_gtfkou.jpg",
      thumbnailDescription:
        "Young man with Down Syndrome is using his smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1657219733/audio/c4-t4-r3_irjvsu.mp3",
        text: `
        <div class="article">
          <p>After you send the thank you email or letter, you can use the schedule you created to follow-up with the employer by phone.</p>
          <p>There are a few things to remember when calling someone you've interviewed with.</p>
          <h2>Things to Remember</h2>
          <p>If you haven't heard from the potential employer after a week, it's time to check in by phone.</p>
          <p>A good follow-up telephone contact is:</p>
          <ul>
            <li>Professional</li>
            <li>Brief</li>
            <li>Complete</li>
          </ul>
          <h3>Tip #1:</h3>
          <p>Be sure to call from a quiet place.</p>
          <h3>Tip #2:</h3>
          <p>Be sure that the message you leave is professional and easy to understand.</p>
          <h3>Tip #3:</h3>
          <p>Before you call, take a minute to think about what you want to say then write it down.</p>
          <h3>Tip #4:</h3>
          <p>Your call or message should:</p>
          <ul>
            <li>Be spoken slowly and clearly.</li>
            <li>Contain a pleasant greeting.</li>
            <li>Include your first and last name and phone number.</li>
            <li>Explain why you're calling.</li>
            <li>Refer to the position that you interviewed for and when you were interviewed.</li>
            <li>Repeat your interest in the job.</li>
            <li>Request a return call.</li>
          </ul>
          <p>The way you say something is as important as the words you use. Practice what you want to say a few times before you call. Be polite and professional! Take a few deep breaths and think positive!</p>
        </div>`,
      },
      keywords: [],
    },
    // Category 5 resources
    {
      id: "c5-t1-r1",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "The Waiting Game",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t1-r1_amtu4e.jpg",
      thumbnailPosition: "15% 50%",
      thumbnailDescription: "Black man is being interviewed.",
      type: "video",
      content: {
        media: "RPXIKoF-JOE",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Bennie:</span> My momma always told me not to ever to give up. I started at the bottom. And look at me. I have a good job. Because I had faith and I can do it and I'm not going to put myself down and not think I can't do it because this is something in my heart that I really want to do.</p>
          <p><span class="transcript-speaker">Laura:</span> You just have to be yourself. Don't be anybody else and just don't give up. Keep on going for it. Do whatever, call up the managers to get their attention in a respectful way. Just present yourself.</p>
          <p><span class="transcript-speaker">James:</span> I kept myself encouraged by volunteering at a feline rescue and found some way I could fit into society by volunteering there. Somewhere I think it says in the Bible if you seek, you will find and if they continue to look, they will in fact find a job.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t1-r2",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "What to Do While You Wait",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t1-r2_j1pwiq.jpg",
      thumbnailDescription:
        "Group of young people are volunteering at a food shelf.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598202/audio/c5-t1-r2_w8jplq.mp3",
        text: `
        <div class="article">
          <p>Here are some things you can do to stay positive.</p>

          <h2>Follow-up on pending applications or interviews</h2>
          <p>Some employers don't notify applicants when a job is filled. Check in with the hiring manager if you don't hear back from an employer. Politely ask if the position is still open and if you're being considered. Use the techniques you learned in Course 4.</p>

          <h2>Apply for other jobs and go on interviews</h2>
          <p>Many job seekers fall into the trap of thinking that every application will result in a job offer. You can waste a lot of time waiting for an answer. Keep looking!</p>

          <h2>Keep networking</h2>
          <p>Stay in touch with other jobseekers and the people who are helping you find job openings.</p>

          <h2>Volunteer</h2>
          <p>Find ways to volunteer in the field or career you're interested in. You'll stay busy and add new things to your resumé. Volunteering is great for networking and can result in a job hire!</p>

          <h2>Review and update your resumé</h2>
          <p>It may be time for a resumé tune-up, especially if you aren't being asked to interview for jobs or aren't getting any offers.</p>

          <h2>Sharpen your skills</h2>
          <p>Are employers choosing other candidates with more experience or skills? If so, find ways to improve your skills. For example, you can go to the library to learn more or sign up for free classes at the Workforce Center.</p>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c5-t1-r3",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "Receiving a Job Offer",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1675386662/images/c5-t1-r3-replacement_rdabqk.jpg",
      thumbnailDescription:
        "Young Muslim woman wearing a hijab receives a job offer on her phone and is very happy.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598202/audio/c5-t1-r3_aldpqv.mp3",
        text: `
        <div class="article">
          <p>Always respond professionally if an employer contacts you. You want to leave a good impression, even when you get news that you don't want to hear. After all, the employer may not think you're right for this job but another position may open later on. A hiring manager may contact you with a job offer in several ways.</p>

          <h2>Telephone</h2>
          <ul>
            <li>You don't know when an employer will call, so answer every call in a businesslike manner.</li>
            <li>Make sure your voice mail message is brief, professional and clear. Be sure to include your name, ask the caller to leave a message, and promise to return the call as soon as possible.</li>
            <li>No nicknames, ring-back tones or “fun” messages.</li>
          </ul>

          <h2>Email</h2>
          <ul>
            <li>Check your email account at least once a day.</li>
            <li>Respond promptly if an employer contacts you.</li>
          </ul>

          <h2>In-person</h2>
          <ul>
            <li>Sometimes an interview goes well and you're offered the job on the spot!</li>
            <li>You may have time to think about the offer, but not always. Be sure to let them know you will get back to them within 24 hours.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t1-r4",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "What is a Job Offer?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t1-r4_ube319.jpg",
      thumbnailDescription:
        "Young man is opening mail and receives a job and is very happy.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658027/audio/c5-t1-r4_01_buhfee.mp3",
        text: `
        <div class="article">
          <p>A job offer is just that – an invitation from an employer to join his or her organization as an employee. A job offer usually includes:</p>
          <ul>
            <li>The title of the job the employer is hiring you to do.</li>
            <li>How much you'll be paid and information on any benefits.</li>
            <li>When you will start and where you will work.</li>
            <li>The days, hours and shift the employer wants you to work.</li>
            <li>Uniform or dresscode requirements.</li>
            <li>Whether you must take and pass a physical, drug screening or background check before you start.</li>
          </ul>
          <p>The employer also may tell you about the new employee orientation process or explain how you will be trained.</p>
          <p>Remember, a job offer can be taken away if your employer finds out that you weren't honest on your application or during your interview. You also can lose the job if you fail a required physical exam, drug screening or background check.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t1-r5",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "How to Respond When You're Offered a Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t1-r5_k84hlz.jpg",
      thumbnailDescription:
        "Young man with Down Syndrome is smiling while talking on his smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657986/audio/c5-t1-r5_01_zmwuol.mp3",
        text: `
        <div class="article">
          <p>If you're offered a job virtually, by phone or in-person, you may not have to decide immediately. You can ask for 24 hours to think about an offer. When an employer contacts you with a job offer:</p>
          <ul>
            <li>Be enthusiastic and professional.</li>
            <li>Ask for a copy of the job description, pay, hours and benefits if you don't already have one.</li>
            <li>Make sure you understand the job's requirements.</li>
            <li>Thank the employer.</li>
            <li>Get back to the employer as soon as possible!</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t1-r6",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "What if You Don't Get the Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t1-r6_nyirev.jpg",
      thumbnailDescription:
        "Young woman is frowning while talking on her smart phone and is taking notes.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598200/audio/c5-t1-r6_rteykj.mp3",
        text: `
        <div class="article">
          <p>Don't take it personally if an employer decides to hire someone else. This doesn't mean you're not a good person, it just means the employer decided that another applicant was better suited for the job.</p>
          <p>Stay calm. Don't say anything you might regret. After all, you may want to apply for another job with this employer. They'll remember if you were polite and professional – and if you weren't!</p>
          <p>If possible:</p>
          <ul>
            <li class="text-weight-bold">Ask why you weren't chosen.</li>
            <ul>
              <li>It doesn't hurt to ask! Use what you learn to improve your resumé and interview skills.</li>
            </ul>

            <li class="text-weight-bold">Ask to be notified if another job opens at the company.</li>
            <ul>
              <li>Some employers keep a list of applicants who want to be considered for future openings.</li>
            </ul>

            <li class="text-weight-bold">Let the employer know that you're still interested in the job.</li>
            <ul>
              <li>Tell the employer you'd like to be considered for the job if the other candidate doesn't work out. After all, the other person might change his or her mind or might have found another position.</li>
            </ul>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c5-t1-r7",
      categoryId: "c5",
      topicId: "c5-t1",
      title: "Learning from Your Experience",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t1-r7_ja6zwo.jpg",
      thumbnailDescription: "Young black man is thoughtfully thinking.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598200/audio/c5-t1-r7_lhwjsl.mp3",
        text: `
        <div class="article">
          <p>Every rejection offers a chance to improve your job seeking skills. Set aside your emotions and think objectively about the application or interview process to find ways you can improve.</p>

          <h2>The Application Process</h2>
          <ul>
            <li>Was your application neat and easy to read?</li>
            <li>Was it complete? Did you answer all of the questions?</li>
            <li>Were your responses honest and accurate?</li>
            <li>Do you think you left a good impression?</li>
            <li>Is there anything that you could have done better?</li>
            <li>What will you do differently?</li>
          </ul>

          <h2>The Interview Process</h2>
          <ul>
            <li>Did you think the interview was successful?</li>
            <li>Did you dress appropriately?</li>
            <li>Did you arrive on time?</li>
            <li>How well do you think you answered the employer's questions?</li>
            <li>How well do you think you explained why you would be a good fit for the job?</li>
            <li>Did you leave a good impression?</li>
            <li>What will you do differently?</li>
          </ul>

          <h2>Your Resumé</h2>
          <ul>
            <li>Does your resumé focus on the right things?</li>
            <li>Did you customize your resumé?</li>
            <li>Are there things you should add or take out?</li>
            <li>Did you have the experience or skills that the job required?</li>
          </ul>
        </div>`,
      },
      keywords: ["resume"],
    },
    {
      id: "c5-t2-r1",
      categoryId: "c5",
      topicId: "c5-t2",
      title: "Understanding a Job Offer",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t2-r1_ai836w.jpg",
      thumbnailDescription: "Job Description is lying on a desk.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598201/audio/c5-t2-r1_oy4a0y.mp3",
        text: `
        <div class="article">
          <p>Take a few minutes to gather information about the job. Ask the employer for a copy of the job description if you don't already have one, then use the four “Ws” to gather what you need to know. Take notes so you can refer to them later.</p>

          <h2>WHAT:</h2>
          <ul>
            <li>What is the job? Be sure you know the position you'll be filling, especially if you've applied for more than one position at the same employer. If you aren't clear about the job's responsibilities, ask for more information.</li>
            <li>What will you be paid? Will you get benefits?</li>
            <li>What should you wear? Be sure that you clearly understand the company's dress code or uniform requirements.</li>
            <li>What do you need to pay for upfront? Some jobs require you to join a union or buy a uniform. Try to find out how much it will cost and if the expense will be deducted from your first paycheck.</li>
            <li>What tests will you need to take or pass? Some employers require candidates to pass an employment physical, drug screening or skills test before a job can be offered officially. Be sure you understand what will happen if you don't pass. Remember, everyone hired for this position has to pass the same test. You can't be singled out.</li>
          </ul>

          <h2>WHEN:</h2>
          <ul>
            <li>When will you work? Know the days, hours and shift that the employer wants you to work. Is the job full-time, part-time or “as needed?” If the job is part-time or “as needed,” find out how many hours you can expect to work each week or pay period.</li>
            <li>When does the employer want you to start? Write down the date, day and time.</li>
          </ul>

          <h2>WHERE:</h2>
          <ul>
            <li>Where should you go on your first day? This may not be the same place where you were interviewed or submitted your application. Also, some employers may want you to attend a special orientation session at a different location.</li>
            <li>Where will you work? Know the location where you will work.</li>
          </ul>

          <h2>WHO:</h2>
          <ul>
            <li>Who will you meet or ask for on your first day? This may be your supervisor or someone from human resources.</li>
            <li>Who will you work for? Find out the name of your direct supervisor.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t2-r2",
      categoryId: "c5",
      topicId: "c5-t2",
      title: "Evaluating the Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t2-r2_zn0u3a.jpg",
      thumbnailPosition: "16% 50%",
      thumbnailDescription:
        "Evaluation form has five options: Outstanding, Very Good, Satisfactory, Marginal and Unsatisfactory. Outstanding is checked off .",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657992/audio/c5-t2-r2_02_foy6hq.mp3",
        text: `
        <div class="article">
          <p>Now that you understand the job offer, it's time to decide if the job is right for you. Remember, every job and every job seeker is different. That's why it's important to evaluate the job offer according to your needs. Of course, no job is perfect so you may have to decide if the job is “close enough.”</p>

          <h2>The Job</h2>
          <p>You should be able to perform the essential functions of the job if you have the training and/or supports you need. Think about each element of the job description, then decide if you:</p>
          <ol>
            <li>Can do the job.</li>
            <li>Can learn to do the job with proper training.</li>
            <li>Can do the job with support or an accommodation.</li>
            <li>Want to do the job.</li>
          </ol>

          <h2>Salary and Benefits</h2>
          <p>Will the salary work for you? Be sure to take into account the number of hours you'll work and what you'll be paid. If you're eligible for benefits, how much will they cost? Does the salary work for you after these expenses are deducted?</p>

          <h2>The Employer</h2>
          <p>Think about the employer. Is this employer a good fit for you? Do you think you will have opportunities for growth?</p>

          <h2>Your Wants and Needs</h2>
          <p>In Course 1, you identified your job requirements – the things the job must have if it is to work for you. Review the “Needs and Wants” worksheet that you completed in Course 1 to help you decide if this job offer meets your requirements.</p>

          <h2>Growth Potential</h2>
          <p>Even if a job isn't exactly what you're looking for, it may offer new experiences or opportunities to build your skills and expand your career.</p>

          <h2>Supplemental Security Income (SSI) Impacts</h2>
          <p>If you receive disability benefits, they may be affected by paid work. Your placement specialist can help you find the right resources to help you learn more about your financial situation.</p>

          <h2>Advice</h2>
          <p>You don't have to decide whether or not to accept a job on your own. You can:</p>
          <ul>
            <li>Come up with a list of “pros” and “cons” and discuss them with your friends and family members.</li>
            <li>Discuss the job offer with a placement specialist.</li>
            <li>Talk with a financial resource.</li>
            <li>Figure out what's holding you back (talk with friends, family or your placement specialist for support).</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t2-r3",
      categoryId: "c5",
      topicId: "c5-t2",
      title: "Is There Room for Negotiation?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t2-r3_wmvsyu.jpg",
      thumbnailPosition: "75% 50%",
      thumbnailDescription:
        "Stop watch has the words Time to Negotiate overlaid on the face of the watch.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598200/audio/c5-t2-r3_beg6r3.mp3",
        text: `
        <div class="article">
          <p>Some employers may be willing to negotiate some aspects of the job if they really want you to join their organizations. Negotiation usually relates to:</p>
          <ul>
            <li>Salary</li>
            <li>Hours</li>
            <li>Benefits</li>
            <li>Specific tasks</li>
          </ul>
          <p>Asking an employer to negotiate an offer carries some risks, including losing the job. Your placement specialist can help you weigh the risks and decide how to respond if the employer isn't willing to negotiate.</p>

          <h2>Negotiating Tips</h2>
          <p>If you decide to negotiate with a potential employer:</p>
          <ul>
            <li>Be polite, respectful and flexible.</li>
            <li>Explain your concerns simply and clearly.</li>
            <li>Offer a specific, reasonable solution.</li>
            <li>Practice what you'll say with your placement specialist, a friend or family member.</li>
            <li>Listen carefully if the employer offers a different solution. Be reasonable and willing to compromise.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t2-r4",
      categoryId: "c5",
      topicId: "c5-t2",
      title: "Accepting a Job Offer",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t2-r4_vxxkfg.jpg",
      thumbnailDescription:
        "Smart phone is displaying an incoming call from Job Offer: Dream Job. Accept and Decline buttons appear on the bottom of the screen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598200/audio/c5-t2-r4_diysr2.mp3",
        text: `
        <div class="article">
          <p>If you decide that the job is a great fit, there's only one thing left to do – ACCEPT!</p>
          <p>When accepting a job:</p>
          <ul>
            <li>Decide what you want to say before you call.</li>
            <li>Call the employer within 24 hours.</li>
            <li>Repeat what you understand about the job to make sure you and the employer are on the same page.</li>
            <li>Verify the day and time you'll start working, where you should go and what you should wear.</li>
            <li>Be enthusiastic and thank the employer for the opportunity.</li>
          </ul>
          <p>There's no standard way to accept a job because every job and every employer is different! Read the script below to see how Johnny Jobseeker accepted a job.</p>

          <h2>Phone Call Script</h2>
          <p class="text-italic">This is Johnny Jobseeker. I'm calling to accept the maintenance position that you offered to me yesterday. I can't wait to get started! When do I start and where should I go?</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t2-r5",
      categoryId: "c5",
      topicId: "c5-t2",
      title: "Declining a Job Offer",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t2-r5_r9bilv.jpg",
      thumbnailDescription:
        "Serious young woman is talking on her smart phone.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598200/audio/c5-t2-r5_db5g2l.mp3",
        text: `
        <div class="article">
          <p>If you decide that a job is not right for you, let the employer know as soon as possible. When turning down a job offer:</p>
          <ul>
            <li>Practice what you plan to say.</li>
            <li>Be polite and respectful.</li>
            <li>Explain that you've decided not to accept the offer and why. Keep it simple.</li>
            <li>Thank the employer for wanting to hire you.</li>
          </ul>
          <p>There's no standard way to turn down a job because every job and every employer is different! Read the script below to see how Johnny Jobseeker turned down a job that didn't meet his needs.</p>

          <h2>Phone Call Script</h2>
          <p class="text-italic">This is Johnny Jobseeker. I'm sorry but I won't able to accept the maintenance job because I need to work more hours than you offered. If it's possible to increase the number of hours, I'd really like to work for you. Thanks for the offer. If you have a full-time position opening in the future, please let me know.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r1",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Tips for Filling Out Paperwork",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t3-r1_fv9evd.jpg",
      thumbnailDescription:
        "Young black man is at his desk filling out paperwork.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598200/audio/c5-t3-r1_fzssl7.mp3",
        text: `
        <div class="article">
          <p>Your new employer will need to gather information about you. Employers usually do this by requiring new employees to complete various forms on their first day or week on the job.</p>
          <p>If your new employer asks you to fill out employment forms on-site before you start work, be sure to shower and dress neatly, turn off your cell phone and remove ear buds, Bluetooth devices, etc. before you enter the building.</p>
          <p>When completing employment paperwork:</p>
          <ul>
            <li>Gather important information and documents beforehand.</li>
            <li>Print neatly if you're filling in paper forms.</li>
            <li>Make sure the information is accurate and complete before you submit it.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r2",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Most Common Employment Paperwork",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842305/images/c5-t3-r2_zx71tl.jpg",
      thumbnailDescription: "W4 form and pen is lying on a desk.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657989/audio/c5-t3-r2_01_lfxbvz.mp3",
        text: `
        <div class="article">
          <p>Let's look at some of the common types of paperwork that you may be asked to complete.</p>

          <h2>Required Federal Forms</h2>
          <p>The federal government requires all employees to complete:</p>
          <ul>
            <li>Form W-4: Employee's Tax Withholding Allowance Certificate</li>
            <li>Form I-9/Section 1: Employment Eligibility Verification</li>
          </ul>

          <h2>Other Common Employment Forms</h2>
          <p>Employers also may require you to fill out:</p>
          <ul>
            <li>Job offer acceptance letter or employment contract</li>
            <li>Employee Information form</li>
            <li>Emergency Contact form</li>
            <li>Benefit enrollment forms</li>
            <li>A job application, if you haven't done so already</li>
            <li>Vehicle registration if you park on-site</li>
            <li>Direct deposit banking authorization</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r3",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Documents You'll Need",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t3-r3_hbjkbl.jpg",
      thumbnailDescription: "Social Security card for John Smith.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657990/audio/c5-t3-r3_01_oewa0q.mp3",
        text: `
        <div class="article">
          <p>Your employer is required to verify that you've provided documentation that proves your identity and that you can work in the United States legally. Every employee needs to provide proper documentation, although the documents required will depend on your situation.</p>

          <h2>ORIGINAL Copy of Your Social Security Card</h2>
          <p>You must show your employer an original copy of your Social Security card – a photocopy won't work. If you don't have an original copy, order one now. Or know that your employer can use Social Security Number Verification Service (SSNVS) which allows registered employers to quickly verify whether a person's name and SSN match Social Security's records.</p>

          <h2>Certified Birth Certificate or Passport</h2>
          <p>If you don't have an original Social Security card, your employer can accept an original, certified birth certificate or passport. Again, photocopies are not acceptable. If you don't have a certified copy, order one now.</p>

          <h2>Picture Identification</h2>
          <ul>
            <li>You will need to provide one form of official identification that contains your picture.</li>
            <li>State-issued driver's license.</li>
            <li>State-issued identification card.</li>
            <li>School identification card that contains your photograph.</li>
          </ul>

          <h2>Work Authorization Documents</h2>
          <p>If you're eligible to work in the United States but are not a U.S. citizen, you will need to provide special documentation, such as a Permanent Resident Card (green card), Employment Authorization Document (work permit), Deferred Action for Childhood Arrivals (DACA) or other Visa issued by the United States Citizenship and Immigration Services.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r4",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Information You'll Need to Provide",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t3-r4_odrmfc.jpg",
      thumbnailPosition: "0% 50%",
      thumbnailDescription: "Emergency Contact Information form and a pen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598201/audio/c5-t3-r4_uw5wb4.mp3",
        text: `
        <div class="article">
          <p>You'll find it's a lot easier to fill out job-related paperwork if you have the information you need on hand.</p>
          <p>Here are five things you should bring along:</p>

          <h2>Your Completed Master Application</h2>
          <p>The Master Application that you completed in Course 2 contains a lot of information about you, your background and your past employment.</p>

          <h2>Emergency Contact Information</h2>
          <p>Many employers will ask you to provide the name and telephone number of one or two people who can be contacted in an emergency. Bring their contact information with you. You also should be prepared to provide your health insurance information, the name and phone number of your doctor or clinic and the hospital you prefer.</p>

          <h2>Benefit Decisions</h2>
          <p>You will need to enroll if you decide to access employment benefits through your employer. Think about your choices in advance and gather the information you'll need to assign beneficiaries, select a health care plan, etc.</p>

          <h2>Bank Account Information or Blank Deposit Slip</h2>
          <p>If you decide to have your paycheck automatically deposited to your account, you will need to provide your banking information or a blank deposit slip.</p>

          <h2>Transcripts and Required Licenses</h2>
          <p>Some employers may ask for official copies of your diploma or transcript, special licenses or training certificates, etc.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r5",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Form W-4",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t3-r5_nneksb.jpg",
      thumbnailDescription: "Snapshot of a Form W-4.",
      thumbnailPosition: "0% 50%",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657991/audio/c5-t3-r5_02_aljnur.mp3",
        text: `
        <div class="article">
          <p>Your employer is legally required to “withhold,” or set aside, money from your paycheck to pay your federal income taxes. Your employer gives these funds to the IRS, which uses them to pay any federal income taxes that you owe on your earnings at the end of the year. If any funds are left over, they will be returned to you.</p>

          <h2>IRS Form W-4</h2>
          <p>IRS Form W-4 is the federal tax withholding form that you complete for your employer to determine how much should be withheld from your paycheck for federal income taxes. You can change and update your W-4 form at any time if your personal or financial circumstances change. In addition, the federal W-4 form, many states also have a state-specifc W-4 form, determining the state income tax withholding.</p>
          <p>Let's look at the federal W-4 form more closely.</p>

          <h3>Step 1: Enter Personal Information</h3>
          <ol type="a">
            <li>Your full name and address.</li>
            <li>Your Social security number - Check to be sure that your last name is the same as the one that appears on your Social Security card. Follow the instructions if your name is different.</li>
            <li>Select the appropriate filing status.</li>
          </ol>

          <h3>Steps 2-4: Only complete if they apply to you; otherwise skip to step 5</h3>

          <h3>Step 2: Multiple Jobs or Spouse Works</h3>
          <p>Complete if you hold more than one job at a time or are married filing jointly and your spouse also works. The correct amount of withholding depends on income earned from all of these jobs. Use the multiple jobs worksheet found after the main form (step 2b).</p>

          <h3>Step 3: Claim Dependent and Other Credits</h3>
          <p>Complete as prompted on the form if you can claim qualifying children or other dependents, which may result in other credits.</p>

          <h3>Step 4: Other Adjustments (optional)</h3>
          <p>Indicate other income (not from jobs), deductions and extra withholding as outlined on the form. Use Step 4b Deductions Worksheet found after the main form.</p>

          <h3>Step 5: Sign Here</h3>
          <p>Add your signature and date to validate that the form is true, correct and complete.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r6",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Form I-9",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t3-r6_agngak.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription: "Snapshot of a Form I-9.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658030/audio/c5-t3-r6_02_pw6ymr.mp3",
        text: `
        <div class="article">
          <p>All employers in the United States are required to keep a completed I-9 form on file for every employee they hire. The I-9 form verifies:</p>
          <ol>
            <li>Your identity.</li>
            <li>That you can work in the United States legally.</li>
          </ol>
          <p><b>YOU</b> are required to complete Section 1 of Form I-9 within three days of starting your new job. You also are required to show acceptable documentation to your employer.</p>
          <p><b>YOUR EMPLOYER</b> is responsible for reviewing the identity and employment authorization documents that you provide and for completing Sections 2 and 3.</p>

          <br/>
          <hr>

          <h2>Form I-9</h2>
          <h3>SECTION 1: EMPLOYEE INFORMATION AND VERIFICATION</h3>
          <h4>Personal Information</h4>
          <ol>
            <li>Enter your full name, including your middle initial:</li>
            <ul>
              <li><b>Goodworker, Ima J.</b></li>
            </ul>

            <li>Enter your full address.</li>
            <ul>
              <li><b>1234 Main St. Apt #4, Wannajob, MN  55111</b></li>
            </ul>

            <li>Enter any other last names you have legally used:</li>
            <ul>
              <li><b>Ima J. Hireme</b></li>
            </ul>

            <li>Enter your date of birth in this format: month/day/year:</li>
            <ul>
              <li><b>5/29/2007</b></li>
            </ul>

            <li>Enter your Social Security Number:</li>
            <ul>
              <li><b>111-22-3333</b></li>
            </ul>

            <li>Enter your email address:</li>
            <ul>
              <li><b>imagoodworker@email.com</b></li>
            </ul>

            <li>Enter your telephone number with area code:</li>
            <ul>
              <li><b>555-555-5555</b></li>
            </ul>
          </ol>

          <h4>Verification of Identity</h4>
          <p>Read each statement and choose the one that applies to you, then sign and date the form.</p>

          <h4>Preparer and/or Translator Certification</h4>
          <p>Indicate whether you used a preparer or translator. If someone helped you complete Section 1, that person needs to provide their contact information, sign and date this section.</p>

          <h3>SECTION 2: EMPLOYER REVIEW AND VERIFICATION</h3>
          <p><strong>Your employer will complete this section.</strong> They will examine the documents you provided to verify your identity and eligibility to work in the United States.</p>
          <p>Your employer will ask you to provide one document from List A, found on the reverse side of the I-9 form. If you can't provide one of these documents, then you must provide two other documents: one from List B <strong>and</strong> one document from List C.</p>
          <h3>SECTION 3: UPDATING AND REVERIFICATION</h3>
          <p>Your employer is also responsible for completing this section if necessary. As a new employee, this won't apply to you unless you worked for the employer before and were rehired within three years after leaving the job.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t3-r7",
      categoryId: "c5",
      topicId: "c5-t3",
      title: "Other Employment Forms",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t3-r7_trpjzo.jpg",
      thumbnailDescription: "Employment Benefits Package is lying on a desk.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657992/audio/c5-t3-r7_01_psyfbd.mp3",
        text: `
        <div class="article">
          <p>Employers also may ask you to complete other forms.</p>

          <h2>1. A Job Application</h2>
          <p>If you haven't done so already, you may be asked to complete a job application to be placed in your employee file. You learned about job applications in Course 2.</p>

          <h2>2. Employee Information Form</h2>
          <p>You may be asked to provide:</p>
          <ul>
            <li>Your name, address, email and phone number.</li>
            <li>Your date of birth.</li>
            <li>Your Social Security number.</li>
            <li>Your marital status and spouse's contact information (if applicable).</li>
            <li>Health-related information such as conditions, allergies and special requirements (usually optional).</li>
          </ul>

          <h2>3. Emergency Contact Form</h2>
          <p>You may be asked to provide:</p>
          <ul>
            <li>The names and contact information for one or two people who can be contacted in an emergency.</li>
            <li>Your doctor or clinic's name and telephone number.</li>
            <li>Your preferred hospital.</li>
            <li>Additional health information such as allergies and medical alerts (usually optional).</li>
          </ul>

          <h2>4. Benefit Enrollment Forms</h2>
          <p>Some employers offer special employment benefits like:</p>
          <ul>
            <li>Health care coverage.</li>
            <li>Life insurance benefits.</li>
            <li>Participation in a retirement savings plan.</li>
            <li>Pre-tax spending accounts.</li>
            <li>Automatic deductions for transit fares.</li>
          </ul>
          <p>These are important decisions so think about them carefully. Be sure that you understand:</p>
          <ul>
            <li>Exactly what the benefit will cost and how your take-home pay will be affected. Employers offer the benefits but participants usually pay some of the cost.</li>
            <li>How you will access the benefits you've selected.</li>
            <li>When the benefits go into effect.</li>
          </ul>

          <h2>5. Direct Deposit Authorization</h2>
          <p>Some employers will deposit your paycheck to a banking account automatically.
          If you choose this option, you will be required to complete a Direct Deposit Authorization form and provide your banking information and account number or attach a blank deposit slip.
          </p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r1",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Understanding the ADA",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t4-r1_co3cyo.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription:
        "Flag is in the background with the words ADA: American with Disabilities Act overlaid.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657992/audio/c5-t4-r1_01_dck12t.mp3",
        text: `
        <div class="article">
          <p>The Americans with Disabilities Act, or the ADA, is a federal law that protects people with disabilities from discrimination in employment. It also makes sure that public facilities, transportation, communications and state and local government programs and services are accessible to everyone. Additionally, it establishes requirements for telecommunication relay services and closed captioning.</p>
          <p>The ADA protects any person who has a disability from being discriminated against in the job application process, hiring, firing, advancement, pay and job training. If you have a disability, you must also be qualified to perform the essential functions or duties of a job, with or without reasonable accommodation, in order to be protected from job discrimination by the ADA , An individual with a disability is a person who:</p>
          <ul>
            <li>Has a physical or mental impairment that substantially limits one or more major life activities;</li>
            <li>Has a record of such an impairment; or</li>
            <li>Is regarded as having such an impairment.</li>
          </ul>
          <p>The ADA has five parts, called Titles. Title 1 of the ADA focuses on employment. It:</p>
          <ol>
            <li>Applies to all employers with 15 or more employees.</li>
            <li>Requires employers to provide equal employment opportunities to all workers, including individuals with disabilities.</li>
            <li>Prohibits employers from asking questions about an applicant's disabilities during the hiring process with the exception of the voluntary Self-Identification of Disability form and general questions about all applicants' ability to perform essential job functions.</li>
            <li>Requires employers to provide “reasonable accommodations” to employees who disclose a disability.</li>
            <li>Goes into effect as soon as an employee discloses a disability to an employer or potential employer.</li>
          </ol>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r2",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Your Rights Under the ADA",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t4-r2_b4hsua.jpg",
      thumbnailDescription:
        "Law book with a Judge's gavel lying on top of the book.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598201/audio/c5-t4-r2_wohhq8.mp3",
        text: `
        <div class="article">
          <p>Under the ADA, employees with disabilities have the same rights as employees without disabilities. Employers also have rights under the ADA.</p>

          <h2>Employee Rights</h2>
          <p>Individuals with disabilities have the right to:</p>
          <ul>
            <li>Equal access and consideration in hiring, job assignments, pay, layoffs and firing, training, promotions, benefits and all other employment-related activities.</li>
            <li>Ask for and receive “reasonable accommodations” so they can perform their jobs more successfully.</li>
            <li>Request additional or new accommodations at any time.</li>
            <li>Expect employers to respect their privacy.</li>
          </ul>

          <h2>Employer Rights</h2>
          <p>Employers also have protections under the ADA. Employers have the right to:</p>
          <ul>
            <li>Hire employees who are qualified to do the job.</li>
            <li>Decide if a requested accommodation poses an “undue hardship” and offer a different option.</li>
            <li>Request supporting documentation to verify that an employee has a disability.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r3",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Disabilities in the Work World",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t4-r3_q3qypg.jpg",
      thumbnailPosition: "30% 50%",
      thumbnailDescription:
        "Young woman with disabilities is being interviewed.",
      type: "video",
      content: {
        media: "PTuTaIR2EpQ",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Laura:</span> As far as accommodations, she was very understanding. She was out of this world understanding and she came up with all of the ways to make my job successful for me. When I chose to disclose my cerebral palsy to my interviewer, I chose to do it after the whole interview because I just wanted to go in there first of all and really state who I am and my disability is just one little limitation that I have. I just wanted to present myself without that. That was my decision to let them know because I would feel more comfortable.</p>
          <p><span class="transcript-speaker">James:</span> Being on Social Security, I needed to have the job to accommodate my work schedule to fit in with my RSDI benefits. They helped accommodate me and reduce my hours and reduce my days so that I could still be a working adult.</p>
          <p><span class="transcript-speaker">Alan:</span> I told them that I had a disability and that I'm going to need some accommodations. And he said no problem. There are larger labels on things so that I'm able to read them easier. And there are some people that are willing to show me where things go.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r4",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "What is a Reasonable Accommodation?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t4-r4_pfbrsh.jpg",
      thumbnailDescription: "JAN: Job Accommodation Network logo.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697825227/audio/c5-t4-r4_02_dgf05y.mp3",
        text: `
        <div class="article">
          <p>The ADA requires employers to provide reasonable accommodations to any employee with a disability who asks for help.</p>
          <p>An accommodation is any strategy, modification or adjustment provided by an employer to ensure that people with disabilities have equal employment opportunities. In many cases, an accommodation is a minor adjustment to a specific task, work environment or work process. The word “reasonable” is important because employers don't have to provide accommodations that impose an “undue hardship” on their business. This means that an employer doesn't have to provide a solution that is too costly, requires the company to change the way it does business, etc. Because every employee is different and has different needs, every solution or accommodation is different.</p>
          <p>Here are some examples of accommodations:</p>
          <ul>
            <li>Changing a work area to be more accessible.</li>
            <li>Restructuring a job or shifting job duties.</li>
            <li>Adjusting your work schedule so you can go to medical appointments.</li>
            <li>Buying special equipment or modifying existing equipment.</li>
            <li>Modifying training or materials so that they are more accessible.</li>
          </ul>
          <p>The <a href="https://askjan.org/" target="_blank">Job Accommodations Network</a> (JAN) is a great resource for information about job accommodations and your right to reasonable accommodations under the ADA.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r5",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Disclosing a Disability",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657842306/images/c5-t4-r5_s7oedw.jpg",
      thumbnailDescription:
        "Young black man is having a serious discussion with his supervisor.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658036/audio/c5-t4-r5_02_eickoc.mp3",
        text: `
        <div class="article">
          <p>Disclosing a disability is a personal decision. Here are six common questions people ask when deciding whether or not to disclose a disability.</p>

          <h2>Do I have to disclose a disability?</h2>
          <p>No. It's up to you to decide if you want to tell your employer about your disability. This is called disclosure. You are never required to tell an employer that you have a disability and your employer is not allowed to ask you. However, you are not protected by the ADA until after you tell your employer that you have a disability.</p>

          <h2>When is the best time to disclose a disability?</h2>
          <p>There's no right or wrong time to discuss a disability with your employer. You can disclose your disability at any time. However, many people find it's best to disclose a disability after they've been offered a job. You can choose to disclose a disability:</p>
          <ul>
            <li>During the application process.</li>
            <li>During the interview process.</li>
            <li>After you have been offered a job.</li>
            <li>After you start work.</li>
            <li>At any time during your employment.</li>
            <li>After a problem occurs that is related to your disability.</li>
            <li>Never.</li>
          </ul>

          <h2>What should I think about?</h2>
          <p>You may want to consider disclosing a disability if:</p>
          <ul>
            <li>It's difficult or becoming difficult to accomplish some aspects of the job.</li>
            <li>Your disability poses a potential safety issue to you or your coworkers.</li>
            <li>Your coworkers or supervisor have mentioned problems with your job performance that you think are related to your disability.</li>
            <li>You have a very visible disability.</li>
          </ul>
          <p>Keep in mind that you only need to disclose a disability to your direct supervisor and the designated ADA person(s) in human resources. They are not allowed to share this confidential information with your co-workers or other employees. It is also your decision to share about a disability with co-workers.</p>

          <h2>Are there advantages to disclosing a disability?</h2>
          <p>One of the biggest advantages is that you're protected by the ADA once you tell your employer that you have a disability. Your employer is then required to provide reasonable accommodations if you ask for them.</p>
          <p>Other potential advantages:</p>
          <ul>
            <li>Keeping a disability hidden can create anxiety and stress and sharing about this part of yourself can lead to a greater sense of well-being.</li>
            <li>You might be able to do your job more successfully because you can collaboratively problem-solve about modifications and accommodations.</li>
            <li>It can lead to a more supportive and inclusive work environment.</li>
            <li>Co-workers and supervisors may be more sensitive to your needs and offer assistance as necessary.</li>
            <li>You may be able to access additional resources and services through your employer or other organizations.</li>
          </ul>

          <h2>Are there any disadvantages to disclosing a disability?</h2>
          <p>Yes. There are some potential disadvantages to disclosing a disability to your employer.</p>
          <ul>
            <li>You may find that your employer or coworkers treat you differently from other coworkers. This might be experienced as stigma, bias, prejudice, stereotypes and misunderstandings related to you and the disability.</li>
            <li>Your employer may illegally discriminate against you, for example in regards to job assignments and opportunities for advancement.</li>
            <li>It can lead to a loss of privacy and expose you to unwanted questions or attention.</li>
          </ul>

          <h2>What information do I have to share?</h2>
          <p>You don't have to share any information unless you ask for an accommodation. If you request an accommodation, your employer has the right to request independent documentation from a healthcare provider. However, you are never required to share:</p>
          <ul>
            <li>Specific, personal details about your disability.</li>
            <li>How you acquired the disability.</li>
            <li>Your prognosis.</li>
            <li>Current treatments that you're receiving or might need in the future.</li>
            <li>Treatments you've received in the past.</li>
          </ul>
          <p>You should consider sharing:</p>
          <ul>
            <li>General information that will help your employer better understand your disability as it relates to your job.</li>
            <li>Ideas for specific accommodations and how they will help you perform your job functions and duties.</li>
            <li>That you are aware of your rights under the ADA, including the right to reasonable accommodations.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r6",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Tips for Disclosing a Disability",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657896962/images/c5-t4-r6_euqq3o.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription:
        "Woman is having a serious discussion with her supervisor.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657995/audio/c5-t4-r6_02_e6gijf.mp3",
        text: `
        <div class="article">
          <p>You can disclose a disability in writing or during a confidential conversation with your employer.</p>
          <p>Regardless of the way you tell your employer, you should do these six things.</p>

          <h2>1. Focus on what you can do, not on your limitations</h2>
          <p>Don't make your disability out to be more important than it is. For example, “I can lift up to 10 pounds” sounds more positive than “I can't lift more than 10 pounds.”</p>

          <h2>2. Identify specific solutions to address specific challenges</h2>
          <p>For example, “I would like to take my morning break one hour earlier so that I can take my medication as prescribed.”</p>

          <h2>3. Review the job description to make sure you understand your employer's expectations</h2>
          <p>Be sure that you understand the difference between critical job responsibilities (essential functions) and those that are less important. Focus on essential functions and suggest specific solutions or accommodations that will help you to perform them better.</p>

          <h2>4. Decide what you want to tell your employer.</h2>
          <p>Practice disclosing your disability with your job placement specialist or another person.</p>

          <h2>5. Think about questions your employer may have and how you will respond</h2>
          <p>Do some research on the Internet or ask your job placement specialist about possible questions and responses related to the disability and reasonable accommodations.</p>

          <h2>6. Be prepared to provide documentation from your doctor or other professional</h2>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r7",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Tips for Requesting an Accommodation",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657896961/images/c5-t4-r7_rmokvs.jpg",
      thumbnailDescription:
        "Sky is in the background with a sign that says Accommodation .",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658598202/audio/c5-t4-r7_mo9kph.mp3",
        text: `
        <div class="article">
          <p>The ADA does not provide a specific process for requesting workplace accommodations. If your employer has a form for requesting an accommodation, use it to document your disability and explain the accommodation that you're requesting.</p>
          <p>If your employer does not have a form, make your request in writing. While the ADA does not require a written request, a letter will document your request in case there are questions later on. If you feel more comfortable asking for an accommodation in person, be sure to follow-up with a written request.</p>
          <p>Check out the <a href="https://askjan.org/" target="_blank">Job Accommodations Network</a> web site to learn more or work with your job placement specialist to come up with a plan for talking with your employer.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c5-t4-r8",
      categoryId: "c5",
      topicId: "c5-t4",
      title: "Taking Advantage of Companywide Programs",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1657896962/images/c5-t4-r8_lqznvr.jpg",
      thumbnailDescription:
        "15 portrait shots of people with a variety of diversities.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657993/audio/c5-t4-r8_01_ix2coa.mp3",
        text: `
        <div class="article">
          <p>You don't always have to ask for a special accommodation. Many employers offer policies and programs that are available to all employees, with and without disabilities.</p>
          <p>Find out if your employer offers:</p>
          <ul>
            <li>Employee assistance programs that offer confidential counseling services.</li>
            <li>Opportunities to work from home or flexible schedules.</li>
            <li>Free training.</li>
            <li>Bulletin boards or web pages with job postings, carpool programs, etc.</li>
            <li>Special interest groups for employees.</li>
            <li>Transit subsidies or programs.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    // Category 6 resources
    {
      id: "c6-t1-r1",
      categoryId: "c6",
      topicId: "c6-t1",
      title: "What Makes a Good Employee?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006052/images/c6-t1-r1_kbovgs.jpg",
      thumbnailDescription:
        "Black man is a chef and working in a restaurant kitchen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657996/audio/c6-t1-r1_01_hjtzna.mp3",
        text: `
        <div class="article">
          <p>Good employees share some common characteristics called “soft skills.”</p>
          <p>Soft skills have to do with:</p>
          <ul>
            <li>The way you communicate with other people.</li>
            <li>The way you work with other people in your work group.</li>
            <li>How you pick up on other people's feelings.</li>
            <li>How you follow the rules and solve problems.</li>
          </ul>
          <p>Hard skills are the education, experience, skills and abilities that you need to do a job.</p>
          <p>Think about each characteristic (soft skill) on the following list. Does it describe you? If not, ask your job placement specialist for help. Great employees:</p>
          <ul>
            <li>Are flexible.</li>
            <li>Are punctual – they arrive on time, stay until their shift ends and call their supervisor when they can't make it to work.</li>
            <li>Work hard every day.</li>
            <li>Are open to constructive feedback about areas for growth or improvement.</li>
            <li>Ask questions if they don't understand something.</li>
            <li>Are willing to learn new things.</li>
            <li>Have a good attitude.</li>
            <li>Are pleasant to be around – are polite and treat their coworkers and supervisors with respect.</li>
            <li>Know when to talk, when to listen and what topics to avoid.</li>
            <li>Are team players who pitch in when they're needed.</li>
            <li>Follow the rules, including dress code or uniform requirements.</li>
            <li>Have good hygiene.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t1-r2",
      categoryId: "c6",
      topicId: "c6-t1",
      title: "What Employers Expect from an Employee",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006052/images/c6-t1-r2_lwpblv.jpg",
      thumbnailDescription: "Employer is thoughtfully thinking.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697818817/audio/c6-t1-r2_02_wmptsl.mp3",
        text: `
        <div class="article">
          <p>No matter what job you are hired to do, most employers look for and expect similar things from their employees.</p>
          <p>Most employers don't expect you to know how to do a new job perfectly. However, they do expect you to know the things that will help you be successful on the job.</p>

          <h2>Be reliable</h2>
          <p>Employers expect you to show up on time. Let your supervisor know as soon as possible if you're ill or can't go to work because of an emergency. You may lose your job if you just don't show up. Plus, your coworkers will have to cover for you.</p>

          <h2>Take your job seriously</h2>
          <p>Work hard, follow instructions and do your job to the best of your ability. Ask questions if you're not sure what to do and ask for help when you need it.</p>

          <h2>Be honest</h2>
          <p>Employers value employees who are honest in their actions and their words. Never take anything that isn't yours; don't exaggerate your skills; and always take responsibility for your mistakes.</p>

          <h2>Put in effort</h2>
          <p>Employers don't expect you to be perfect. However, they do expect you to work hard and give your best effort.</p>

          <h2>Ask questions</h2>
          <p>There's an old saying that there's no such thing as a silly question. This is especially true when you're just learning a job or are new to an organization. Ask for more information when you need it. It's easier to avoid problems than to try to correct them later.</p>

          <h2>Be polite and respectful to your coworkers</h2>
          <p>Employers want their employees to get along. Treat your coworkers and supervisors the way you want to be treated. Help out when you're asked or see a need. Be pleasant and polite.</p>

          <h2>Have a positive attitude</h2>
          <p>Employers want positive and kind employees. You want to be remembered for a can-do attitude, not because people didn't want to work with you.</p>

          <h2>Communicate with your supervisor</h2>
          <p>Your supervisor can help you improve your skills or work out problems with a coworker. But good communication goes both ways – be sure to listen to what your supervisor says.</p>

          <h2>Follow company policies</h2>
          <p>Employers put policies and procedures in place to help their businesses run smoothly. Be sure you understand and follow your employer's policies.</p>

          <h2>Follow the company's dress code</h2>
          <p>If you're required to wear a uniform as part of your job, follow the requirements completely. This means making sure your uniform is clean and that you wear all the pieces. For example, if the employer requires a certain type of shirt, pants, socks, shoes, hat and nametag, be sure to follow it unless you have an employer approved exemption.</p>
          <p>The same goes for following the company dress codes. If the company has a business casual dress code, do not wear casual clothes such as jeans and a t-shirt.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t1-r3",
      categoryId: "c6",
      topicId: "c6-t1",
      title: "Advance Preparations",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006052/images/c6-t1-r3_i2wutc.jpg",
      thumbnailDescription:
        "To-Do List is written in a notebook lying on a desk with a cup of coffee and a pen.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657997/audio/c6-t1-r3_01_mgsl0p.mp3",
        text: `
        <div class="article">
          <p>Tomorrow's the big day! It's important to start off right. Here's a “to-do” list of tips to help you get ready.</p>

          <h2>Think positive!</h2>
          <p>The next day or two will probably be a little stressful. Before you start a new job, think about the kind of employee you want to be. Do you know what you need to do to be successful? Some positive self-talk is a good way to prepare for the week ahead.</p>

          <h2>Plan to get to work early</h2>
          <p>Arriving a few minutes early will give you time to get settled, use the restroom and clock in.</p>

          <h2>Get some sleep!</h2>
          <p>Try to get a good night's rest. You want to be at your best on your first day.</p>

          <h2>Set two alarms</h2>
          <p>Don't oversleep on your first day. It happens. To be safe, set two alarms for five or ten minutes apart and place them in different parts of the room.</p>

          <h2>Know how you'll get to work</h2>
          <p>If you're taking public transportation, check the schedule and take a practice trip. Take into account the time you'll need to get from the transit stop to your workplace. And, bring the correct fare. Remember, fares go up during peak travel times. If you're driving or getting dropped off, test how long it takes to drive to work from your home. Travel the same route that you'll use to get to work at about the same time of day.</p>

          <h2>Set out your clothes</h2>
          <p>Decide what you're going to wear. Set out everything you need the night before, including your entire uniform, clean underclothes, socks and shoes. If you don't wear a uniform, choose clothing that fits the dress code. Be sure that your choices are clean and pressed.</p>

          <h2>Gather important documents</h2>
          <p>In Course 5, you learned about some of the supporting documents that your employer is required to review. Gather them, along with your emergency contact information and a copy of your Master Application. Bring all of the documents with you to the job.</p>

          <h2>Know where to go and who to meet</h2>
          <p>Be sure you know when to arrive, what door to enter, where to go inside the building and who you're supposed to meet.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t2-r1",
      categoryId: "c6",
      topicId: "c6-t2",
      title: "Making a Good Impression",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t2-r1_jsvefz.jpg",
      thumbnailPosition: "100% 50%",
      thumbnailDescription: "Smiling Asian woman enters a medical clinic.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658000/audio/c6-t2-r1_01_guhd0j.mp3",
        text: `
        <div class="article">
          <p>You obviously made a good impression on the person that hired you. Now, you need to make a good impression on your coworkers.</p>
          <p>Here are 10 tips to help you start your new job right.</p>

          <h2>1. Be calm</h2>
          <p>Humans prefer to be around people who are calm or content. If you feel anxious, nervous or jittery, take a few deep breaths to help you relax.</p>

          <h2>2. Body language matters</h2>
          <p>As you meet and greet people, you want your body language to send a positive message. For many people, a firm handshake and direct eye contact are signs of confidence and respect. Or you might smile, nod, slightly bow and look towards the person.</p>

          <h2>3. Learn people's names</h2>
          <p>You'll be introduced to a lot of people during the first week. It may be tough to keep their names straight. Try to say each person's name a couple of times while looking at their face. Each time you connect the name with a specific person, it will be easier to recall his or her name later.</p>

          <h2>4. Be polite and respectful to everyone</h2>
          <p>Treat every person you come in contact with respectfully. This includes your supervisor, your coworkers, maintenance staff, etc. Kindness, and even a simple smile, can go a long way!</p>

          <h2>5. Arrive on time. Even better, arrive a few minutes early!</h2>
          <p>This shows you're dependable and eager to get started.</p>

          <h2>6. Keep to scheduled breaks</h2>
          <p>Be sure you understand and follow the company's policies for work and meal breaks. Don't take longer unless you have an approved accommodation.</p>

          <h2>7. Don't be a know-it-all</h2>
          <p>When you start a job, you want your coworkers to know that you can do it. But, there's a fine line between bragging and letting people know that you're experienced.</p>

          <h2>8. Listen and don't share too much personal information</h2>
          <p>Many people talk a lot when they're nervous. It's better to be a good listener. This allows you to get to know your job and your coworkers more quickly. Keep it professional, not personal. Don't share everything about your life on your first day. If everything goes well, you'll be working with these people for a long time so they'll have plenty of time to get to know you.</p>

          <h2>9. Ask questions and get help when you need it</h2>
          <p>If you have questions, ask for help. Your employer has hired you to do a job – that doesn't mean they assume you'll know how they want it done.</p>

          <h2>10. Dress for success</h2>
          <p>You've heard this over and over but it's important. Dress professionally and follow the company dress code or uniform policy every day, not just your first few days. If your employer sets aside certain days when employees can dress more casually, don't go overboard.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t2-r2",
      categoryId: "c6",
      topicId: "c6-t2",
      title: "Five Things You Can Do to Start Off Right",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t2-r2_ugn4kr.jpg",
      thumbnailDescription:
        "Calendar is in the background and a post-it note says Start a New Job!",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658620035/audio/c6-t2-r2_dujhfo.mp3",
        text: `
        <div class="article">
          <p>Don't expect to learn everything right away on your new job…it will take some time. Be patient.</p>
          <p>Your employer expects you to:</p>
          <ol>
            <li><b>LISTEN</b> carefully.</li>
            <li><b>WATCH</b> how a task is done.</li>
            <li><b>ASK</b> questions when you don't understand.</li>
            <li><b>ASK</b> for help when you need it.</li>
            <li><b>UNDERSTAND</b> your job and the employer's expectations.</li>
          </ol>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t2-r3",
      categoryId: "c6",
      topicId: "c6-t2",
      title: "Learning About the Company and the Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t2-r3_jtlwp6.jpg",
      thumbnailDescription:
        "Conference Room has two women and a man sitting at a table with another woman standing and leading a discussion.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657999/audio/c6-t2-r3_01_bzfvjy.mp3",
        text: `
        <div class="article">
          <h2>Welcome to New Employee Orientation</h2>
          <p>It's time to get to work! Every company has its own way of helping new employees learn about the company and their jobs. You might be asked to participate in some formal orientation activities.</p>
          <p>Larger organizations often hold regular new employee orientation sessions to:</p>
          <ul>
            <li>Explain the company's culture and philosophy.</li>
            <li>Discuss important policies like customer privacy or diversity.</li>
            <li>Explain and help new employees sign up for benefits and complete important paperwork, like W-4 and I-9 forms.</li>
            <li>Explain security procedures and provide a security ID card.</li>
            <li>Provide and review employee handbook. If your organization asks you to attend an orientation session, you should do four things.</li>
          </ul>

          <h3>1. Do it</h3>
          <p>This is a good chance to learn about the company and meet other new employees.</p>

          <h3>2. Ask questions</h3>
          <p>An HR Representative often attends these sessions and can answer questions about paychecks, benefits and how to request an accommodation.</p>

          <h3>3. Participate</h3>
          <p>Actively take part in all of the orientation activities.</p>

          <h3>4. Listen carefully</h3>
          <p>You can pick up a lot of important information about the company by listening carefully to what the facilitator says.</p>

          <h2>Learning Your New Job</h2>
          <p>Every employer decides how new employees are trained. Some companies ask new employees to watch or “shadow” other employees while they do their job or a coworker may be assigned to teach you. Your supervisor also may train you directly.</p>
          <p>Typically, your supervisor will explain how your success will be measured, including the company's expectations for quotas, speed or accuracy. Your supervisor also may provide on-the-spot training. If this happens, do your best to accept their feedback and constructive criticism politely and respectfully.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t2-r4",
      categoryId: "c6",
      topicId: "c6-t2",
      title: "Employment Policies",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t2-r4_oquctr.jpg",
      thumbnailPosition: "50% 50%",
      thumbnailDescription:
        "Employment handbook is lying on a desk with a high-lighter.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657998/audio/c6-t2-r4_01_g5jguu.mp3",
        text: `
        <div class="article">
          <p>Every company has rules that employees are expected to follow. These employment policies may be outlined in an employee handbook or posted on the company's employee web site.</p>

          <h2>Employee Handbook</h2>
          <p>You may be given a copy of the employee handbook or a handout containing important employment policies. Most handbooks include:</p>
          <ol>
            <li>Information on the company like their mission and vision.</li>
            <li>Important procedures for calling in sick, taking time off, signing up for or changing benefits, etc.</li>
            <li>“Big picture” policies, such as the company's privacy policies, anti-discrimination and harassment, drug/alcohol, termination and code of conduct.</li>
            <li>General pay and benefit information.</li>
            <li>Health and safety information and guidelines for the worksite.</li>
            <li>The company's conflict resolution or complaints process.</li>
            <li>General work schedules, performance expectations, disciplinary procedures and evaluation plans.</li>
            <li>Procedures for requesting a job accommodation and leaves of absence.</li>
          </ol>

          <h2>Your Responsibilities</h2>
          <ul>
            <li>You are responsible for reading and understanding company policies. If you have questions, discuss them with your supervisor. You also can ask your coworkers or job placement specialist for help.</li>
            <li>You may be asked to sign a form stating that you received and read the employee handbook.</li>
            <li>You are responsible for following the procedures outlined in the handbook.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t2-r5",
      categoryId: "c6",
      topicId: "c6-t2",
      title: "Most Common Policies",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t2-r5_tfp90e.jpg",
      thumbnailDescription:
        "Close-up of a filing cabinet with one folder labeled Policies.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657997/audio/c6-t2-r5_01_uxiutf.mp3",
        text: `
        <div class="article">
          <h2>Policies to Pay Special Attention To</h2>
          <p>Some policies to pay close attention to include:</p>
          <ul>
            <li>Breaks</li>
            <li>How to call in sick or late</li>
            <li>Harassment</li>
            <li>Paid time-off (PTO) (i.e., sick leave, vacation timPerformance and salary reviews</li>
            <li>Personal cell phone use on the job</li>
            <li>Use of business technology for personal reasons</li>
            <li>Privacy policies</li>
            <li>Requesting accommodations under the ADA</li>
            <li>Resolving conflicts at work</li>
            <li>Sexual harassment</li>
            <li>Technology use</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t2-r6",
      categoryId: "c6",
      topicId: "c6-t2",
      title: "Recognizing Unwritten Rules",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1675386662/images/c6-t2-r6-replacement_kwfc9g.jpg",
      thumbnailDescription:
        "Young lesbian woman with blue hair is standing in front of a building.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658036/audio/c6-t2-r6_01_uyrg4s.mp3",
        text: `
        <div class="article">
          <p>Almost every workplace has unwritten rules that employees follow. You can discover these unwritten rules by listening carefully and watching how your coworkers or supervisor respond in different situations.</p>
          <p>Here are ten common “unwritten rules:”</p>

          <h2>1. Watch your language!</h2>
          <p>Never swear, use slang terms or joke about race, gender, a person's appearance, etc. You need to be very careful, even if your workplace seems pretty relaxed. Remember, what you say and how you say it leaves an impression – good or bad – on your supervisors and coworkers.</p>

          <h2>2. Dress appropriately</h2>
          <p>The importance of dressing appropriately has been stressed throughout eTrac. That's because enforcing a dress code is one of the most common workplace problems that employers have to address. Your manager will usually explain the company's general uniform and dress code expectations. Be sure to follow them.</p>

          <h2>3. Personal hygiene</h2>
          <p>Personal hygiene is important. Be sure to always look and smell clean – your hair, body, teeth and clothes.</p>

          <h2>4. Arriving early and staying late</h2>
          <p>It is healthy to keep to your scheduled worktime. However, pay attention to the time your coworkers arrive at work and leave at the end of the workday. Some companies or work groups have an unwritten rule that you should arrive a few minutes early. The same is true for the end of the workday. Adjust your arrival and departure times as  desired, but remember having boundaries about your work hours is also important.</p>

          <h2>5. Interpersonal relationships</h2>
          <p>It can be difficult to know when work relationships become inappropriate. Harassment, including sexual harassment, is a serious issue. You have the right to be treated with respect by your coworkers, supervisors and management. The reverse is true, as well. You are required to treat coworkers, supervisors and company management respectfully. This includes what you say and do during and after working hours.</p>

          <h2>6. Listening to music</h2>
          <p>Everyone has their own preferences in music, radio stations, etc. If your company allows employees to listen to music or the radio at work:</p>
          <ul>
            <li>Keep the volume down.</li>
            <li>Use personal headphones or earbuds if that's an option.</li>
            <li>Even if you're wearing headphones, others might hear what you're listening to. Ask coworkers if they can hear your music and turn down the volume if necessary.</li>
            <li>Don't sing along, hum or whistle, or tap your pen in time to the music.</li>
          </ul>

          <h2>7. Smoking or vaping</h2>
          <p>Most companies limit smoking or vaping tobacco to specific, designated areas. Review smoking policies carefully to be sure that you understand where and when you can smoke during your workday. The same goes for chewing tobacco.</p>

          <h2>8. Using company property</h2>
          <p>You may have access to a wide range of company property as part of your job. Make sure you understand what equipment or property can be taken out of the building and if you need to sign it out or tell someone when you do. Some workplaces allow employees to use copiers and fax machines for personal purposes. Don't abuse the privilege!</p>

          <h2>9. Using your cell phone at work</h2>
          <p>If your employer doesn't have a formal policy, do not use your cell phone at work to make or receive calls or text messages. Remember, you're being paid to work. If you bring your phone to work, turn it off! If that's not possible, set it to vibrate or the do-not-disturb setting.</p>

          <h2>10. Using the Internet for personal reasons</h2>
          <p>Many companies have formal policies that restrict employees' use of the company's Internet for personal reasons. If your company doesn't have an official policy, don't surf the Internet at work. Even if your company does allow employees to use the Internet for personal reasons, don't abuse the privilege. Limit personal Internet use to break times. And, never use the company's Internet to look for another job!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r1",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Keeping Your Job",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r1_sohyz6.jpg",
      thumbnailDescription: "Man is operating a US Postal machine.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657999/audio/c6-t3-r1_01_tdivus.mp3",
        text: `
        <div class="article">
          <p>You worked hard to get your job. Now, you need to work just as hard to keep it! Here are some important tips for being successful at work.</p>
          <ul>
            <li><b>Have a good attitude.</b> Be polite, friendly, respectful and positive.</li>
            <li><b>Get to work on time every day.</b> If you're ill, be sure to notify your employer.</li>
            <li><b>Stay until the end of your shift.</b> Don't cut out early. You're being paid to do a job for a specific period of time.</li>
            <li><b>Be flexible and cooperative.</b> Do your best to get along with your coworkers and supervisors by helping out when you can and taking on new responsibilities when asked.</li>
            <li><b>Be honest.</b> Don't steal anything from your employer or coworkers and don't lie or blame others for your mistakes.</li>
            <li><b>Follow the rules.</b> This goes for uniform requirements or dress codes, smoking policies, safety rules, and other employment policies.</li>
            <li><b>Be independent.</b> Do the work you're paid to do. Ask for an accommodation if you're finding it difficult to do the job. You also can ask your job placement specialist for suggestions.</li>
            <li><b>Follow-through.</b> Do what you say you're going to do.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r2",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Tips for Getting Along with Customers",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r2_tfng14.jpg",
      thumbnailDescription:
        "Young woman is selling a pair of jeans to a customer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697657999/audio/c6-t3-r2_02_ahnxff.mp3",
        text: `
        <div class="article">
          <p>If you're in a job where you work with customers, it's important to stay positive and professional. Here are some things to keep in mind when working with customers:</p>
          <ul>
            <li>Be polite…even when the customer isn't.</li>
            <li>Greet customers in a friendly way.</li>
            <li>Listen carefully if a customer has concerns.</li>
            <li>Apologize and offer to help if there's a problem.</li>
            <li>Know the products or services that you're selling or supporting.</li>
            <li>Look professional and have good hygiene.</li>
            <li>Follow the dress code. Make sure your clothes are clean and neat. If you wear a uniform, be sure that it's complete.</li>
            <li>If the customer asks a question that you can't answer, find a coworker who can.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r3",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Tips for Getting Along with Coworkers",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r4_zqtqoi.jpg",
      thumbnailDescription:
        "5 coworkers are in a meeting with a variety of diversities. Young black man in a wheelchair is smiling.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658000/audio/c6-t3-r3_02_eadm3t.mp3",
        text: `
        <div class="article">
          <p>It's important to build good relationships with your coworkers. Here are some things to remember to do:</p>
          <ul>
            <li>Be kind, polite, respectful and positive.</li>
            <li>Be a good team player and pitch in to help your coworkers when needed.</li>
            <li>Do your work well and completely.</li>
            <li>Leave your personal problems at home.</li>
            <li>Take on new challenges as an opportunity to learn and grow.</li>
            <li>Clean up after yourself. Don't leave your lunch in the refrigerator overnight, your dirty coffee cup on the counter, etc.</li>
          </ul>
          <p>There are also some things you should <strong>not</strong> do.</p>
          <ul>
            <li>Don't gossip or lie about your coworkers.</li>
            <li>Don't judge the way your coworkers do their jobs.</li>
            <li>Don't show off or try to impress other employees.</li>
            <li>Don't take credit for other people's work.</li>
          </ul>
          <p>The best advice: Always treat your coworkers the way you want to be treated.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r4",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Tips for Getting Along with Your Supervisor",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r3_xgx3wq.jpg",
      thumbnailDescription:
        "Male supervisor is showing a male employee how to operate a machine in a factory.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658001/audio/c6-t3-r4_01_nygnce.mp3",
        text: `
        <div class="article">
          <p>Your supervisor is more than just another coworker. Establishing and maintaining a good relationship with your supervisor is important to your success on the job. Keep these things in mind when working with your supervisor:</p>
          <ul>
            <li>Remember, your supervisor is in charge. They make the decisions, not you.</li>
            <li>Do your work well and completely, even if you disagree with your supervisor's approach or think you know a better way.</li>
            <li>Arrive on time and stay until the end of your shift.</li>
            <li>Follow the dress code.</li>
            <li>If you encounter or learn about a problem on the job that you think your supervisor should know about, decide what you want to say, then ask to speak with your supervisor. Stay calm and always offer to help.</li>
            <li>Don't try to stir up trouble by gossiping or lie about your supervisor or coworkers.</li>
            <li>Show that you're proud of your employer and what you do.</li>
            <li>Don't brag about your accomplishments and don't take credit for another employee's work.</li>
            <li>Ask for new challenges, especially ones that will expand your career.</li>
          </ul>
          <p>Remember, always be a problem solver, not a problem!</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r5",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Watch What You Say at Work",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r5_el7zyg.jpg",
      thumbnailDescription:
        "Young woman is whispering in another woman's ear and she looks shocked.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658002/audio/c6-t3-r5_01_nkhgxn.mp3",
        text: `
        <div class="article">
          <p>Many workplace problems start when someone says the wrong thing. Think before you talk. If there's a chance something you want to say could be taken the wrong way, DON'T SAY IT!</p>

          <h2>Topics to avoid</h2>
          <p>Some topics should not be discussed at work because they are sensitive, can make people feel uncomfortable or cause arguments. Change the subject if these topics come up in conversation:</p>
          <ul>
            <li>Anything that is sexual in nature.</li>
            <li>Religion.</li>
            <li>Politics.</li>
            <li>Controversial social issues.</li>
            <li>Negative comments about a coworker, your supervisor or the company.</li>
          </ul>

          <h2>Don't overshare</h2>
          <p>Don't share too much personal information at work. Examples include:</p>
          <ul>
            <li>Personal finances.</li>
            <li>Your health challenges.</li>
            <li>Personal relationships, dating or problems with your family.</li>
            <li>Your alcohol or drug use.</li>
          </ul>
          <p>If you're dealing with serious personal problems that are affecting your job performance, talk confidentially with your job placement specialist, human resources, your supervisor or the company's employee assistance provider. They may be able to help you find the resources you need.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r6",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Common Workplace Conflicts",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006054/images/c6-t3-r6_hthwum.jpg",
      thumbnailPosition: "55% 50%",
      thumbnailDescription:
        "Notebook is lying on a desk displaying the words Conflict Management.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658001/audio/c6-t3-r6_02_zjf3vf.mp3",
        text: `
        <div class="article">
          <p>Workplace conflicts often fall into several categories:</p>
          <ul>
            <li>Problems getting along with coworkers.</li>
            <li>Problems getting along with your supervisor.</li>
            <li>Being late for work, leaving early or not showing up for your shift.</li>
            <li>Not doing your job well.</li>
            <li>Abusing drugs or alcohol.</li>
            <li>Having poor hygiene or grooming.</li>
            <li>Violating the company's dress code or uniform requirement.</li>
            <li>Having a negative attitude at work.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r7",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Who Can Help if You Have a Problem?",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r7_r8msc5.jpg",
      thumbnailPosition: "15% 50%",
      thumbnailDescription:
        "Sign displaying the words Human Resources and pointing to the right.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658001/audio/c6-t3-r7_01_tmvndr.mp3",
        text: `
        <div class="article">
          <p>Even excellent employees can run into problems on the job. You don't have to handle them alone!</p>
          <p>Here are some places you can go for help:</p>
          <ul>
            <li>Your Job Placement Specialist</li>
            <li>Your Supervisor</li>
            <li>Other Coworkers</li>
            <li>Human Resources</li>
            <li>Employee Assistance Providers</li>
            <li>Your Company's Conflict Resolution Policy (if there is onYour Union (if you're part of on</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r8",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Resolving Workplace Conflicts",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r8_kvqd3v.jpg",
      thumbnailDescription:
        "Two young women are in a conference room talking about another woman behind her back.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658005/audio/c6-t3-r8_01_y4dwyk.mp3",
        text: `
        <div class="article">
          <p>Workplace conflicts can occur when:</p>
          <ul>
            <li>Two or more coworkers have different needs or ideas.</li>
            <li>Someone behaves inappropriately at work.</li>
            <li>Someone doesn't do their job.</li>
            <li>People don't communicate well.</li>
            <li>There are changes in the way the work is done.</li>
            <li>When bias, discrimination or micro-aggressions happen:</li>
            <ul>
              <li>Bias is a prejudice, an unfair judgement or treatment to a particular person or group compared with another. An example of a bias is making a judgement about someone based on their race.</li>
              <li>Discrimination is when someone treats you differently, unfairly or worse based on characteristics such as disability, race, gender, age or sexual orientation. An example of discrimination is a person with a disability being harassed by a coworker with name-calling or offensive jokes.</li>
              <li>Microaggressions are insults and putdowns that happen in daily interactions and usually result in happening from a bias someone holds.  Sometimes the person saying or doing the microaggression doesn't realize they are causing harm. An example of a microaggression related to disability is speaking slowly or raising your voice to talk with a blind person.</li>
            </ul>
          </ul>

          <p>Here are some things to know about workplace conflicts:</p>
          <ul>
            <li>Conflicts rarely just “go away.”</li>
            <li>Conflicts usually grow and become more difficult to resolve.</li>
            <li>Conflicts can upset other people in your workplace who aren't directly involved.</li>
          </ul>
          <p>It's important to find a way to resolve workplace conflicts in a positive, professional way. Try to take care of it on your own. If that's doesn't work, ask for help from your supervisor or human resources. If a conflict is really serious, follow your organization's formal conflict resolution process.</p>
          <p>Your job placement specialist may also have ideas and advice.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r9",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Ten Steps for Resolving Conflicts",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r9_mdqzop.jpg",
      thumbnailDescription:
        "Circular graphic with ten steps depicted. In the center of the graphic are the words Resolving Conflicts.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658004/audio/c6-t3-r9_02_dlohsg.mp3",
        text: `
        <div class="article">
          <ol>
            <li><b>Take a step back.</b> Think objectively about what's bothering you so you understand why you're upset.</li>
            <li><b>Review the situation objectively.</b> Decide if the situation is really as serious as you think it is. Could you be frustrated about something else?</li>
            <li><b>Keep it private.</b> Don't try to involve other coworkers. This can create a difficult work environment for everyone. You don't want to get a reputation as a gossiper.</li>
            <li><b>Set a time to talk.</b> Find a time and place to meet with the person or people who are directly involved.</li>
            <li><b>Stay calm and present your point of view.</b> Keep it clear and simple.</li>
            <li><b>Accept responsibility for the part you played in the conflict.</b> Apologize for anything that you said or did that might have contributed to the situation.</li>
            <li><b>Offer practical solutions.</b> Focus on the future, not rehashing the past.</li>
            <li><b>Listen to the other side.</b> You may be surprised by what you learn.</li>
            <li><b>Work together to resolve the issue. Compromise.</b> Remember, this isn't about winning or losing, it's about doing what's best for the company. Try to find a solution that everyone agrees on.</li>
            <li><b>Let it go!</b> Put your energy towards what you can control or influence – yourself!</li>
          </ol>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r10",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Performance Reviews",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006053/images/c6-t3-r10_ujlu1f.jpg",
      thumbnailDescription:
        "Stop watch with the words Time to Evaluate on the face of the watch.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658033/audio/c6-t3-r10_01_y679ty.mp3",
        text: `
        <div class="article">
          <p><p>A performance review is a formal process that an employer uses to assess an employee's job performance. Think of it as a report card for your work life! Some employers will meet with you after your first three to six months on the job to discuss your job performance. Others review employee performance after your first year.</p>
          <p>A performance review can be used to:</p></p>
          <ul>
            <li>Evaluate your work performance according to standards set by your employer.</li>
            <li>Recognize your accomplishments.</li>
            <li>Discuss performance problems.</li>
            <li>Identify areas for improvement and additional training that you might need.</li>
            <li>Set goals for the future.</li>
            <li>Discuss your career with the organization.</li>
          </ul>
          <p>Sometimes, a performance review is tied to a raise in pay.</p>

          <h2>What to Expect</h2>
          <p>Every company conducts performance reviews or appraisals in its own way. It's a good idea to find out what you can expect to happen. You can:</p>
          <ul>
            <li>Ask your supervisor to explain the process.</li>
            <li>Talk with your coworkers.</li>
            <li>Check your employee handbook.</li>
          </ul>
          <p>Most appraisals are a casual conversation between you and your supervisor. It should take place in a private place and anything that is discussed should be kept confidential.</p>
          <p>After the review, your supervisor may provide a written recap of the conversation and describe any action plans that you and your supervisor agreed on. You will have a chance to review and sign it before it is placed in your personnel file.</p>

          <h2>Preparing for a Performance Review</h2>
          <p>You may be asked to prepare for an annual review by evaluating how well you think you performed your job over a specific period of time. Your supervisor may simply ask you to think about some topics or you may be asked to fill out an appraisal form and return it before the meeting.</p>
          <p>Be sure to consider things that happened during the entire period being reviewed, not just the last few weeks or months:</p>
          <ul>
            <li>Be objective about what you think you did well and what you think you could improve.</li>
            <li>Don't be modest. This is your chance to highlight your successes.</li>
            <li>Identify things you think you should improve.</li>
            <li>Identify some goals for the coming year.</li>
            <li>Think about questions you want to ask your supervisor.</li>
            <li>Review your career goals and identify new tasks that might help you achieve them.</li>
          </ul>

          <h2>Tips for Handling Constructive Feedback</h2>
          <p>Criticism can be tough to hear. Here are some suggestions for handling constructive feedback:</p>
          <ul>
            <li>Stay polite and be respectful.</li>
            <li>Take a deep breath. This will help you stay calm.</li>
            <li>Stay positive.</li>
            <li>Think about what was actually said. Be objective.</li>
            <li>Ask for more information and specific examples if you're confused and specific suggestions for improving your performance.</li>
            <li>Set concrete goals for the future and a timeline for achieving them.</li>
            <li>If you disagree with your supervisor, politely explain that you see things differently. Offer specific examples.</li>
            <li>Provide documentation that shows you tried to meet expectations.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t3-r11",
      categoryId: "c6",
      topicId: "c6-t3",
      title: "Requesting an Accommodation",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006054/images/c6-t3-r11_dtyede.jpg",
      thumbnailDescription:
        "Laptop is sitting on a desk and displaying the JAN website.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658620036/audio/c6-t3-r11_t15daw.mp3",
        text: `
        <div class="article">
          <p>As you become more experienced at your job, you may decide that an accommodation might be helpful. Remember, you can ask for an accommodation at any time during your employment.</p>
          <p>Go to the <a href="https://askjan.org/" target="_blank">Job Accommodations Network</a> web site to learn more or you can talk to your job placement specialist or someone from human resources.</p>
          <p>You might want to consider disclosing your disability or requesting an accommodation if:</p>
          <ul>
            <li>Your physical situation has changed and it's harder to complete the essential functions of your job.</li>
            <li>You received a poor performance review that you think relates to your disability.</li>
            <li>You think your disability poses a safety risk to you or your coworkers.</li>
            <li>You will need to take time off for regular medical appointments, etc.</li>
            <li>Your medication is affecting your performance.</li>
            <li>You need to request a medical leave.</li>
            <li>You learned about new technology or equipment that you think would help.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t4-r1",
      categoryId: "c6",
      topicId: "c6-t4",
      title: "Looking to the Future",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006054/images/c6-t4-r1_y8hidl.jpg",
      thumbnailPosition: "15% 50%",
      thumbnailDescription: "Young man with autism is being interviewed",
      type: "video",
      content: {
        media: "fGqh4C33q4E",
        text: `
        <div class="transcript">
          <p><span class="transcript-speaker">Bennie:</span> Well, I want to go back to school and take up nursing. And be in a situation that I can help people.</p>
          <p><span class="transcript-speaker">Laura:</span> I ultimately want to be a manager of Lifetime Fitness. I just have to go to my employer, my bosses, and say this is ultimately what I want to do. What do I have to do? Of course I have to gain experience.</p>
          <p><span class="transcript-speaker">James:</span> The amount of time that I've been in the housekeeping department will be a stepping stone to a supervisor or a lead person.</p>
          <p><span class="transcript-speaker">Zach:</span> I hope to become a manager whenever I get the chance.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t4-r2",
      categoryId: "c6",
      topicId: "c6-t4",
      title: "Exploring Options at Your Current Employer",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006052/images/c6-t4-r2_dez4zr.jpg",
      thumbnailDescription:
        "Young Asian woman in a wheelchair is smiling and talking with her employer.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1697658004/audio/c6-t4-r2_01_nb96w8.mp3",
        text: `
        <div class="article">
          <p>Your goal is to stay at your job for at least a year. This not only helps you to build work history, but it also shows that you're dependable and allows you to gain new skills and build relationships with people who can help you expand your career.</p>
          <p>You can continue to expand your career at your current job. Many people make the mistake of thinking that they have to leave their current employer in order to get a new job or do something different.</p>
          <p>However, many employers prefer to promote or move a current worker into a different job within the company rather than hire a new employee. After all, it's expensive to hire and train new employees.</p>
          <p>You may be able to find new opportunities at your current employer by:</p>
          <ul>
            <li>Asking your supervisor for new responsibilities.</li>
            <li>Discussing your career goals with your supervisor.</li>
            <li>Looking for internal job postings that might interest you.</li>
            <li>Finding out if the company has jobs – not necessarily job openings – that meet your interests.</li>
            <li>Excelling in your current position.</li>
            <li>Networking with other employees who have jobs that interest you or who work in other departments.</li>
            <li>Investigating the possibility of creating a job that might work better for you.</li>
            <li>Offering to serve on committees that might pave the way for new opportunities.</li>
          </ul>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t4-r3",
      categoryId: "c6",
      topicId: "c6-t4",
      title: "Keeping Your Career on Track",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006052/images/c6-t4-r3_debatk.jpg",
      thumbnailDescription:
        "Overhead sign on an interstate says Your Career with two arrows pointing forward.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658620036/audio/c6-t4-r3_dm0xzh.mp3",
        text: `
        <div class="article">
          <p>You'll keep your career on track by asking questions like:</p>
          <ol>
            <li>What do you like best about your job?</li>
            <li>What do you like least?</li>
            <li>How does this job fit into your career goals?</li>
            <li>Do you like the company and industry?</li>
            <li>Are there other jobs at this company that interest you?</li>
            <li>What have you learned about yourself and how you work?</li>
            <li>Do you need new skills in order to get promoted or expand your career?</li>
            <li>Where can you get this experience or training?</li>
          </ol>
          <p>Decide what you need to do to stay on track or, if your career interests have changed, start exploring those interests and looking at new careers.</p>
        </div>`,
      },
      keywords: [],
    },
    {
      id: "c6-t4-r4",
      categoryId: "c6",
      topicId: "c6-t4",
      title: "Telling Your Employer that You're Leaving",
      thumbnail:
        "https://res.cloudinary.com/der67rfj1/image/upload/v1658006052/images/c6-t4-r4_ez3uiu.jpg",
      thumbnailPosition: "70% 50%",
      thumbnailDescription:
        "Young man in a wheelchair is having a discussion with his supervisor.",
      type: "article",
      content: {
        media:
          "https://res.cloudinary.com/der67rfj1/video/upload/v1658620037/audio/c6-t4-r4_lsa6ce.mp3",
        text: `
        <div class="article">
          <p>At some point, you may decide it's time to find a different job. Ideally, you will find a new job before you quit the one you have now! After all, it can take several months, or longer, to find a new job and you don't want to be without a source of income.</p>
          <p>It's important to leave a good impression when you resign. You should:</p>
          <ul>
            <li>Leave your job the way you started it.</li>
            <ul>
              <li>Your employer will remember if you acted unprofessionally, “checked out” before your last day or became difficult to work with. You started your job with enthusiasm and a commitment to working hard…you should leave your job the same way.</li>
            </ul>

            <li>Write a resignation letter.</li>
            <ul>
              <li>Some employers need this for their files. It doesn't have to be long or detailed. Keep it simple, polite and respectful. Be sure that it is dated and that you keep a copy.</li>
            </ul>

            <li>Tell your supervisor BEFORE you tell your coworkers.</li>
            <ul>
              <li>Tell your supervisor in person that you will be leaving the company and thank him or her for the opportunity to work at the company. Don't lie about your reasons for leaving. If you feel comfortable, briefly explain your new position. Give your supervisor the resignation letter that you prepared.</li>
            </ul>

            <li>Give your employer at least two-weeks notice.</li>
            <ul>
              <li>Don't walk in and say you're leaving at the end of the day. Not only is this unprofessional, it will be what your employer remembers most about you.</li>
            </ul>

            <li>Ask for a reference and letter of recommendation.</li>
            <ul>
              <li>This is a great way to make sure you have what you need for future job searches.</li>
            </ul>

            <li>Turn in necessary paperwork, uniform and IDs.</li>

          </ul>
        </div>`,
      },
      keywords: [],
    },
  ],
};

export default data;
