const routes = [
  {
    path: "/",
    name: "splash",
    component: () => import("pages/SplashPage.vue"),
  },
  {
    path: "/home",
    name: "Home",
    component: () => import("pages/HomePage.vue"),
  },
  {
    path: "/category/:id",
    component: () => import("layouts/CategoryLayout.vue"),
    children: [
      {
        path: "",
        name: "category",
        component: () => import("pages/CategoryPage.vue"),
      },
    ],
  },
  {
    path: "/topic/:id",
    component: () => import("layouts/TopicLayout.vue"),
    children: [
      {
        path: "",
        name: "topic",
        component: () => import("pages/TopicPage.vue"),
      },
    ],
  },
  {
    path: "/list/:id",
    name: "list",
    component: () => import("pages/ResourceListPage.vue"),
  },
  {
    path: "/resource/:id",
    component: () => import("layouts/ResourceLayout.vue"),
    children: [
      {
        path: "",
        name: "resource",
        component: () => import("pages/ResourcePage.vue"),
      },
    ],
  },
  {
    path: "/search",
    name: "Search",
    component: () => import("pages/SearchPage.vue"),
  },
  {
    path: "/user",
    component: () => import("layouts/UserLayout.vue"),
    children: [
      {
        path: "likes",
        name: "Likes",
        component: () => import("pages/LikesPage.vue"),
      },
      {
        path: "my-info",
        name: "My Info",
        component: () => import("pages/MyInfoPage.vue"),
      },
      {
        path: "my-master-application",
        name: "My Master Application",
        component: () => import("pages/MyMasterApplicationPage.vue"),
      },
      {
        path: "master-application-instructions",
        name: "Instructions",
        component: () => import("pages/MasterApplicationInstructionsPage.vue"),
      },
      {
        path: "master-application",
        name: "Master Application",
        component: () => import("pages/MasterApplicationPage.vue"),
      },
      {
        path: "more",
        name: "More",
        component: () => import("pages/MorePage.vue"),
      },
      {
        path: "about",
        name: "About",
        component: () => import("pages/MoreAboutPage.vue"),
      },
      {
        path: "contact",
        name: "Contact",
        component: () => import("pages/MoreContactPage.vue"),
      },
      {
        path: "/content-review",
        name: "Content Review",
        component: () => import("pages/ContentReview.vue"),
      },
      {
        path: "/summary",
        name: "Resources Summary",
        component: () => import("pages/SummaryPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
