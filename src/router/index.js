import { route } from "quasar/wrappers";
import { useQuasar } from "quasar";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import routes from "./routes";
import { useContentStore } from "src/stores/ContentStore";
import { useStatusBarColor } from "src/use/useStatusBarColor";
import { Screen } from "quasar";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
      ? createWebHistory
      : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      } else if (to.hash) {
        return window.scrollTo({
          top: document.querySelector(to.hash).offsetTop,
          behavior: "auto",
        });
      } else {
        return { top: 0 };
      }
    },
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  Router.beforeEach((to, from, next) => {
    let contentStore = useContentStore();
    let documentTitle = "";

    // Dynamic route for category, topic, lists & resources
    // Handle various IDs, such as "c1", "c1-t2", "c1-t2-l1", and "c1-t2-r3"
    if (to.params.id) {
      if (to.name === "category") {
        const category = contentStore.getCategory(to.params.id);
        documentTitle = `${category.title} - ${import.meta.env.VITE_APP_TITLE}`;
      } else if (to.name === "topic") {
        const topic = contentStore.getTopic(to.params.id);
        documentTitle = `${topic.title} - ${import.meta.env.VITE_APP_TITLE}`;
      } else if (to.name === "list") {
        const resourceList = contentStore.getResourceList(to.params.id);
        const introItem = contentStore.getResource(resourceList.introItem);
        documentTitle = `${introItem.title} - ${
          import.meta.env.VITE_APP_TITLE
        }`;
      } else if (to.name === "resource") {
        const resource = contentStore.getResource(to.params.id);
        documentTitle = `${resource.title} - ${import.meta.env.VITE_APP_TITLE}`;
      }
    } else {
      documentTitle = `${to.name} - ${import.meta.env.VITE_APP_TITLE}`;
    }
    document.title = documentTitle;
    next();
  });

  Router.afterEach((to, from) => {
    // Orientation control: lock to portrait for small screens
    if (Screen.width < Screen.sizes.sm) {
      screen.orientation.lock("portrait");
    }

    // Set Cordova status bar color changes based on route
    if (to.params.id !== undefined && to.params.id.charAt(0) === "c") {
      // 1. For category/topic/resource screens
      const catId = to.params.id.split("-")[0];
      useStatusBarColor(catId);
    } else {
      // 2. For other screens
      useStatusBarColor(to.name);
    }
  });

  return Router;
});
