import { Screen } from "quasar";

export const formatSecond = (second) => {
  let hour_str = `${Math.floor(second / 60)}`;
  let second_str = `${Math.ceil(second % 60)}`;
  if (hour_str.length === 1) {
    hour_str = `0${hour_str}`;
  }
  if (second_str.length === 1) {
    second_str = `0${second_str}`;
  }
  return `${hour_str}:${second_str}`;
};

export const setGlobalTextSize = (val) => {
  // Idea: usePhone size, versus a larger tablet size
  // Main target: Keep the article line length on tablets to < 80 characters for size 1
  const usePhoneSize = Screen.lt.sm ? true : false;
  const htmlEl = document.getElementsByTagName("html")[0];
  let chosenSize = "";
  if (val == 1) {
    chosenSize = usePhoneSize ? "16px" : "26px";
  } else if (val == 2) {
    chosenSize = usePhoneSize ? "18px" : "30px";
  } else if (val == 3) {
    chosenSize = usePhoneSize ? "20px" : "34px";
  } else if (val == 4) {
    chosenSize = usePhoneSize ? "23px" : "38px";
  }

  htmlEl.style.fontSize = chosenSize;
};
