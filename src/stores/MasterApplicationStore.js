import { defineStore } from "pinia";
import Localbase from "localbase";

const db = new Localbase("MasterApplicationDB");
db.config.debug = false;

export const useMasterApplicationStore = defineStore("MasterApplicationStore", {
  state: () => {
    return {
      sectionTitles: [
        "Update",
        "Personal Data",
        "Eligibility to Work",
        "Criminal History",
        "Education",
        "Past Work Experiences",
        "Position Desired",
        "Other Important Experiences",
        "References",
      ],
      form: {
        section0: {
          email: {
            address: "",
            subject: "",
          },
          updated: {
            label: "Last updated",
            data: "",
          },
        },
        section1: {
          i1: {
            label: "Full name",
            data: "",
          },
          i2: {
            label: "Address",
            data: "",
          },
          i3: {
            label: "City",
            data: "",
          },
          i4: {
            label: "State",
            data: "",
          },
          i5: {
            label: "ZIP code",
            data: "",
          },
          i6: {
            label: "Phone (home)",
            data: "",
          },
          i7: {
            label: "Phone (mobile)",
            data: "",
          },
          i8: {
            label: "Social Security Number",
            data: "",
          },
        },
        section2: {
          i1: {
            label: "Are you eligible to work in the United States?",
            data: "",
          },
          i2: {
            label:
              "Do you have documentation needed to provide proof of eligibility to work in the United States?",
            data: "",
          },
          i3: {
            label: "If you answered yes, what documentation will you provide?",
            data: "",
          },
          i4: {
            label: "Do you have documentation that proves your identity?",
            data: "",
          },
          i5: {
            label: "If you answered yes, what documentation will you provide?",
            data: "",
          },
          i6: {
            label:
              "If you are under age 18, do you have a birth certificate to prove your age?",
            data: "",
          },
        },
        section3: {
          i1: {
            label:
              "Have you ever been convicted of a felony or pleaded “no contest” to a felony within the last five years?",
            data: "",
          },
          i2: {
            label: "If you answered yes, briefly explain the circumstances",
            data: "",
          },
          i3: {
            label: "Do you have a letter of explanation?",
            data: "",
          },
          i4: {
            label: "What does this letter of explanation address?",
            data: "",
          },
        },
        section4: {
          // High school diploma
          i1: {
            label: "Name of institution",
            data: "",
          },
          i2: {
            label: "Address",
            data: "",
          },
          i3: {
            label: "City, state, ZIP code",
            data: "",
          },
          i4: {
            label: "Date of graduation",
            data: "",
          },
          // GED certificate
          i5: {
            label: "Issued by",
            data: "",
          },
          i6: {
            label: "Date completed",
            data: "",
          },
          // Community or technical college
          i7: {
            label: "Name of institution",
            data: "",
          },
          i8: {
            label: "Address",
            data: "",
          },
          i9: {
            label: "City, state, ZIP code",
            data: "",
          },
          i10: {
            label: "Date attended or graduated",
            data: "",
          },
          i11: {
            label: "Degree or certificate earned",
            data: "",
          },
          i12: {
            label: "Special coursework",
            data: "",
          },
          // Four-year degree
          i13: {
            label: "Name of institution",
            data: "",
          },
          i14: {
            label: "Address",
            data: "",
          },
          i15: {
            label: "City, state, ZIP code",
            data: "",
          },
          i16: {
            label: "Dates attended or graduated",
            data: "",
          },
          i17: {
            label: "Major",
            data: "",
          },
          i18: {
            label: "Minor",
            data: "",
          },
          // Apprenticeships and internships
          i19: {
            label: "Program/company name",
            data: "",
          },
          i20: {
            label: "Skills learner",
            data: "",
          },
          i21: {
            label: "Date completed",
            data: "",
          },
          // Special certifications / training / licenses
          // #1
          i22: {
            label: "Title",
            data: "",
          },
          i23: {
            label: "License # if applicable",
            data: "",
          },
          i24: {
            label: "Date completed",
            data: "",
          },
          i25: {
            label: "Restrictions",
            data: "",
          },
          i26: {
            label: "Renewal date",
            data: "",
          },
          // #2
          i27: {
            label: "Title",
            data: "",
          },
          i28: {
            label: "License # if applicable",
            data: "",
          },
          i29: {
            label: "Date completed",
            data: "",
          },
          i30: {
            label: "Restrictions",
            data: "",
          },
          i31: {
            label: "Renewal date",
            data: "",
          },
          // Extracurricular
          i32: {
            label: "Extracurricular activities",
            data: "",
          },
        },
        section5: {
          // Work experience #1
          //   Employer info
          i1: {
            label: "Company name",
            data: "",
          },
          i2: {
            label: "Address",
            data: "",
          },
          i3: {
            label: "City, state, ZIP Code",
            data: "",
          },
          i4: {
            label: "Supervisor",
            data: "",
          },
          i5: {
            label: "Supervisor phone",
            data: "",
          },
          i6: {
            label: "Can this employer be contacted?",
            data: "",
          },
          //   Job title / responsibilities
          i7: {
            label: "Job title",
            data: "",
          },
          i8: {
            label: "Primary responsibilities",
            data: "",
          },
          i9: {
            label: "Date of employment (from)",
            data: "",
          },
          i10: {
            label: "Date of employment (to)",
            data: "",
          },
          i11: {
            label: "Salary",
            data: "",
          },
          i12: {
            label: "Reason for leaving",
            data: "",
          },
          // Work experience #2
          //   Employer info
          i13: {
            label: "Company name",
            data: "",
          },
          i14: {
            label: "Address",
            data: "",
          },
          i15: {
            label: "City, state, ZIP Code",
            data: "",
          },
          i16: {
            label: "Supervisor",
            data: "",
          },
          i17: {
            label: "Supervisor phone",
            data: "",
          },
          i18: {
            label: "Can this employer be contacted?",
            data: "",
          },
          //   Job title / responsibilities
          i19: {
            label: "Job title",
            data: "",
          },
          i20: {
            label: "Primary responsibilities",
            data: "",
          },
          i21: {
            label: "Date of employment (from)",
            data: "",
          },
          i22: {
            label: "Date of employment (to)",
            data: "",
          },
          i23: {
            label: "Salary",
            data: "",
          },
          i24: {
            label: "Reason for leaving",
            data: "",
          },
          // Work experience #3
          //   Employer info
          i25: {
            label: "Company name",
            data: "",
          },
          i26: {
            label: "Address",
            data: "",
          },
          i27: {
            label: "City, state, ZIP Code",
            data: "",
          },
          i28: {
            label: "Supervisor",
            data: "",
          },
          i29: {
            label: "Supervisor phone",
            data: "",
          },
          i30: {
            label: "Can this employer be contacted?",
            data: "",
          },
          //   Job title / responsibilities
          i31: {
            label: "Job title",
            data: "",
          },
          i32: {
            label: "Primary responsibilities",
            data: "",
          },
          i33: {
            label: "Date of employment (from)",
            data: "",
          },
          i34: {
            label: "Date of employment (to)",
            data: "",
          },
          i35: {
            label: "Salary",
            data: "",
          },
          i36: {
            label: "Reason for leaving",
            data: "",
          },
          // Work experience #4
          //   Employer info
          i37: {
            label: "Company name",
            data: "",
          },
          i38: {
            label: "Address",
            data: "",
          },
          i39: {
            label: "City, state, ZIP Code",
            data: "",
          },
          i40: {
            label: "Supervisor",
            data: "",
          },
          i41: {
            label: "Supervisor phone",
            data: "",
          },
          i42: {
            label: "Can this employer be contacted?",
            data: "",
          },
          //   Job title / responsibilities
          i43: {
            label: "Job title",
            data: "",
          },
          i44: {
            label: "Primary responsibilities",
            data: "",
          },
          i45: {
            label: "Date of employment (from)",
            data: "",
          },
          i46: {
            label: "Date of employment (to)",
            data: "",
          },
          i47: {
            label: "Salary",
            data: "",
          },
          i48: {
            label: "Reason for leaving",
            data: "",
          },
        },
        section6: {
          // Position #1
          i1: {
            label: "Title",
            data: "",
          },
          i2: {
            label: "Responsibilities",
            data: "",
          },
          i3: {
            label: "Desired salary",
            data: "",
          },
          // Position #2
          i4: {
            label: "Title",
            data: "",
          },
          i5: {
            label: "Responsibilities",
            data: "",
          },
          i6: {
            label: "Desired salary",
            data: "",
          },
        },
        section7: {
          i1: {
            label: "Community / volunteer activities",
            data: "",
          },
          i2: {
            label: "Hobbies",
            data: "",
          },
          i3: {
            label: "Daily life skills",
            data: "",
          },
          i4: {
            label: "Technology experience / technology training",
            data: "",
          },
        },
        section8: {
          // Reference #1
          i1: {
            label: "Name (1)",
            data: "",
          },
          i2: {
            label: "Company (1)",
            data: "",
          },
          i3: {
            label: "Title (1)",
            data: "",
          },
          i4: {
            label: "Contact information (1)",
            data: "",
          },
          i5: {
            label: "Phone (1)",
            data: "",
          },
          i6: {
            label: "Email (1)",
            data: "",
          },
          i7: {
            label: "Has this person has agreed to act as a reference? (1)",
            data: "",
          },
          // Reference #2
          i8: {
            label: "Name (2)",
            data: "",
          },
          i9: {
            label: "Company (2)",
            data: "",
          },
          i10: {
            label: "Title (2)",
            data: "",
          },
          i11: {
            label: "Contact information (2)",
            data: "",
          },
          i12: {
            label: "Phone (2)",
            data: "",
          },
          i13: {
            label: "Email (2)",
            data: "",
          },
          i14: {
            label: "Has this person has agreed to act as a reference? (2)",
            data: "",
          },
          // Reference #3
          i15: {
            label: "Name (3)",
            data: "",
          },
          i16: {
            label: "Company (3)",
            data: "",
          },
          i17: {
            label: "Title (3)",
            data: "",
          },
          i18: {
            label: "Contact information (3)",
            data: "",
          },
          i19: {
            label: "Phone (3)",
            data: "",
          },
          i20: {
            label: "Email (3)",
            data: "",
          },
          i21: {
            label: "Has this person has agreed to act as a reference? (3)",
            data: "",
          },
        },
      },
    };
  },
  actions: {
    loadFormData() {
      db.collection("formData")
        .get()
        .then((formData) => {
          const data = formData[0].data;
          this.form = JSON.parse(data);
        })
        .catch((error) => {
          // No data in dB yet, do nothing
        });
    },
    saveFormData() {
      db.collection("formData")
        .get()
        .then((data) => {
          if (data.length == 0) {
            // There's no data, add the document
            db.collection("formData").add({
              id: 1,
              // data: this.form.section1.i1.data,
              data: JSON.stringify(this.form),
            });
          } else {
            // There's data, update the document
            db.collection("formData")
              .doc({ id: 1 })
              .update({
                // data: this.form.section1.i1.data,
                data: JSON.stringify(this.form),
              });
          }
        });
    },
    updateDate(dateUpdatedStr) {
      this.form.section0.updated.data = dateUpdatedStr;
    },
    deleteAllData() {
      // Delete localbase collection
      db.collection("formData").delete();

      // Reset all any necessary Pinia store state
      for (const s in this.form) {
        const section = this.form[s];
        for (const f in section) {
          const formInput = section[f];
          formInput.data = "";
        }
      }
    },
  },
  getters: {
    lastUpdated: (state) => {
      return state.form.section0.updated.data;
    },
  },
});
