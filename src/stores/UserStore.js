import { defineStore } from "pinia";
import Localbase from "localbase";

const db = new Localbase("UserDB");
db.config.debug = false;

export const useUserStore = defineStore("UserStore", {
  state: () => {
    return {
      search: "",
      showThemePanel: false,
      textSize: 1,
      themeMode: null,
      isActivated: true,
    };
  },
  actions: {
    saveSettings() {
      db.collection("userSettings")
        .get()
        .then((data) => {
          if (data.length == 0) {
            // There's no data, add the document
            db.collection("userSettings").add({
              id: 1,
              textSize: this.textSize,
              themeMode: this.themeMode,
              isActivated: this.isActivated,
            });
          } else {
            // There's data, update the document
            db.collection("userSettings").doc({ id: 1 }).update({
              textSize: this.textSize,
              themeMode: this.themeMode,
              isActivated: this.isActivated,
            });
          }
        });
    },
    loadSettings() {
      db.collection("userSettings")
        .get()
        .then((settings) => {
          const settingsData = settings[0];
          this.textSize = settingsData.textSize;
          this.themeMode = settingsData.themeMode;
          this.isActivated = settingsData.isActivated;
        })
        .catch(function () {
          console.log("Promise Rejected");
        });
    },
  },
  getters: {
    getSearch: (state) => {
      return state.search;
    },
    getTextSize: (state) => {
      return state.textSize;
    },
    getThemeMode: (state) => {
      return state.themeMode;
    },
    getShowThemePanel: (state) => {
      return state.showThemePanel;
    },
  },
});
