import { defineStore } from "pinia";
import data from "assets/data.js";

export const useContentStore = defineStore("ContentStore", {
  state: () => ({
    data: data,
  }),
  getters: {
    getCategory: (state) => {
      // id: category ID, "c1"
      return (id) => {
        return state.data.categories.find((category) => category.id === id);
      };
    },
    getTopic: (state) => {
      // id: topic ID, "c1-t1"
      return (id) => {
        return state.data.topics.find((topic) => topic.id === id);
      };
    },
    getTopics: (state) => {
      // id: category ID, "c1"
      return (id) => {
        return state.data.topics.filter((topic) => topic.categoryId === id);
      };
    },
    getAllResources: (state) => {
      return state.data.resources;
    },
    getResourcesFromCategory: (state) => {
      // Get's all resources in the category
      // id: topic ID, "c1"
      return (id) => {
        return state.data.resources.filter(
          (resource) => resource.categoryId === id
        );
      };
    },
    getResourcesFromTopic: (state) => {
      // Get's all resources in the topic, including those nested in resource lists
      // id: topic ID, "c1-t1"
      return (id) => {
        return state.data.resources.filter(
          (resource) => resource.topicId === id
        );
      };
    },
    getResource: (state) => {
      // id: resource ID, "c1-t1"
      return (id) => {
        return state.data.resources.find((resource) => resource.id === id);
      };
    },
    getResourceList: (state) => {
      // id: list ID, "c1-t1-l1"
      return (id) => {
        return state.data.resourceLists.find((list) => list.id === id);
      };
    },
    getResourceParentList: (state) => {
      return (id) => {
        let result = undefined;
        const arr = state.data.resourceLists.map((resourceList) => {
          const found = resourceList.list.items.find((item) => item.id === id);
          if (found !== undefined) {
            result = resourceList;
          }
        });
        return result;
      };
    },
    isResourceListIntro: (state) => {
      return (id) => {
        return state.data.resourceLists.find((list) => list.introItem === id);
      };
    },
  },
});
