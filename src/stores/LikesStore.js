import { defineStore } from "pinia";
import Localbase from "localbase";

const db = new Localbase("LikesDB");
db.config.debug = false;

export const useLikesStore = defineStore("LikesStore", {
  state: () => {
    return {
      likes: [],
    };
  },
  actions: {
    loadLikes() {
      db.collection("likes")
        .orderBy("date", "desc")
        .get()
        .then((likes) => {
          this.likes = likes;
        });
    },
    toggleLike(toggledLike) {
      const isLiked = this.likes.some((like) => like.id === toggledLike.id);
      if (!isLiked) {
        // If not liked, like it (add to BEGINNING of array)
        db.collection("likes").add(toggledLike);
        this.likes.unshift(toggledLike);
      } else {
        // If liked, unlike it (create filtered array without it)
        db.collection("likes").doc({ id: toggledLike.id }).delete();
        this.likes = this.likes.filter((like) => like.id !== toggledLike.id);
      }
    },
    removeLikeById(idToRemove) {
      db.collection("likes").doc({ id: idToRemove }).delete();
      this.likes = this.likes.filter((like) => like.id !== idToRemove);
    },
    deleteAllData() {
      // Delete localbase collection
      db.collection("likes").delete();

      // Reset all any necessary Pinia store state
      this.likes = [];
    },
  },
  getters: {
    isLiked: (state) => {
      return (id) => {
        return state.likes.some((like) => like.id === id);
      };
    },
    getLikes: (state) => {
      return state.likes;
    },
  },
});
